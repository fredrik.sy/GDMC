import numpy
import sys

from grammar import symbols
from numpy import ndarray
from worldmap import Material
from worldmap import WorldMap


class Node(object):
    _id = 0

    def __init__(self, height, material):
        """
        :type height: int
        :type material: Material
        """
        self._adjacent = set()
        self._height = height
        self._id = Node._id
        self._material = material
        self._points = set()
        Node._id += 1

    @property
    def adjacent(self):
        return self._adjacent

    @property
    def height(self):
        return self._height

    @height.setter
    def height(self, value):
        self._height = value

    @property
    def id(self):
        return self._id

    @property
    def material(self):
        return self._material

    @property
    def points(self):
        return self._points

    def mask(self):
        minx = sys.maxint
        minz = sys.maxint
        maxx = -sys.maxint - 1
        maxz = -sys.maxint - 1

        for (x, z) in self.points:
            if x < minx:
                minx = x
            if x > maxx:
                maxx = x
            if z < minz:
                minz = z
            if z > maxz:
                maxz = z

        ma = numpy.zeros((maxz - minz + 1, maxx - minx + 1), dtype=bool)

        for (x, z) in self.points:
            ma[z - minz, x - minx] = True

        return ma, minx, minz


class Graph(object):
    def __init__(self, worldmap, ma):
        """
        :type worldmap: WorldMap
        :type ma: ndarray
        """
        self._nodes = {}
        self._worldmap = worldmap

        adjacent = numpy.full_like(worldmap.heightmap, -1, dtype=int)
        points = {(x, z) for (z, x) in numpy.transpose(ma.nonzero())}

        while points:
            (x, z) = points.pop()
            unvisited = {(x, z)}
            node = Node(worldmap.heightmap[z, x], worldmap.materials[z, x])

            while unvisited:
                (x, z) = unvisited.pop()
                node.points.add((x, z))
                adjacent[z, x] = node.id

                for (xc, zc) in ((x - 1, z), (x + 1, z), (x, z - 1), (x, z + 1)):
                    if (xc, zc) in points:
                        if worldmap.heightmap[zc, xc] == worldmap.heightmap[z, x] and \
                                worldmap.materials[zc, xc] == worldmap.materials[z, x]:
                            points.remove((xc, zc))
                            unvisited.add((xc, zc))

            self._nodes[node.id] = node

        ma = {(x, z) for (z, x) in numpy.transpose(numpy.where(adjacent != -1))}

        while ma:
            (x, z) = ma.pop()

            for (xc, zc) in ((x - 1, z), (x + 1, z), (x, z - 1), (x, z + 1)):
                if (xc, zc) in ma:
                    node_id1 = adjacent[z, x]
                    node_id2 = adjacent[zc, xc]

                    if node_id1 != node_id2:
                        self._nodes[node_id1].adjacent.add(node_id2)
                        self._nodes[node_id2].adjacent.add(node_id1)

    @property
    def nodes(self):
        return self._nodes

    def get_symbol(self, node_id, height):
        """
        :type node_id: int
        :type height: int
        """
        edge = True
        node = self._nodes[node_id]

        for (x, z) in node.points:
            if not self._worldmap.edges[z, x]:
                edge = False
                break

        if edge:
            return symbols[node.material] + 'E' + str(node.height - height)
        else:
            return symbols[node.material] + str(node.height - height)

    def set_height(self, node_id, value):
        """
        :type node_id: int
        :type value: int
        """
        node = self._nodes[node_id]
        diff = value - node.height
        assert diff != 0

        for (x, z) in node.points:
            self._worldmap.push_block(x, node.height, z, diff)

        node.height = value
        self._worldmap.revalidate(node.points)

        for neighbour_id in node.adjacent.copy():
            if neighbour_id in self._nodes:
                neighbour_node = self._nodes[neighbour_id]

                if neighbour_node.height == node.height and neighbour_node.material == node.material:
                    node.adjacent.remove(neighbour_id)
                    neighbour_node.adjacent.remove(node_id)
                    node.points.update(neighbour_node.points)

                    for adjacent_id in neighbour_node.adjacent:
                        adjacent_node = self._nodes[adjacent_id]
                        adjacent_node.adjacent.remove(neighbour_id)
                        adjacent_node.adjacent.add(node_id)
                        node.adjacent.add(adjacent_id)

                    self._nodes.pop(neighbour_id)

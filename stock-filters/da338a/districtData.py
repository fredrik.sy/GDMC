class DistrictData(object):
    def __init__(self, origin_x, origin_z, end_x, end_z, house_amount, wealth_score, two_squares, two_floors,
                 x_shift, z_shift):
        self.xOrigin = origin_x
        self.zOrigin = origin_z
        self.xEnd = end_x
        self.zEnd = end_z
        self.houseAmount = house_amount
        self.wealthScore = wealth_score  # 0 for richest
        self.twoSquares = two_squares
        self.twoFloors = two_floors
        self.xShift = x_shift
        self.zShift = z_shift


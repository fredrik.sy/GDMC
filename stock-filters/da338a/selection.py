import heapq
import numpy

from worldmap import Material
from worldmap import WorldMap


class Selection(object):
    def __init__(self, worldmap, options):
        """
        :type worldmap: WorldMap
        :type options: dict
        """
        self._worldmap = worldmap
        self._options = options

    def find(self):
        # ma = numpy.logical_and.reduce((self._worldmap.chunk_materials[:, :, Material.Ground] > 192,
        #                                self._worldmap.heightmap > 50,
        #                                self._worldmap.heightmap < 75))
        ma = numpy.logical_and.reduce((self._worldmap.chunk_materials[:, :, Material.Water] == 0,
                                       self._worldmap.heightmap > 50,
                                       self._worldmap.heightmap < 75))
        points = {(x, z) for (z, x) in numpy.transpose(ma.nonzero())}
        heap = []
        data = []
        self._worldmap.save('stock-filters/da338a/images/selection.jpg', ma)

        while points:
            unvisited = {points.pop()}
            visited = numpy.full_like(self._worldmap.heightmap, False, dtype=bool)

            while unvisited:
                (x, z) = unvisited.pop()
                visited[z, x] = True

                for (xc, zc) in ((x - 1, z), (x + 1, z), (x, z - 1), (x, z + 1)):
                    if (xc, zc) in points:
                        points.remove((xc, zc))
                        unvisited.add((xc, zc))

            count = numpy.count_nonzero(visited)

            if count >= 16 * 16 * self._options['Min Chunks']:
                heapq.heappush(heap, (-count, len(data)))
                data.append(visited)

        while heap:
            i = heapq.heappop(heap)[1]
            yield data[i]

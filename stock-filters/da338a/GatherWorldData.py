from BlockPalette import BlockPalette
from pymclevel.materials import pocketMaterials
from enum import Enum
from districtData import DistrictData
from pymclevel.box import BoundingBox
import pymchelper
from worldmap import WorldMap
import random


class SearchBox(object):
    def __init__(self, start_x, length_x, start_z, length_z):
        self.startX = start_x
        self.lengthX = length_x
        self.startZ = start_z
        self.lengthZ = length_z


# Enum representing tree types
class TreeType(Enum):
    Oak = 0
    Birch = 1
    Spruce = 2
    Jungle = 3
    Acacia = 4
    DarkOak = 5


# Enum representing ground types
class GroundType(Enum):
    Grass = 0
    Rock = 1
    YellowSand = 2
    RedSand = 3
    HardenedClay = 4


def determineWoodType(level, box, height_map, x_shift, z_shift):
    treeType = None
    oakTrees = 0
    spruceTrees = 0
    birchTrees = 0
    jungleTrees = 0
    acaciaTrees = 0
    darkOakTrees = 0
    maxAmount = 0
    w = abs(box.maxz - box.minz)
    h = abs(box.maxx - box.minx)
    countX = box.minx
    countZ = box.minz
    # iterate over the x dimension of the mapping
    for xCoord in range(h):
        # iterate over the z dimension of the mapping
        countZ = box.minz
        for zCoord in range(w):
            heightMapZ = countZ - box.minz
            heightMapX = countX - box.minx
            y = height_map[heightMapX, heightMapZ] + 1
            if pymchelper.has_material(level, pocketMaterials.Wood, countX, y, countZ):
                oakTrees = oakTrees + 1
            elif pymchelper.has_material(level, pocketMaterials.SpruceWood, countX, y, countZ):
                spruceTrees = spruceTrees + 1
            elif pymchelper.has_material(level, pocketMaterials.BirchWood, countX, y, countZ):
                birchTrees = birchTrees + 1
            elif pymchelper.has_material(level, pocketMaterials.JungleWood, countX, y, countZ):
                jungleTrees = jungleTrees + 1
            elif pymchelper.has_material(level, pocketMaterials.AcaciaWood, countX, y, countZ):
                acaciaTrees = acaciaTrees + 1
            elif pymchelper.has_material(level, pocketMaterials.DarkOakWood, countX, y, countZ):
                darkOakTrees = darkOakTrees + 1
            countZ += 1
        countX += 1

    if oakTrees > maxAmount:
        maxAmount = oakTrees
        treeType = TreeType.Oak
    if spruceTrees > maxAmount:
        maxAmount = spruceTrees
        treeType = TreeType.Spruce
    if birchTrees > maxAmount:
        maxAmount = birchTrees
        treeType = TreeType.Birch
    if jungleTrees > maxAmount:
        maxAmount = jungleTrees
        treeType = TreeType.Jungle
    if acaciaTrees > maxAmount:
        maxAmount = acaciaTrees
        treeType = TreeType.Acacia
    if darkOakTrees > maxAmount:
        maxAmount = darkOakTrees
        treeType = TreeType.DarkOak
    if maxAmount == 0:
        treeType = TreeType.Oak  # default if for some reason it can't find any ground blocks
    return treeType


def determineMainGround(level, box, height_map, x_shift, z_shift):
    groundType = None
    grassAmount = 0
    rockAmount = 0
    yellowSand = 0
    redSand = 0
    hardenedClay = 0
    maxAmount = 0
    airAmount = 0
    w = abs(box.maxz - box.minz)
    h = abs(box.maxx - box.minx)
    countX = box.minx
    countZ = box.minz

    # iterate over the x dimension of the mapping
    for xCoord in range(h):
        # iterate over the z dimension of the mapping
        countZ = box.minz
        for zCoord in range(w):
            heightMapZ = countZ - box.minz + z_shift
            heightMapX = countX - box.minx + x_shift
            y = height_map[heightMapZ, heightMapX]
            if pymchelper.has_material(level, pocketMaterials.Grass, countX, y, countZ) or \
                    pymchelper.has_material(level, pocketMaterials.Dirt, countX, y, countZ) or \
                    pymchelper.has_material(level, pocketMaterials.Podzol, countX, y, countZ) or \
                    pymchelper.has_material(level, pocketMaterials.Mycelium, countX, y, countZ):
                grassAmount = grassAmount + 1
            elif pymchelper.has_material(level, pocketMaterials.Gravel, countX, y, countZ) or \
                    pymchelper.has_material(level, pocketMaterials.Stone, countX, y, countZ) or \
                    pymchelper.has_material(level, pocketMaterials.Andesite, countX, y, countZ) or \
                    pymchelper.has_material(level, pocketMaterials.Diorite, countX, y, countZ) or \
                    pymchelper.has_material(level, pocketMaterials.Granite, countX, y, countZ):
                rockAmount = rockAmount + 1
            elif pymchelper.has_material(level, pocketMaterials.Sand, countX, y, countZ) or \
                    pymchelper.has_material(level, pocketMaterials.Sandstone, countX, y, countZ):
                yellowSand = yellowSand + 1
            elif pymchelper.has_material(level, pocketMaterials.RedSand, countX, y, countZ) or \
                    pymchelper.has_material(level, pocketMaterials.RedSandstone, countX, y, countZ):
                redSand = redSand + 1
            elif pymchelper.has_material(level, pocketMaterials.HardenedClay, countX, y, countZ):
                hardenedClay = hardenedClay + 1
            elif pymchelper.has_material(level, pocketMaterials.Air, countX, y, countZ):
                airAmount = airAmount + 1
            countZ += 1
        countX += 1

    if grassAmount > maxAmount:
        maxAmount = grassAmount
        groundType = GroundType.Grass
    if rockAmount > maxAmount:
        maxAmount = rockAmount
        groundType = GroundType.Rock
    if yellowSand > maxAmount:
        maxAmount = yellowSand
        groundType = GroundType.YellowSand
    if redSand > maxAmount:
        maxAmount = redSand
        groundType = GroundType.RedSand
    if hardenedClay > maxAmount:
        maxAmount = hardenedClay
        groundType = GroundType.HardenedClay
    if maxAmount == 0:
        groundType = GroundType.Grass  # default if for some reason it can't find any ground blocks
    return groundType


def createPalette(district_data, tree_type, ground_type):
    defaultIsWood = False

    # default values
    flooring = []
    foundationBlock = []
    blockCorner = []
    blockRoof = []
    blockBesideCorner = []
    blockAtFloorLevel = []
    blockAboveFloorLevel = []
    blockUnderCeilingLevel = []
    blockBesideDoor = []
    blockAboveDoor = []
    blockUnderWindow = []
    blockAboveWindow = []
    blockForWindow = []
    blockDefault = []
    blockDoorBottomX = []
    blockDoorTopX = []
    blockDoorBottomZ = []
    blockDoorTopZ = []

    # # default values
    # flooring = [pocketMaterials.WoodPlanks]
    # foundationBlock = [pocketMaterials.Cobblestone]
    # blockCorner = [pocketMaterials.Wood]
    # blockRoof = [pocketMaterials.Brick]
    # blockBesideCorner = [pocketMaterials.WoodPlanks]
    # blockAtFloorLevel = [pocketMaterials.Wood]
    # blockAboveFloorLevel = [pocketMaterials.Wood]
    # blockUnderCeilingLevel = [pocketMaterials.WoodPlanks]
    # blockBesideDoor = [pocketMaterials.Wood]
    # blockAboveDoor = [pocketMaterials.Wood]
    # blockUnderWindow = [pocketMaterials.WoodPlanks]
    # blockAboveWindow = [pocketMaterials.WoodPlanks]
    # blockForWindow = [pocketMaterials.Glass]
    # blockDefault = [pocketMaterials.WoodPlanks]
    # blockDoorBottomX = [pocketMaterials[64, 0]]
    # blockDoorTopX = [pocketMaterials[64, 8]]
    # blockDoorBottomZ = [pocketMaterials[64, 1]]
    # blockDoorTopZ = [pocketMaterials[64, 9]]

    # foundation block
    # hardened clay
    if ground_type == GroundType.HardenedClay:
        rng = random.randint(0, 3)
        if rng == 0:
            foundationBlock.append(pocketMaterials[159, 15])
        elif rng == 1:
            foundationBlock.append(pocketMaterials[159, 7])
        elif rng == 2:
            foundationBlock.append(pocketMaterials[159, 8])
        elif rng == 3:
            foundationBlock.append(pocketMaterials[159, 0])

    # yellow sand
    elif ground_type == GroundType.YellowSand:
        if district_data.wealthScore == 0:
            foundationBlock.append(pocketMaterials.ChiseledSandstone)
        elif district_data.wealthScore == 1:
            foundationBlock.append(pocketMaterials.Sandstone)
        elif district_data.wealthScore == 2:
            foundationBlock.append(pocketMaterials.Sand)

    # red sand
    elif ground_type == GroundType.RedSand:
        if district_data.wealthScore == 0:
            foundationBlock.append(pocketMaterials.ChiseledRedSandstone)
        elif district_data.wealthScore == 1:
            foundationBlock.append(pocketMaterials.RedSandstone)
        elif district_data.wealthScore == 2:
            foundationBlock.append(pocketMaterials.RedSand)

    # rock
    elif ground_type == GroundType.Rock:
        if district_data.wealthScore == 0:
            foundationBlock.append(pocketMaterials.StoneBricks)
            foundationBlock.append(pocketMaterials.StoneBricks)
            foundationBlock.append(pocketMaterials.StoneBricks)
            foundationBlock.append(pocketMaterials.CrackedStoneBricks)
        elif district_data.wealthScore == 1:
            foundationBlock.append(pocketMaterials.Cobblestone)
            foundationBlock.append(pocketMaterials.Cobblestone)
            foundationBlock.append(pocketMaterials.Cobblestone)
            foundationBlock.append(pocketMaterials.Andesite)

        elif district_data.wealthScore == 2:
            foundationBlock.append(pocketMaterials.Gravel)

    # grass
    elif ground_type == GroundType.Grass:
        if district_data.wealthScore == 0:
            foundationBlock.append(pocketMaterials.StoneBricks)
            foundationBlock.append(pocketMaterials.StoneBricks)
            foundationBlock.append(pocketMaterials.StoneBricks)
            foundationBlock.append(pocketMaterials.MossyStoneBricks)
        elif district_data.wealthScore == 1:
            foundationBlock.append(pocketMaterials.Cobblestone)
            foundationBlock.append(pocketMaterials.Cobblestone)
            foundationBlock.append(pocketMaterials.Cobblestone)
            foundationBlock.append(pocketMaterials.MossStone)
        elif district_data.wealthScore == 2:
            foundationBlock.append(pocketMaterials.CoarseDirt)
            foundationBlock.append(pocketMaterials.CoarseDirt)
            foundationBlock.append(pocketMaterials.Granite)

    # flooring
    # hardened clay
    if ground_type == GroundType.HardenedClay:
        rng = random.randint(0, 1)
        if rng == 0:
            flooring.append(foundationBlock[0])
        elif rng == 1:
            if tree_type == TreeType.Oak:
                if district_data.wealthScore == 0 or district_data.wealthScore == 1:
                    flooring.append(pocketMaterials.WoodPlanks)
                elif district_data.wealthScore == 2:
                    flooring.append(pocketMaterials.WoodPlanks)
                    flooring.append(pocketMaterials.WoodPlanks)
                    flooring.append(pocketMaterials.WoodenStairs)
            elif tree_type == TreeType.Spruce:
                if district_data.wealthScore == 0 or district_data.wealthScore == 1:
                    flooring.append(pocketMaterials.SpruceWoodPlanks)
                elif district_data.wealthScore == 2:
                    flooring.append(pocketMaterials.SpruceWoodPlanks)
                    flooring.append(pocketMaterials.SpruceWoodPlanks)
                    flooring.append(pocketMaterials.SpruceWoodStairs)
            elif tree_type == TreeType.Birch:
                if district_data.wealthScore == 0 or district_data.wealthScore == 1:
                    flooring.append(pocketMaterials.BirchWoodPlanks)
                elif district_data.wealthScore == 2:
                    flooring.append(pocketMaterials.BirchWoodPlanks)
                    flooring.append(pocketMaterials.BirchWoodPlanks)
                    flooring.append(pocketMaterials.BirchWoodStairs)
            elif tree_type == TreeType.Jungle:
                if district_data.wealthScore == 0 or district_data.wealthScore == 1:
                    flooring.append(pocketMaterials.JungleWoodPlanks)
                elif district_data.wealthScore == 2:
                    flooring.append(pocketMaterials.JungleWoodPlanks)
                    flooring.append(pocketMaterials.JungleWoodPlanks)
                    flooring.append(pocketMaterials.JungleWoodStairs)
            elif tree_type == TreeType.Acacia:
                if district_data.wealthScore == 0 or district_data.wealthScore == 1:
                    flooring.append(pocketMaterials.AcaciaWoodPlanks)
                elif district_data.wealthScore == 2:
                    flooring.append(pocketMaterials.AcaciaWoodPlanks)
                    flooring.append(pocketMaterials.AcaciaWoodPlanks)
                    flooring.append(pocketMaterials.AcaciaWoodStairs)
            elif tree_type == TreeType.DarkOak:
                if district_data.wealthScore == 0 or district_data.wealthScore == 1:
                    flooring.append(pocketMaterials.DarkOakWoodPlanks)
                elif district_data.wealthScore == 2:
                    flooring.append(pocketMaterials.DarkOakWoodPlanks)
                    flooring.append(pocketMaterials.DarkOakWoodPlanks)
                    flooring.append(pocketMaterials.DarkOakWoodStairs)
            else:
                flooring.append(foundationBlock[0])

    # rock
    elif ground_type == GroundType.Rock:
        rng = random.randint(0, 1)
        if rng == 0:
            if district_data.wealthScore != 2:
                flooring.append(foundationBlock[0])
            else:
                flooring.append(pocketMaterials.Andesite)
        elif rng == 1:
            if tree_type == TreeType.Oak:
                if district_data.wealthScore == 0 or district_data.wealthScore == 1:
                    flooring.append(pocketMaterials.WoodPlanks)
                elif district_data.wealthScore == 2:
                    flooring.append(pocketMaterials.WoodPlanks)
                    flooring.append(pocketMaterials.WoodPlanks)
                    flooring.append(pocketMaterials.WoodenStairs)
            elif tree_type == TreeType.Spruce:
                if district_data.wealthScore == 0 or district_data.wealthScore == 1:
                    flooring.append(pocketMaterials.SpruceWoodPlanks)
                elif district_data.wealthScore == 2:
                    flooring.append(pocketMaterials.SpruceWoodPlanks)
                    flooring.append(pocketMaterials.SpruceWoodPlanks)
                    flooring.append(pocketMaterials.SpruceWoodStairs)
            elif tree_type == TreeType.Birch:
                if district_data.wealthScore == 0 or district_data.wealthScore == 1:
                    flooring.append(pocketMaterials.BirchWoodPlanks)
                elif district_data.wealthScore == 2:
                    flooring.append(pocketMaterials.BirchWoodPlanks)
                    flooring.append(pocketMaterials.BirchWoodPlanks)
                    flooring.append(pocketMaterials.BirchWoodStairs)
            elif tree_type == TreeType.Jungle:
                if district_data.wealthScore == 0 or district_data.wealthScore == 1:
                    flooring.append(pocketMaterials.JungleWoodPlanks)
                elif district_data.wealthScore == 2:
                    flooring.append(pocketMaterials.JungleWoodPlanks)
                    flooring.append(pocketMaterials.JungleWoodPlanks)
                    flooring.append(pocketMaterials.JungleWoodStairs)
            elif tree_type == TreeType.Acacia:
                if district_data.wealthScore == 0 or district_data.wealthScore == 1:
                    flooring.append(pocketMaterials.AcaciaWoodPlanks)
                elif district_data.wealthScore == 2:
                    flooring.append(pocketMaterials.AcaciaWoodPlanks)
                    flooring.append(pocketMaterials.AcaciaWoodPlanks)
                    flooring.append(pocketMaterials.AcaciaWoodStairs)
            elif tree_type == TreeType.DarkOak:
                if district_data.wealthScore == 0 or district_data.wealthScore == 1:
                    flooring.append(pocketMaterials.DarkOakWoodPlanks)
                elif district_data.wealthScore == 2:
                    flooring.append(pocketMaterials.DarkOakWoodPlanks)
                    flooring.append(pocketMaterials.DarkOakWoodPlanks)
                    flooring.append(pocketMaterials.DarkOakWoodStairs)
            else:
                flooring.append(foundationBlock[0])

    # yellow sand
    elif ground_type == GroundType.YellowSand:
        rng = random.randint(0, 1)
        if rng == 0:
            if district_data.wealthScore != 2:
                flooring.append(foundationBlock[0])
            else:
                flooring.append(pocketMaterials.Sandstone)
        elif rng == 1:
            if tree_type == TreeType.Oak:
                if district_data.wealthScore == 0 or district_data.wealthScore == 1:
                    flooring.append(pocketMaterials.WoodPlanks)
                elif district_data.wealthScore == 2:
                    flooring.append(pocketMaterials.WoodPlanks)
                    flooring.append(pocketMaterials.WoodPlanks)
                    flooring.append(pocketMaterials.WoodenStairs)
            elif tree_type == TreeType.Spruce:
                if district_data.wealthScore == 0 or district_data.wealthScore == 1:
                    flooring.append(pocketMaterials.SpruceWoodPlanks)
                elif district_data.wealthScore == 2:
                    flooring.append(pocketMaterials.SpruceWoodPlanks)
                    flooring.append(pocketMaterials.SpruceWoodPlanks)
                    flooring.append(pocketMaterials.SpruceWoodStairs)
            elif tree_type == TreeType.Birch:
                if district_data.wealthScore == 0 or district_data.wealthScore == 1:
                    flooring.append(pocketMaterials.BirchWoodPlanks)
                elif district_data.wealthScore == 2:
                    flooring.append(pocketMaterials.BirchWoodPlanks)
                    flooring.append(pocketMaterials.BirchWoodPlanks)
                    flooring.append(pocketMaterials.BirchWoodStairs)
            elif tree_type == TreeType.Jungle:
                if district_data.wealthScore == 0 or district_data.wealthScore == 1:
                    flooring.append(pocketMaterials.JungleWoodPlanks)
                elif district_data.wealthScore == 2:
                    flooring.append(pocketMaterials.JungleWoodPlanks)
                    flooring.append(pocketMaterials.JungleWoodPlanks)
                    flooring.append(pocketMaterials.JungleWoodStairs)
            elif tree_type == TreeType.Acacia:
                if district_data.wealthScore == 0 or district_data.wealthScore == 1:
                    flooring.append(pocketMaterials.AcaciaWoodPlanks)
                elif district_data.wealthScore == 2:
                    flooring.append(pocketMaterials.AcaciaWoodPlanks)
                    flooring.append(pocketMaterials.AcaciaWoodPlanks)
                    flooring.append(pocketMaterials.AcaciaWoodStairs)
            elif tree_type == TreeType.DarkOak:
                if district_data.wealthScore == 0 or district_data.wealthScore == 1:
                    flooring.append(pocketMaterials.DarkOakWoodPlanks)
                elif district_data.wealthScore == 2:
                    flooring.append(pocketMaterials.DarkOakWoodPlanks)
                    flooring.append(pocketMaterials.DarkOakWoodPlanks)
                    flooring.append(pocketMaterials.DarkOakWoodStairs)
            else:
                flooring.append(pocketMaterials.Sandstone)

    # red sand
    elif ground_type == GroundType.RedSand:
        rng = random.randint(0, 1)
        if rng == 0:
            if district_data.wealthScore != 2:
                flooring.append(foundationBlock[0])
            else:
                flooring.append(pocketMaterials.RedSandstone)
        elif rng == 1:
            if tree_type == TreeType.Oak:
                if district_data.wealthScore == 0 or district_data.wealthScore == 1:
                    flooring.append(pocketMaterials.WoodPlanks)
                elif district_data.wealthScore == 2:
                    flooring.append(pocketMaterials.WoodPlanks)
                    flooring.append(pocketMaterials.WoodPlanks)
                    flooring.append(pocketMaterials.WoodenStairs)
            elif tree_type == TreeType.Spruce:
                if district_data.wealthScore == 0 or district_data.wealthScore == 1:
                    flooring.append(pocketMaterials.SpruceWoodPlanks)
                elif district_data.wealthScore == 2:
                    flooring.append(pocketMaterials.SpruceWoodPlanks)
                    flooring.append(pocketMaterials.SpruceWoodPlanks)
                    flooring.append(pocketMaterials.SpruceWoodStairs)
            elif tree_type == TreeType.Birch:
                if district_data.wealthScore == 0 or district_data.wealthScore == 1:
                    flooring.append(pocketMaterials.BirchWoodPlanks)
                elif district_data.wealthScore == 2:
                    flooring.append(pocketMaterials.BirchWoodPlanks)
                    flooring.append(pocketMaterials.BirchWoodPlanks)
                    flooring.append(pocketMaterials.BirchWoodStairs)
            elif tree_type == TreeType.Jungle:
                if district_data.wealthScore == 0 or district_data.wealthScore == 1:
                    flooring.append(pocketMaterials.JungleWoodPlanks)
                elif district_data.wealthScore == 2:
                    flooring.append(pocketMaterials.JungleWoodPlanks)
                    flooring.append(pocketMaterials.JungleWoodPlanks)
                    flooring.append(pocketMaterials.JungleWoodStairs)
            elif tree_type == TreeType.Acacia:
                if district_data.wealthScore == 0 or district_data.wealthScore == 1:
                    flooring.append(pocketMaterials.AcaciaWoodPlanks)
                elif district_data.wealthScore == 2:
                    flooring.append(pocketMaterials.AcaciaWoodPlanks)
                    flooring.append(pocketMaterials.AcaciaWoodPlanks)
                    flooring.append(pocketMaterials.AcaciaWoodStairs)
            elif tree_type == TreeType.DarkOak:
                if district_data.wealthScore == 0 or district_data.wealthScore == 1:
                    flooring.append(pocketMaterials.DarkOakWoodPlanks)
                elif district_data.wealthScore == 2:
                    flooring.append(pocketMaterials.DarkOakWoodPlanks)
                    flooring.append(pocketMaterials.DarkOakWoodPlanks)
                    flooring.append(pocketMaterials.DarkOakWoodStairs)
            else:
                flooring.append(pocketMaterials.RedSandstone)

    # grass
    elif ground_type == GroundType.Grass:
        if tree_type == TreeType.Oak:
            if district_data.wealthScore == 0 or district_data.wealthScore == 1:
                flooring.append(pocketMaterials.WoodPlanks)
            elif district_data.wealthScore == 2:
                flooring.append(pocketMaterials.WoodPlanks)
                flooring.append(pocketMaterials.WoodPlanks)
                flooring.append(pocketMaterials.WoodenStairs)
        elif tree_type == TreeType.Spruce:
            if district_data.wealthScore == 0 or district_data.wealthScore == 1:
                flooring.append(pocketMaterials.SpruceWoodPlanks)
            elif district_data.wealthScore == 2:
                flooring.append(pocketMaterials.SpruceWoodPlanks)
                flooring.append(pocketMaterials.SpruceWoodPlanks)
                flooring.append(pocketMaterials.SpruceWoodStairs)
        elif tree_type == TreeType.Birch:
            if district_data.wealthScore == 0 or district_data.wealthScore == 1:
                flooring.append(pocketMaterials.BirchWoodPlanks)
            elif district_data.wealthScore == 2:
                flooring.append(pocketMaterials.BirchWoodPlanks)
                flooring.append(pocketMaterials.BirchWoodPlanks)
                flooring.append(pocketMaterials.BirchWoodStairs)
        elif tree_type == TreeType.Jungle:
            if district_data.wealthScore == 0 or district_data.wealthScore == 1:
                flooring.append(pocketMaterials.JungleWoodPlanks)
            elif district_data.wealthScore == 2:
                flooring.append(pocketMaterials.JungleWoodPlanks)
                flooring.append(pocketMaterials.JungleWoodPlanks)
                flooring.append(pocketMaterials.JungleWoodStairs)
        elif tree_type == TreeType.Acacia:
            if district_data.wealthScore == 0 or district_data.wealthScore == 1:
                flooring.append(pocketMaterials.AcaciaWoodPlanks)
            elif district_data.wealthScore == 2:
                flooring.append(pocketMaterials.AcaciaWoodPlanks)
                flooring.append(pocketMaterials.AcaciaWoodPlanks)
                flooring.append(pocketMaterials.AcaciaWoodStairs)
        elif tree_type == TreeType.DarkOak:
            if district_data.wealthScore == 0 or district_data.wealthScore == 1:
                flooring.append(pocketMaterials.DarkOakWoodPlanks)
            elif district_data.wealthScore == 2:
                flooring.append(pocketMaterials.DarkOakWoodPlanks)
                flooring.append(pocketMaterials.DarkOakWoodPlanks)
                flooring.append(pocketMaterials.DarkOakWoodStairs)
        else:
            flooring.append(pocketMaterials.CoarseDirt)

    # corner block
    if ground_type == GroundType.Grass or ground_type == GroundType.Rock:
        rng = random.randint(0, 1)
        if district_data.wealthScore == 0:
            blockCorner.append(pocketMaterials.DoubleStoneSlab)
        elif district_data.wealthScore == 1:
            if rng == 0:
                blockCorner.append(pocketMaterials.Cobblestone)
            elif rng == 1:
                if tree_type == TreeType.Oak:
                    blockCorner.append(pocketMaterials.Wood)
                elif tree_type == TreeType.Spruce:
                    blockCorner.append(pocketMaterials.SpruceWood)
                elif tree_type == TreeType.Birch:
                    blockCorner.append(pocketMaterials.BirchWood)
                elif tree_type == TreeType.Jungle:
                    blockCorner.append(pocketMaterials.JungleWood)
                elif tree_type == TreeType.Acacia:
                    blockCorner.append(pocketMaterials.AcaciaWood)
                elif tree_type == TreeType.DarkOak:
                    blockCorner.append(pocketMaterials.DarkOakWood)
                else:
                    blockCorner.append(pocketMaterials.Cobblestone)
        elif district_data.wealthScore == 2:
            if rng == 0:
                if tree_type == TreeType.Oak:
                    blockCorner.append(pocketMaterials.Wood)
                elif tree_type == TreeType.Spruce:
                    blockCorner.append(pocketMaterials.SpruceWood)
                elif tree_type == TreeType.Birch:
                    blockCorner.append(pocketMaterials.BirchWood)
                elif tree_type == TreeType.Jungle:
                    blockCorner.append(pocketMaterials.JungleWood)
                elif tree_type == TreeType.Acacia:
                    blockCorner.append(pocketMaterials.AcaciaWood)
                elif tree_type == TreeType.DarkOak:
                    blockCorner.append(pocketMaterials.DarkOakWood)
                else:
                    blockCorner.append(pocketMaterials.Cobblestone)
            elif rng == 1:
                if tree_type == TreeType.Oak:
                    blockCorner.append(pocketMaterials.WoodPlanks)
                elif tree_type == TreeType.Spruce:
                    blockCorner.append(pocketMaterials.SpruceWoodPlanks)
                elif tree_type == TreeType.Birch:
                    blockCorner.append(pocketMaterials.BirchWoodPlanks)
                elif tree_type == TreeType.Jungle:
                    blockCorner.append(pocketMaterials.JungleWoodPlanks)
                elif tree_type == TreeType.Acacia:
                    blockCorner.append(pocketMaterials.AcaciaWoodPlanks)
                elif tree_type == TreeType.DarkOak:
                    blockCorner.append(pocketMaterials.DarkOakWoodPlanks)
                else:
                    blockCorner.append(pocketMaterials.Cobblestone)

    elif ground_type == GroundType.HardenedClay:
        if district_data.wealthScore == 2:
            blockCorner.append(foundationBlock[0])
        else:
            if tree_type == TreeType.Oak:
                blockCorner.append(pocketMaterials.Wood)
            elif tree_type == TreeType.Spruce:
                blockCorner.append(pocketMaterials.SpruceWood)
            elif tree_type == TreeType.Birch:
                blockCorner.append(pocketMaterials.BirchWood)
            elif tree_type == TreeType.Jungle:
                blockCorner.append(pocketMaterials.JungleWood)
            elif tree_type == TreeType.Acacia:
                blockCorner.append(pocketMaterials.AcaciaWood)
            elif tree_type == TreeType.DarkOak:
                blockCorner.append(pocketMaterials.DarkOakWood)
            else:
                blockCorner.append(foundationBlock[0])

    elif ground_type == GroundType.YellowSand or ground_type == GroundType.RedSand:
        if district_data.wealthScore == 0:
            if tree_type == TreeType.Oak:
                blockCorner.append(pocketMaterials.Wood)
            elif tree_type == TreeType.Spruce:
                blockCorner.append(pocketMaterials.SpruceWood)
            elif tree_type == TreeType.Birch:
                blockCorner.append(pocketMaterials.BirchWood)
            elif tree_type == TreeType.Jungle:
                blockCorner.append(pocketMaterials.JungleWood)
            elif tree_type == TreeType.Acacia:
                blockCorner.append(pocketMaterials.AcaciaWood)
            elif tree_type == TreeType.DarkOak:
                blockCorner.append(pocketMaterials.DarkOakWood)
            else:
                blockCorner.append(foundationBlock[0])
        elif district_data.wealthScore != 0:
            rng = random.randint(0, 1)
            if ground_type == GroundType.YellowSand:
                if rng == 0:
                    blockCorner.append(pocketMaterials.ChiseledSandstone)
                elif rng == 1:
                    blockCorner.append(pocketMaterials.SmoothSandstone)
            elif ground_type == GroundType.RedSand:
                if rng == 0:
                    blockCorner.append(pocketMaterials.ChiseledRedSandstone)
                elif rng == 1:
                    blockCorner.append(pocketMaterials.SmoothRedSandstone)

    # roof block
    if ground_type == GroundType.Grass:
        if district_data.wealthScore == 0:
            blockRoof.append(pocketMaterials.Brick)
        elif district_data.wealthScore == 1:
            if tree_type == TreeType.Oak:
                blockRoof.append(pocketMaterials.WoodPlanks)
            elif tree_type == TreeType.Spruce:
                blockRoof.append(pocketMaterials.SpruceWoodPlanks)
            elif tree_type == TreeType.Birch:
                blockRoof.append(pocketMaterials.BirchWoodPlanks)
            elif tree_type == TreeType.Jungle:
                blockRoof.append(pocketMaterials.JungleWoodPlanks)
            elif tree_type == TreeType.Acacia:
                blockRoof.append(pocketMaterials.AcaciaWoodPlanks)
            elif tree_type == TreeType.DarkOak:
                blockRoof.append(pocketMaterials.DarkOakWoodPlanks)
            else:
                rng = random.randint(0, 1)
                if rng == 0:
                    blockRoof.append(pocketMaterials.YellowWool)
                elif rng == 1:
                    blockRoof.append(pocketMaterials.BrownWool)
        elif district_data.wealthScore == 2:
            rng = random.randint(0, 1)
            if rng == 0:
                blockRoof.append(pocketMaterials.YellowWool)
            elif rng == 1:
                blockRoof.append(pocketMaterials.BrownWool)

    elif ground_type == GroundType.HardenedClay:
        rng = random.randint(0, 2)
        if rng == 0:
            blockRoof.append(pocketMaterials.YellowHardenedClay)
        elif rng == 1:
            blockRoof.append(pocketMaterials.OrangeHardenedClay)
        elif rng == 2:
            blockRoof.append(pocketMaterials.LimeHardenedClay)

    elif ground_type == GroundType.Rock:
        rng = random.randint(0, 1)
        if rng == 0:
            if district_data.wealthScore == 0:
                blockRoof.append(pocketMaterials.DoubleStoneSlab)
            elif district_data.wealthScore == 1:
                blockRoof.append(pocketMaterials.Stone)
            elif district_data.wealthScore == 2:
                rng = random.randint(0, 1)
                if rng == 0:
                    blockRoof.append(pocketMaterials.Andesite)
                elif rng == 1:
                    blockRoof.append(pocketMaterials.Granite)
        elif rng == 1:
            if tree_type == TreeType.Oak:
                blockRoof.append(pocketMaterials.WoodPlanks)
            elif tree_type == TreeType.Spruce:
                blockRoof.append(pocketMaterials.SpruceWoodPlanks)
            elif tree_type == TreeType.Birch:
                blockRoof.append(pocketMaterials.BirchWoodPlanks)
            elif tree_type == TreeType.Jungle:
                blockRoof.append(pocketMaterials.JungleWoodPlanks)
            elif tree_type == TreeType.Acacia:
                blockRoof.append(pocketMaterials.AcaciaWoodPlanks)
            elif tree_type == TreeType.DarkOak:
                blockRoof.append(pocketMaterials.DarkOakWoodPlanks)
            else:
                rng = random.randint(0, 1)
                if rng == 0:
                    blockRoof.append(pocketMaterials.Andesite)
                elif rng == 1:
                    blockRoof.append(pocketMaterials.Granite)

    elif ground_type == GroundType.YellowSand:
        rng = random.randint(0, 1)
        if district_data.wealthScore == 0:
            blockRoof.append(pocketMaterials.SmoothSandstone)
        elif district_data.wealthScore == 1:
            if rng == 0:
                blockRoof.append(pocketMaterials.Sandstone)
            elif rng == 1:
                if tree_type == TreeType.Oak:
                    blockRoof.append(pocketMaterials.WoodPlanks)
                elif tree_type == TreeType.Spruce:
                    blockRoof.append(pocketMaterials.SpruceWoodPlanks)
                elif tree_type == TreeType.Birch:
                    blockRoof.append(pocketMaterials.BirchWoodPlanks)
                elif tree_type == TreeType.Jungle:
                    blockRoof.append(pocketMaterials.JungleWoodPlanks)
                elif tree_type == TreeType.Acacia:
                    blockRoof.append(pocketMaterials.AcaciaWoodPlanks)
                elif tree_type == TreeType.DarkOak:
                    blockRoof.append(pocketMaterials.DarkOakWoodPlanks)
                else:
                    blockRoof.append(pocketMaterials.Sandstone)
        elif district_data.wealthScore == 2:
            if tree_type == TreeType.Oak:
                blockRoof.append(pocketMaterials.WoodPlanks)
            elif tree_type == TreeType.Spruce:
                blockRoof.append(pocketMaterials.SpruceWoodPlanks)
            elif tree_type == TreeType.Birch:
                blockRoof.append(pocketMaterials.BirchPlanks)
            elif tree_type == TreeType.Jungle:
                blockRoof.append(pocketMaterials.JunglePlanks)
            elif tree_type == TreeType.Acacia:
                blockRoof.append(pocketMaterials.AcaciaPlanks)
            elif tree_type == TreeType.DarkOak:
                blockRoof.append(pocketMaterials.DarkOakPlanks)
            else:
                blockRoof.append(pocketMaterials.Sandstone)

    elif ground_type == GroundType.RedSand:
        rng = random.randint(0, 1)
        if district_data.wealthScore == 0:
            blockRoof.append(pocketMaterials.SmoothRedSandstone)
        elif district_data.wealthScore == 1:
            if rng == 0:
                blockRoof.append(pocketMaterials.RedSandstone)
            elif rng == 1:
                if tree_type == TreeType.Oak:
                    blockRoof.append(pocketMaterials.WoodPlanks)
                elif tree_type == TreeType.Spruce:
                    blockRoof.append(pocketMaterials.SpruceWoodPlanks)
                elif tree_type == TreeType.Birch:
                    blockRoof.append(pocketMaterials.BirchWoodPlanks)
                elif tree_type == TreeType.Jungle:
                    blockRoof.append(pocketMaterials.JungleWoodPlanks)
                elif tree_type == TreeType.Acacia:
                    blockRoof.append(pocketMaterials.AcaciaWoodPlanks)
                elif tree_type == TreeType.DarkOak:
                    blockRoof.append(pocketMaterials.DarkOakWoodPlanks)
                else:
                    blockRoof.append(pocketMaterials.RedSandstone)
        elif district_data.wealthScore == 2:
            if tree_type == TreeType.Oak:
                blockRoof.append(pocketMaterials.WoodPlanks)
            elif tree_type == TreeType.Spruce:
                blockRoof.append(pocketMaterials.SpruceWoodPlanks)
            elif tree_type == TreeType.Birch:
                blockRoof.append(pocketMaterials.BirchPlanks)
            elif tree_type == TreeType.Jungle:
                blockRoof.append(pocketMaterials.JunglePlanks)
            elif tree_type == TreeType.Acacia:
                blockRoof.append(pocketMaterials.AcaciaPlanks)
            elif tree_type == TreeType.DarkOak:
                blockRoof.append(pocketMaterials.DarkOakPlanks)
            else:
                blockRoof.append(pocketMaterials.RedSandstone)

    # Block default
    # grass
    if ground_type == GroundType.Grass:
        rng = random.randint(0, 1)
        if district_data.wealthScore == 0:
            if rng == 0:
                blockDefault.append(pocketMaterials.StoneBricks)
            elif rng == 1:
                defaultIsWood = True
                if tree_type == TreeType.Oak:
                    blockDefault.append(pocketMaterials.Wood)
                elif tree_type == TreeType.Spruce:
                    blockDefault.append(pocketMaterials.SpruceWood)
                elif tree_type == TreeType.Birch:
                    blockDefault.append(pocketMaterials.BirchWood)
                elif tree_type == TreeType.Jungle:
                    blockDefault.append(pocketMaterials.JungleWood)
                elif tree_type == TreeType.Acacia:
                    blockDefault.append(pocketMaterials.AcaciaWood)
                elif tree_type == TreeType.DarkOak:
                    blockDefault.append(pocketMaterials.DarkOakWood)
                else:
                    defaultIsWood = False
                    blockDefault.append(pocketMaterials.StoneBricks)
        elif district_data.wealthScore == 1:
            if tree_type == TreeType.Oak:
                blockDefault.append(pocketMaterials.WoodPlanks)
            elif tree_type == TreeType.Spruce:
                blockDefault.append(pocketMaterials.SpruceWoodPlanks)
            elif tree_type == TreeType.Birch:
                blockDefault.append(pocketMaterials.BirchWoodPlanks)
            elif tree_type == TreeType.Jungle:
                blockDefault.append(pocketMaterials.JungleWoodPlanks)
            elif tree_type == TreeType.Acacia:
                blockDefault.append(pocketMaterials.AcaciaWoodPlanks)
            elif tree_type == TreeType.DarkOak:
                blockDefault.append(pocketMaterials.DarkOakWoodPlanks)
            else:
                blockDefault.append(pocketMaterials.StoneBricks)
        elif district_data.wealthScore == 2:
            if tree_type == TreeType.Oak:
                blockDefault.append(pocketMaterials.WoodPlanks)
            elif tree_type == TreeType.Spruce:
                blockDefault.append(pocketMaterials.SpruceWoodPlanks)
            elif tree_type == TreeType.Birch:
                blockDefault.append(pocketMaterials.BirchWoodPlanks)
            elif tree_type == TreeType.Jungle:
                blockDefault.append(pocketMaterials.JungleWoodPlanks)
            elif tree_type == TreeType.Acacia:
                blockDefault.append(pocketMaterials.AcaciaWoodPlanks)
            elif tree_type == TreeType.DarkOak:
                blockDefault.append(pocketMaterials.DarkOakWoodPlanks)
            else:
                blockDefault.append(pocketMaterials.CoarseDirt)

    # yellow sand / red sand
    elif ground_type == GroundType.YellowSand or ground_type == GroundType.RedSand:
        if district_data.wealthScore == 0:
            blockDefault.append(pocketMaterials.Stone)
        elif district_data.wealthScore == 1:
            rng = random.randint(0, 1)
            if rng == 0:
                if ground_type == GroundType.YellowSand:
                    blockDefault.append(pocketMaterials.Sandstone)
                else:
                    blockDefault.append(pocketMaterials.RedSandstone)
            elif rng == 1:
                if tree_type == TreeType.Oak:
                    blockDefault.append(pocketMaterials.WoodPlanks)
                elif tree_type == TreeType.Spruce:
                    blockDefault.append(pocketMaterials.SpruceWoodPlanks)
                elif tree_type == TreeType.Birch:
                    blockDefault.append(pocketMaterials.BirchWoodPlanks)
                elif tree_type == TreeType.Jungle:
                    blockDefault.append(pocketMaterials.JungleWoodPlanks)
                elif tree_type == TreeType.Acacia:
                    blockDefault.append(pocketMaterials.AcaciaWoodPlanks)
                elif tree_type == TreeType.DarkOak:
                    blockDefault.append(pocketMaterials.DarkOakWoodPlanks)
                else:
                    if ground_type == GroundType.YellowSand:
                        blockDefault.append(pocketMaterials.Sandstone)
                    else:
                        blockDefault.append(pocketMaterials.RedSandstone)
        elif district_data.wealthScore == 2:
            if ground_type == GroundType.YellowSand:
                blockDefault.append(pocketMaterials.Sand)
                blockDefault.append(pocketMaterials.Sand)
                blockDefault.append(pocketMaterials.Sandstone)
            else:
                blockDefault.append(pocketMaterials.RedSand)
                blockDefault.append(pocketMaterials.RedSand)
                blockDefault.append(pocketMaterials.RedSandstone)

    # hardened clay
    elif ground_type == GroundType.HardenedClay:
        rng = random.randint(0, 1)
        if rng == 0:
            blockDefault.append(foundationBlock[0])
        elif rng == 1:
            if tree_type == TreeType.Oak:
                blockDefault.append(pocketMaterials.WoodPlanks)
            elif tree_type == TreeType.Spruce:
                blockDefault.append(pocketMaterials.SpruceWoodPlanks)
            elif tree_type == TreeType.Birch:
                blockDefault.append(pocketMaterials.BirchWoodPlanks)
            elif tree_type == TreeType.Jungle:
                blockDefault.append(pocketMaterials.JungleWoodPlanks)
            elif tree_type == TreeType.Acacia:
                blockDefault.append(pocketMaterials.AcaciaWoodPlanks)
            elif tree_type == TreeType.DarkOak:
                blockDefault.append(pocketMaterials.DarkOakWoodPlanks)
            else:
                blockDefault.append(foundationBlock[0])

    # rock
    elif ground_type == GroundType.Rock:
        rng = random.randint(0, 1)
        if district_data.wealthScore != 2:
            if rng == 0:
                blockDefault.append(pocketMaterials.Stone)
            elif rng == 1:
                if tree_type == TreeType.Oak:
                    blockDefault.append(pocketMaterials.WoodPlanks)
                elif tree_type == TreeType.Spruce:
                    blockDefault.append(pocketMaterials.SpruceWoodPlanks)
                elif tree_type == TreeType.Birch:
                    blockDefault.append(pocketMaterials.BirchWoodPlanks)
                elif tree_type == TreeType.Jungle:
                    blockDefault.append(pocketMaterials.JungleWoodPlanks)
                elif tree_type == TreeType.Acacia:
                    blockDefault.append(pocketMaterials.AcaciaWoodPlanks)
                elif tree_type == TreeType.DarkOak:
                    blockDefault.append(pocketMaterials.DarkOakWoodPlanks)
                else:
                    blockDefault.append(pocketMaterials.Stone)
        elif district_data.wealthScore == 2:
            blockDefault.append(pocketMaterials.Stone)
            blockDefault.append(pocketMaterials.Andesite)

    # block beside corner
    # grass
    if ground_type == GroundType.Grass:
        if defaultIsWood:
            blockBesideCorner.append(blockDefault[0])
        else:
            rng = random.randint(0, 1)
            if rng == 0:
                blockBesideCorner.append(blockDefault[0])
            elif rng == 1:
                if tree_type == TreeType.Oak:
                    blockBesideCorner.append(pocketMaterials.Wood)
                elif tree_type == TreeType.Spruce:
                    blockBesideCorner.append(pocketMaterials.SpruceWood)
                elif tree_type == TreeType.Birch:
                    blockBesideCorner.append(pocketMaterials.BirchWood)
                elif tree_type == TreeType.Jungle:
                    blockBesideCorner.append(pocketMaterials.JungleWood)
                elif tree_type == TreeType.Acacia:
                    blockBesideCorner.append(pocketMaterials.AcaciaWood)
                elif tree_type == TreeType.DarkOak:
                    blockBesideCorner.append(pocketMaterials.DarkOakWood)
                else:
                    blockBesideCorner.append(blockDefault[0])

    # yellow sand / red sand
    elif ground_type == GroundType.YellowSand or ground_type == GroundType.RedSand:
        rng = random.randint(0, 1)
        if district_data.wealthScore == 0:
            blockBesideCorner.append(pocketMaterials.StoneBricks)
        elif district_data.wealthScore == 1:
            if rng == 0:
                if ground_type == GroundType.YellowSand:
                    blockBesideCorner.append(pocketMaterials.ChiseledSandstone)
                else:
                    blockBesideCorner.append(pocketMaterials.ChiseledRedSandstone)
            elif rng == 1:
                if tree_type == TreeType.Oak:
                    blockBesideCorner.append(pocketMaterials.Wood)
                elif tree_type == TreeType.Spruce:
                    blockBesideCorner.append(pocketMaterials.SpruceWood)
                elif tree_type == TreeType.Birch:
                    blockBesideCorner.append(pocketMaterials.BirchWood)
                elif tree_type == TreeType.Jungle:
                    blockBesideCorner.append(pocketMaterials.JungleWood)
                elif tree_type == TreeType.Acacia:
                    blockBesideCorner.append(pocketMaterials.AcaciaWood)
                elif tree_type == TreeType.DarkOak:
                    blockBesideCorner.append(pocketMaterials.DarkOakWood)
                else:
                    blockBesideCorner.append(blockDefault[0])
        elif district_data.wealthScore == 2:
            if rng == 0:
                if ground_type == GroundType.YellowSand:
                    blockBesideCorner.append(pocketMaterials.Sandstone)
                else:
                    blockBesideCorner.append(pocketMaterials.RedSandstone)
            elif rng == 1:
                blockBesideCorner.append(blockDefault[0])

    # hardened clay
    elif ground_type == GroundType.HardenedClay:
        rng = random.randint(0, 1)
        if rng == 0:
            blockBesideCorner.append(blockDefault[0])
        elif rng == 1:
            if tree_type == TreeType.Oak:
                blockBesideCorner.append(pocketMaterials.Wood)
            elif tree_type == TreeType.Spruce:
                blockBesideCorner.append(pocketMaterials.SpruceWood)
            elif tree_type == TreeType.Birch:
                blockBesideCorner.append(pocketMaterials.BirchWood)
            elif tree_type == TreeType.Jungle:
                blockBesideCorner.append(pocketMaterials.JungleWood)
            elif tree_type == TreeType.Acacia:
                blockBesideCorner.append(pocketMaterials.AcaciaWood)
            elif tree_type == TreeType.DarkOak:
                blockBesideCorner.append(pocketMaterials.DarkOakWood)
            else:
                blockBesideCorner.append(blockDefault[0])

    # rock
    elif ground_type == GroundType.Rock:
        rng = random.randint(0, 1)
        if district_data.wealthScore != 2:
            if rng == 0:
                blockBesideCorner.append(blockDefault[0])
            elif rng == 1:
                if tree_type == TreeType.Oak:
                    blockBesideCorner.append(pocketMaterials.Wood)
                elif tree_type == TreeType.Spruce:
                    blockBesideCorner.append(pocketMaterials.SpruceWood)
                elif tree_type == TreeType.Birch:
                    blockBesideCorner.append(pocketMaterials.BirchWood)
                elif tree_type == TreeType.Jungle:
                    blockBesideCorner.append(pocketMaterials.JungleWood)
                elif tree_type == TreeType.Acacia:
                    blockBesideCorner.append(pocketMaterials.AcaciaWood)
                elif tree_type == TreeType.DarkOak:
                    blockBesideCorner.append(pocketMaterials.DarkOakWood)
                else:
                    blockBesideCorner.append(blockDefault[0])
        elif district_data.wealthScore == 2:
            blockBesideCorner.append(pocketMaterials.Stone)
            blockBesideCorner.append(pocketMaterials.Andesite)

    # block above floor level
    # grass
    if ground_type == GroundType.Grass:
        if defaultIsWood:
            blockAboveFloorLevel.append(blockDefault[0])
        else:
            rng = random.randint(0, 1)
            if rng == 0:
                blockAboveFloorLevel.append(blockDefault[0])
            elif rng == 1:
                if tree_type == TreeType.Oak:
                    blockAboveFloorLevel.append(pocketMaterials.Wood)
                elif tree_type == TreeType.Spruce:
                    blockAboveFloorLevel.append(pocketMaterials.SpruceWood)
                elif tree_type == TreeType.Birch:
                    blockAboveFloorLevel.append(pocketMaterials.BirchWood)
                elif tree_type == TreeType.Jungle:
                    blockAboveFloorLevel.append(pocketMaterials.JungleWood)
                elif tree_type == TreeType.Acacia:
                    blockAboveFloorLevel.append(pocketMaterials.AcaciaWood)
                elif tree_type == TreeType.DarkOak:
                    blockAboveFloorLevel.append(pocketMaterials.DarkOakWood)
                else:
                    blockAboveFloorLevel.append(blockDefault[0])

    # yellow sand / red sand
    elif ground_type == GroundType.YellowSand or ground_type == GroundType.RedSand:
        rng = random.randint(0, 1)
        if district_data.wealthScore == 0:
            blockAboveFloorLevel.append(pocketMaterials.StoneBricks)
        elif district_data.wealthScore == 1:
            if rng == 0:
                if ground_type == GroundType.YellowSand:
                    blockAboveFloorLevel.append(pocketMaterials.ChiseledSandstone)
                else:
                    blockAboveFloorLevel.append(pocketMaterials.ChiseledRedSandstone)
            elif rng == 1:
                if tree_type == TreeType.Oak:
                    blockAboveFloorLevel.append(pocketMaterials.Wood)
                elif tree_type == TreeType.Spruce:
                    blockAboveFloorLevel.append(pocketMaterials.SpruceWood)
                elif tree_type == TreeType.Birch:
                    blockAboveFloorLevel.append(pocketMaterials.BirchWood)
                elif tree_type == TreeType.Jungle:
                    blockAboveFloorLevel.append(pocketMaterials.JungleWood)
                elif tree_type == TreeType.Acacia:
                    blockAboveFloorLevel.append(pocketMaterials.AcaciaWood)
                elif tree_type == TreeType.DarkOak:
                    blockAboveFloorLevel.append(pocketMaterials.DarkOakWood)
                else:
                    blockAboveFloorLevel.append(blockDefault[0])
        elif district_data.wealthScore == 2:
            if rng == 0:
                if ground_type == GroundType.YellowSand:
                    blockAboveFloorLevel.append(pocketMaterials.Sandstone)
                else:
                    blockAboveFloorLevel.append(pocketMaterials.RedSandstone)
            elif rng == 1:
                blockAboveFloorLevel.append(blockDefault[0])

    # hardened clay
    elif ground_type == GroundType.HardenedClay:
        rng = random.randint(0, 1)
        if rng == 0:
            blockAboveFloorLevel.append(blockDefault[0])
        elif rng == 1:
            if tree_type == TreeType.Oak:
                blockAboveFloorLevel.append(pocketMaterials.Wood)
            elif tree_type == TreeType.Spruce:
                blockAboveFloorLevel.append(pocketMaterials.SpruceWood)
            elif tree_type == TreeType.Birch:
                blockAboveFloorLevel.append(pocketMaterials.BirchWood)
            elif tree_type == TreeType.Jungle:
                blockAboveFloorLevel.append(pocketMaterials.JungleWood)
            elif tree_type == TreeType.Acacia:
                blockAboveFloorLevel.append(pocketMaterials.AcaciaWood)
            elif tree_type == TreeType.DarkOak:
                blockAboveFloorLevel.append(pocketMaterials.DarkOakWood)
            else:
                blockAboveFloorLevel.append(blockDefault[0])

    # rock
    elif ground_type == GroundType.Rock:
        rng = random.randint(0, 1)
        if district_data.wealthScore != 2:
            if rng == 0:
                blockAboveFloorLevel.append(blockDefault[0])
            elif rng == 1:
                if tree_type == TreeType.Oak:
                    blockAboveFloorLevel.append(pocketMaterials.Wood)
                elif tree_type == TreeType.Spruce:
                    blockAboveFloorLevel.append(pocketMaterials.SpruceWood)
                elif tree_type == TreeType.Birch:
                    blockAboveFloorLevel.append(pocketMaterials.BirchWood)
                elif tree_type == TreeType.Jungle:
                    blockAboveFloorLevel.append(pocketMaterials.JungleWood)
                elif tree_type == TreeType.Acacia:
                    blockAboveFloorLevel.append(pocketMaterials.AcaciaWood)
                elif tree_type == TreeType.DarkOak:
                    blockAboveFloorLevel.append(pocketMaterials.DarkOakWood)
                else:
                    blockAboveFloorLevel.append(blockDefault[0])
        elif district_data.wealthScore == 2:
            blockAboveFloorLevel.append(pocketMaterials.Stone)
            blockAboveFloorLevel.append(pocketMaterials.Andesite)

    # block under ceiling level
    # grass
    if ground_type == GroundType.Grass:
        if defaultIsWood:
            blockUnderCeilingLevel.append(blockDefault[0])
        else:
            rng = random.randint(0, 1)
            if rng == 0:
                blockUnderCeilingLevel.append(blockDefault[0])
            elif rng == 1:
                if tree_type == TreeType.Oak:
                    blockUnderCeilingLevel.append(pocketMaterials.Wood)
                elif tree_type == TreeType.Spruce:
                    blockUnderCeilingLevel.append(pocketMaterials.SpruceWood)
                elif tree_type == TreeType.Birch:
                    blockUnderCeilingLevel.append(pocketMaterials.BirchWood)
                elif tree_type == TreeType.Jungle:
                    blockUnderCeilingLevel.append(pocketMaterials.JungleWood)
                elif tree_type == TreeType.Acacia:
                    blockUnderCeilingLevel.append(pocketMaterials.AcaciaWood)
                elif tree_type == TreeType.DarkOak:
                    blockUnderCeilingLevel.append(pocketMaterials.DarkOakWood)
                else:
                    blockUnderCeilingLevel.append(blockDefault[0])

    # yellow sand / red sand
    elif ground_type == GroundType.YellowSand or ground_type == GroundType.RedSand:
        rng = random.randint(0, 1)
        if district_data.wealthScore == 0:
            blockUnderCeilingLevel.append(pocketMaterials.StoneBricks)
        elif district_data.wealthScore == 1:
            if rng == 0:
                if ground_type == GroundType.YellowSand:
                    blockUnderCeilingLevel.append(pocketMaterials.ChiseledSandstone)
                else:
                    blockUnderCeilingLevel.append(pocketMaterials.ChiseledRedSandstone)
            elif rng == 1:
                if tree_type == TreeType.Oak:
                    blockUnderCeilingLevel.append(pocketMaterials.Wood)
                elif tree_type == TreeType.Spruce:
                    blockUnderCeilingLevel.append(pocketMaterials.SpruceWood)
                elif tree_type == TreeType.Birch:
                    blockUnderCeilingLevel.append(pocketMaterials.BirchWood)
                elif tree_type == TreeType.Jungle:
                    blockUnderCeilingLevel.append(pocketMaterials.JungleWood)
                elif tree_type == TreeType.Acacia:
                    blockUnderCeilingLevel.append(pocketMaterials.AcaciaWood)
                elif tree_type == TreeType.DarkOak:
                    blockUnderCeilingLevel.append(pocketMaterials.DarkOakWood)
                else:
                    blockUnderCeilingLevel.append(blockDefault[0])
        elif district_data.wealthScore == 2:
            if rng == 0:
                if ground_type == GroundType.YellowSand:
                    blockUnderCeilingLevel.append(pocketMaterials.Sandstone)
                else:
                    blockUnderCeilingLevel.append(pocketMaterials.RedSandstone)
            elif rng == 1:
                blockUnderCeilingLevel.append(blockDefault[0])

    # hardened clay
    elif ground_type == GroundType.HardenedClay:
        rng = random.randint(0, 1)
        if rng == 0:
            blockUnderCeilingLevel.append(blockDefault[0])
        elif rng == 1:
            if tree_type == TreeType.Oak:
                blockUnderCeilingLevel.append(pocketMaterials.Wood)
            elif tree_type == TreeType.Spruce:
                blockUnderCeilingLevel.append(pocketMaterials.SpruceWood)
            elif tree_type == TreeType.Birch:
                blockUnderCeilingLevel.append(pocketMaterials.BirchWood)
            elif tree_type == TreeType.Jungle:
                blockUnderCeilingLevel.append(pocketMaterials.JungleWood)
            elif tree_type == TreeType.Acacia:
                blockUnderCeilingLevel.append(pocketMaterials.AcaciaWood)
            elif tree_type == TreeType.DarkOak:
                blockUnderCeilingLevel.append(pocketMaterials.DarkOakWood)
            else:
                blockUnderCeilingLevel.append(blockDefault[0])

    # rock
    elif ground_type == GroundType.Rock:
        rng = random.randint(0, 1)
        if district_data.wealthScore != 2:
            if rng == 0:
                blockUnderCeilingLevel.append(blockDefault[0])
            elif rng == 1:
                if tree_type == TreeType.Oak:
                    blockUnderCeilingLevel.append(pocketMaterials.Wood)
                elif tree_type == TreeType.Spruce:
                    blockUnderCeilingLevel.append(pocketMaterials.SpruceWood)
                elif tree_type == TreeType.Birch:
                    blockUnderCeilingLevel.append(pocketMaterials.BirchWood)
                elif tree_type == TreeType.Jungle:
                    blockUnderCeilingLevel.append(pocketMaterials.JungleWood)
                elif tree_type == TreeType.Acacia:
                    blockUnderCeilingLevel.append(pocketMaterials.AcaciaWood)
                elif tree_type == TreeType.DarkOak:
                    blockUnderCeilingLevel.append(pocketMaterials.DarkOakWood)
                else:
                    blockUnderCeilingLevel.append(blockDefault[0])
        elif district_data.wealthScore == 2:
            blockUnderCeilingLevel.append(pocketMaterials.Stone)
            blockUnderCeilingLevel.append(pocketMaterials.Andesite)

    # block beside door
    # grass
    if ground_type == GroundType.Grass:
        if defaultIsWood:
            blockBesideDoor.append(blockDefault[0])
        else:
            rng = random.randint(0, 1)
            if rng == 0:
                blockBesideDoor.append(blockDefault[0])
            elif rng == 1:
                if tree_type == TreeType.Oak:
                    blockBesideDoor.append(pocketMaterials.Wood)
                elif tree_type == TreeType.Spruce:
                    blockBesideDoor.append(pocketMaterials.SpruceWood)
                elif tree_type == TreeType.Birch:
                    blockBesideDoor.append(pocketMaterials.BirchWood)
                elif tree_type == TreeType.Jungle:
                    blockBesideDoor.append(pocketMaterials.JungleWood)
                elif tree_type == TreeType.Acacia:
                    blockBesideDoor.append(pocketMaterials.AcaciaWood)
                elif tree_type == TreeType.DarkOak:
                    blockBesideDoor.append(pocketMaterials.DarkOakWood)
                else:
                    blockBesideDoor.append(blockDefault[0])

    # yellow sand / red sand
    elif ground_type == GroundType.YellowSand or ground_type == GroundType.RedSand:
        rng = random.randint(0, 1)
        if district_data.wealthScore == 0:
            blockBesideDoor.append(pocketMaterials.StoneBricks)
        elif district_data.wealthScore == 1:
            if rng == 0:
                if ground_type == GroundType.YellowSand:
                    blockBesideDoor.append(pocketMaterials.ChiseledSandstone)
                else:
                    blockBesideDoor.append(pocketMaterials.ChiseledRedSandstone)
            elif rng == 1:
                if tree_type == TreeType.Oak:
                    blockBesideDoor.append(pocketMaterials.Wood)
                elif tree_type == TreeType.Spruce:
                    blockBesideDoor.append(pocketMaterials.SpruceWood)
                elif tree_type == TreeType.Birch:
                    blockBesideDoor.append(pocketMaterials.BirchWood)
                elif tree_type == TreeType.Jungle:
                    blockBesideDoor.append(pocketMaterials.JungleWood)
                elif tree_type == TreeType.Acacia:
                    blockBesideDoor.append(pocketMaterials.AcaciaWood)
                elif tree_type == TreeType.DarkOak:
                    blockBesideDoor.append(pocketMaterials.DarkOakWood)
                else:
                    blockBesideDoor.append(blockDefault[0])
        elif district_data.wealthScore == 2:
            if rng == 0:
                if ground_type == GroundType.YellowSand:
                    blockBesideDoor.append(pocketMaterials.Sandstone)
                else:
                    blockBesideDoor.append(pocketMaterials.RedSandstone)
            elif rng == 1:
                blockBesideDoor.append(blockDefault[0])

    # hardened clay
    elif ground_type == GroundType.HardenedClay:
        rng = random.randint(0, 1)
        if rng == 0:
            blockBesideDoor.append(blockDefault[0])
        elif rng == 1:
            if tree_type == TreeType.Oak:
                blockBesideDoor.append(pocketMaterials.Wood)
            elif tree_type == TreeType.Spruce:
                blockBesideDoor.append(pocketMaterials.SpruceWood)
            elif tree_type == TreeType.Birch:
                blockBesideDoor.append(pocketMaterials.BirchWood)
            elif tree_type == TreeType.Jungle:
                blockBesideDoor.append(pocketMaterials.JungleWood)
            elif tree_type == TreeType.Acacia:
                blockBesideDoor.append(pocketMaterials.AcaciaWood)
            elif tree_type == TreeType.DarkOak:
                blockBesideDoor.append(pocketMaterials.DarkOakWood)
            else:
                blockBesideDoor.append(blockDefault[0])

    # rock
    elif ground_type == GroundType.Rock:
        rng = random.randint(0, 1)
        if district_data.wealthScore != 2:
            if rng == 0:
                blockBesideDoor.append(blockDefault[0])
            elif rng == 1:
                if tree_type == TreeType.Oak:
                    blockBesideDoor.append(pocketMaterials.Wood)
                elif tree_type == TreeType.Spruce:
                    blockBesideDoor.append(pocketMaterials.SpruceWood)
                elif tree_type == TreeType.Birch:
                    blockBesideDoor.append(pocketMaterials.BirchWood)
                elif tree_type == TreeType.Jungle:
                    blockBesideDoor.append(pocketMaterials.JungleWood)
                elif tree_type == TreeType.Acacia:
                    blockBesideDoor.append(pocketMaterials.AcaciaWood)
                elif tree_type == TreeType.DarkOak:
                    blockBesideDoor.append(pocketMaterials.DarkOakWood)
                else:
                    blockBesideDoor.append(blockDefault[0])
        elif district_data.wealthScore == 2:
            blockBesideDoor.append(pocketMaterials.Stone)
            blockBesideDoor.append(pocketMaterials.Andesite)

    # block above door
    # grass
    if ground_type == GroundType.Grass:
        if defaultIsWood:
            blockAboveDoor.append(blockDefault[0])
        else:
            rng = random.randint(0, 1)
            if rng == 0:
                blockAboveDoor.append(blockDefault[0])
            elif rng == 1:
                if tree_type == TreeType.Oak:
                    blockAboveDoor.append(pocketMaterials.Wood)
                elif tree_type == TreeType.Spruce:
                    blockAboveDoor.append(pocketMaterials.SpruceWood)
                elif tree_type == TreeType.Birch:
                    blockAboveDoor.append(pocketMaterials.BirchWood)
                elif tree_type == TreeType.Jungle:
                    blockAboveDoor.append(pocketMaterials.JungleWood)
                elif tree_type == TreeType.Acacia:
                    blockAboveDoor.append(pocketMaterials.AcaciaWood)
                elif tree_type == TreeType.DarkOak:
                    blockAboveDoor.append(pocketMaterials.DarkOakWood)
                else:
                    blockAboveDoor.append(blockDefault[0])

    # yellow sand / red sand
    elif ground_type == GroundType.YellowSand or ground_type == GroundType.RedSand:
        rng = random.randint(0, 1)
        if district_data.wealthScore == 0:
            blockAboveDoor.append(pocketMaterials.StoneBricks)
        elif district_data.wealthScore == 1:
            if rng == 0:
                if ground_type == GroundType.YellowSand:
                    blockAboveDoor.append(pocketMaterials.ChiseledSandstone)
                else:
                    blockAboveDoor.append(pocketMaterials.ChiseledRedSandstone)
            elif rng == 1:
                if tree_type == TreeType.Oak:
                    blockAboveDoor.append(pocketMaterials.Wood)
                elif tree_type == TreeType.Spruce:
                    blockAboveDoor.append(pocketMaterials.SpruceWood)
                elif tree_type == TreeType.Birch:
                    blockAboveDoor.append(pocketMaterials.BirchWood)
                elif tree_type == TreeType.Jungle:
                    blockAboveDoor.append(pocketMaterials.JungleWood)
                elif tree_type == TreeType.Acacia:
                    blockAboveDoor.append(pocketMaterials.AcaciaWood)
                elif tree_type == TreeType.DarkOak:
                    blockAboveDoor.append(pocketMaterials.DarkOakWood)
                else:
                    blockAboveDoor.append(blockDefault[0])
        elif district_data.wealthScore == 2:
            if rng == 0:
                if ground_type == GroundType.YellowSand:
                    blockAboveDoor.append(pocketMaterials.Sandstone)
                else:
                    blockAboveDoor.append(pocketMaterials.RedSandstone)
            elif rng == 1:
                blockAboveDoor.append(blockDefault[0])

    # hardened clay
    elif ground_type == GroundType.HardenedClay:
        rng = random.randint(0, 1)
        if rng == 0:
            blockAboveDoor.append(blockDefault[0])
        elif rng == 1:
            if tree_type == TreeType.Oak:
                blockAboveDoor.append(pocketMaterials.Wood)
            elif tree_type == TreeType.Spruce:
                blockAboveDoor.append(pocketMaterials.SpruceWood)
            elif tree_type == TreeType.Birch:
                blockAboveDoor.append(pocketMaterials.BirchWood)
            elif tree_type == TreeType.Jungle:
                blockAboveDoor.append(pocketMaterials.JungleWood)
            elif tree_type == TreeType.Acacia:
                blockAboveDoor.append(pocketMaterials.AcaciaWood)
            elif tree_type == TreeType.DarkOak:
                blockAboveDoor.append(pocketMaterials.DarkOakWood)
            else:
                blockAboveDoor.append(blockDefault[0])

    # rock
    elif ground_type == GroundType.Rock:
        rng = random.randint(0, 1)
        if district_data.wealthScore != 2:
            if rng == 0:
                blockAboveDoor.append(blockDefault[0])
            elif rng == 1:
                if tree_type == TreeType.Oak:
                    blockAboveDoor.append(pocketMaterials.Wood)
                elif tree_type == TreeType.Spruce:
                    blockAboveDoor.append(pocketMaterials.SpruceWood)
                elif tree_type == TreeType.Birch:
                    blockAboveDoor.append(pocketMaterials.BirchWood)
                elif tree_type == TreeType.Jungle:
                    blockAboveDoor.append(pocketMaterials.JungleWood)
                elif tree_type == TreeType.Acacia:
                    blockAboveDoor.append(pocketMaterials.AcaciaWood)
                elif tree_type == TreeType.DarkOak:
                    blockAboveDoor.append(pocketMaterials.DarkOakWood)
                else:
                    blockAboveDoor.append(blockDefault[0])
        elif district_data.wealthScore == 2:
            blockAboveDoor.append(pocketMaterials.Stone)
            blockAboveDoor.append(pocketMaterials.Andesite)

    # block under window
    # grass
    if ground_type == GroundType.Grass:
        if defaultIsWood:
            blockUnderWindow.append(blockDefault[0])
        else:
            rng = random.randint(0, 1)
            if rng == 0:
                blockUnderWindow.append(blockDefault[0])
            elif rng == 1:
                if tree_type == TreeType.Oak:
                    blockUnderWindow.append(pocketMaterials.Wood)
                elif tree_type == TreeType.Spruce:
                    blockUnderWindow.append(pocketMaterials.SpruceWood)
                elif tree_type == TreeType.Birch:
                    blockUnderWindow.append(pocketMaterials.BirchWood)
                elif tree_type == TreeType.Jungle:
                    blockUnderWindow.append(pocketMaterials.JungleWood)
                elif tree_type == TreeType.Acacia:
                    blockUnderWindow.append(pocketMaterials.AcaciaWood)
                elif tree_type == TreeType.DarkOak:
                    blockUnderWindow.append(pocketMaterials.DarkOakWood)
                else:
                    blockUnderWindow.append(blockDefault[0])

    # yellow sand / red sand
    elif ground_type == GroundType.YellowSand or ground_type == GroundType.RedSand:
        rng = random.randint(0, 1)
        if district_data.wealthScore == 0:
            blockUnderWindow.append(pocketMaterials.StoneBricks)
        elif district_data.wealthScore == 1:
            if rng == 0:
                if ground_type == GroundType.YellowSand:
                    blockUnderWindow.append(pocketMaterials.ChiseledSandstone)
                else:
                    blockUnderWindow.append(pocketMaterials.ChiseledRedSandstone)
            elif rng == 1:
                if tree_type == TreeType.Oak:
                    blockUnderWindow.append(pocketMaterials.Wood)
                elif tree_type == TreeType.Spruce:
                    blockUnderWindow.append(pocketMaterials.SpruceWood)
                elif tree_type == TreeType.Birch:
                    blockUnderWindow.append(pocketMaterials.BirchWood)
                elif tree_type == TreeType.Jungle:
                    blockUnderWindow.append(pocketMaterials.JungleWood)
                elif tree_type == TreeType.Acacia:
                    blockUnderWindow.append(pocketMaterials.AcaciaWood)
                elif tree_type == TreeType.DarkOak:
                    blockUnderWindow.append(pocketMaterials.DarkOakWood)
                else:
                    blockUnderWindow.append(blockDefault[0])
        elif district_data.wealthScore == 2:
            if rng == 0:
                if ground_type == GroundType.YellowSand:
                    blockUnderWindow.append(pocketMaterials.Sandstone)
                else:
                    blockUnderWindow.append(pocketMaterials.RedSandstone)
            elif rng == 1:
                blockUnderWindow.append(blockDefault[0])

    # hardened clay
    elif ground_type == GroundType.HardenedClay:
        rng = random.randint(0, 1)
        if rng == 0:
            blockUnderWindow.append(blockDefault[0])
        elif rng == 1:
            if tree_type == TreeType.Oak:
                blockUnderWindow.append(pocketMaterials.Wood)
            elif tree_type == TreeType.Spruce:
                blockUnderWindow.append(pocketMaterials.SpruceWood)
            elif tree_type == TreeType.Birch:
                blockUnderWindow.append(pocketMaterials.BirchWood)
            elif tree_type == TreeType.Jungle:
                blockUnderWindow.append(pocketMaterials.JungleWood)
            elif tree_type == TreeType.Acacia:
                blockUnderWindow.append(pocketMaterials.AcaciaWood)
            elif tree_type == TreeType.DarkOak:
                blockUnderWindow.append(pocketMaterials.DarkOakWood)
            else:
                blockUnderWindow.append(blockDefault[0])

    # rock
    elif ground_type == GroundType.Rock:
        rng = random.randint(0, 1)
        if district_data.wealthScore != 2:
            if rng == 0:
                blockUnderWindow.append(blockDefault[0])
            elif rng == 1:
                if tree_type == TreeType.Oak:
                    blockUnderWindow.append(pocketMaterials.Wood)
                elif tree_type == TreeType.Spruce:
                    blockUnderWindow.append(pocketMaterials.SpruceWood)
                elif tree_type == TreeType.Birch:
                    blockUnderWindow.append(pocketMaterials.BirchWood)
                elif tree_type == TreeType.Jungle:
                    blockUnderWindow.append(pocketMaterials.JungleWood)
                elif tree_type == TreeType.Acacia:
                    blockUnderWindow.append(pocketMaterials.AcaciaWood)
                elif tree_type == TreeType.DarkOak:
                    blockUnderWindow.append(pocketMaterials.DarkOakWood)
                else:
                    blockUnderWindow.append(blockDefault[0])
        elif district_data.wealthScore == 2:
            blockUnderWindow.append(pocketMaterials.Stone)
            blockUnderWindow.append(pocketMaterials.Andesite)

    # block above window
    # grass
    if ground_type == GroundType.Grass:
        if defaultIsWood:
            blockAboveWindow.append(blockDefault[0])
        else:
            rng = random.randint(0, 1)
            if rng == 0:
                blockAboveWindow.append(blockDefault[0])
            elif rng == 1:
                if tree_type == TreeType.Oak:
                    blockAboveWindow.append(pocketMaterials.Wood)
                elif tree_type == TreeType.Spruce:
                    blockAboveWindow.append(pocketMaterials.SpruceWood)
                elif tree_type == TreeType.Birch:
                    blockAboveWindow.append(pocketMaterials.BirchWood)
                elif tree_type == TreeType.Jungle:
                    blockAboveWindow.append(pocketMaterials.JungleWood)
                elif tree_type == TreeType.Acacia:
                    blockAboveWindow.append(pocketMaterials.AcaciaWood)
                elif tree_type == TreeType.DarkOak:
                    blockAboveWindow.append(pocketMaterials.DarkOakWood)
                else:
                    blockAboveWindow.append(blockDefault[0])

    # yellow sand / red sand
    elif ground_type == GroundType.YellowSand or ground_type == GroundType.RedSand:
        rng = random.randint(0, 1)
        if district_data.wealthScore == 0:
            blockAboveWindow.append(pocketMaterials.StoneBricks)
        elif district_data.wealthScore == 1:
            if rng == 0:
                if ground_type == GroundType.YellowSand:
                    blockAboveWindow.append(pocketMaterials.ChiseledSandstone)
                else:
                    blockAboveWindow.append(pocketMaterials.ChiseledRedSandstone)
            elif rng == 1:
                if tree_type == TreeType.Oak:
                    blockAboveWindow.append(pocketMaterials.Wood)
                elif tree_type == TreeType.Spruce:
                    blockAboveWindow.append(pocketMaterials.SpruceWood)
                elif tree_type == TreeType.Birch:
                    blockAboveWindow.append(pocketMaterials.BirchWood)
                elif tree_type == TreeType.Jungle:
                    blockAboveWindow.append(pocketMaterials.JungleWood)
                elif tree_type == TreeType.Acacia:
                    blockAboveWindow.append(pocketMaterials.AcaciaWood)
                elif tree_type == TreeType.DarkOak:
                    blockAboveWindow.append(pocketMaterials.DarkOakWood)
                else:
                    blockAboveWindow.append(blockDefault[0])
        elif district_data.wealthScore == 2:
            if rng == 0:
                if ground_type == GroundType.YellowSand:
                    blockAboveWindow.append(pocketMaterials.Sandstone)
                else:
                    blockAboveWindow.append(pocketMaterials.RedSandstone)
            elif rng == 1:
                blockAboveWindow.append(blockDefault[0])

    # hardened clay
    elif ground_type == GroundType.HardenedClay:
        rng = random.randint(0, 1)
        if rng == 0:
            blockAboveWindow.append(blockDefault[0])
        elif rng == 1:
            if tree_type == TreeType.Oak:
                blockAboveWindow.append(pocketMaterials.Wood)
            elif tree_type == TreeType.Spruce:
                blockAboveWindow.append(pocketMaterials.SpruceWood)
            elif tree_type == TreeType.Birch:
                blockAboveWindow.append(pocketMaterials.BirchWood)
            elif tree_type == TreeType.Jungle:
                blockAboveWindow.append(pocketMaterials.JungleWood)
            elif tree_type == TreeType.Acacia:
                blockAboveWindow.append(pocketMaterials.AcaciaWood)
            elif tree_type == TreeType.DarkOak:
                blockAboveWindow.append(pocketMaterials.DarkOakWood)
            else:
                blockAboveWindow.append(blockDefault[0])

    # rock
    elif ground_type == GroundType.Rock:
        rng = random.randint(0, 1)
        if district_data.wealthScore != 2:
            if rng == 0:
                blockAboveWindow.append(blockDefault[0])
            elif rng == 1:
                if tree_type == TreeType.Oak:
                    blockAboveWindow.append(pocketMaterials.Wood)
                elif tree_type == TreeType.Spruce:
                    blockAboveWindow.append(pocketMaterials.SpruceWood)
                elif tree_type == TreeType.Birch:
                    blockAboveWindow.append(pocketMaterials.BirchWood)
                elif tree_type == TreeType.Jungle:
                    blockAboveWindow.append(pocketMaterials.JungleWood)
                elif tree_type == TreeType.Acacia:
                    blockAboveWindow.append(pocketMaterials.AcaciaWood)
                elif tree_type == TreeType.DarkOak:
                    blockAboveWindow.append(pocketMaterials.DarkOakWood)
                else:
                    blockAboveWindow.append(blockDefault[0])
        elif district_data.wealthScore == 2:
            blockAboveWindow.append(pocketMaterials.Stone)
            blockAboveWindow.append(pocketMaterials.Andesite)

    # block for window
    if district_data.wealthScore != 2:
        blockForWindow.append(pocketMaterials.GlassPane)
    else:
        blockForWindow.append(pocketMaterials.LightGrayStainedGlassPane)
        blockForWindow.append(pocketMaterials.WhiteStainedGlassPane)

    # block at floor level
    rng = random.randint(0, 2)
    if rng == 0:
        blockAtFloorLevel.append(blockDefault[0])
    else:
        blockAtFloorLevel.append(blockCorner[0])

    # doors
    if tree_type == TreeType.Oak:
        blockDoorBottomX = [pocketMaterials[64, 0]]
        blockDoorTopX = [pocketMaterials[64, 8]]
        blockDoorBottomZ = [pocketMaterials[64, 1]]
        blockDoorTopZ = [pocketMaterials[64, 9]]
    elif tree_type == TreeType.Spruce:
        blockDoorBottomX = [pocketMaterials[193, 0]]
        blockDoorTopX = [pocketMaterials[193, 8]]
        blockDoorBottomZ = [pocketMaterials[193, 1]]
        blockDoorTopZ = [pocketMaterials[193, 9]]
    elif tree_type == TreeType.Birch:
        blockDoorBottomX = [pocketMaterials[194, 0]]
        blockDoorTopX = [pocketMaterials[194, 8]]
        blockDoorBottomZ = [pocketMaterials[194, 1]]
        blockDoorTopZ = [pocketMaterials[194, 9]]
    elif tree_type == TreeType.Jungle:
        blockDoorBottomX = [pocketMaterials[195, 0]]
        blockDoorTopX = [pocketMaterials[195, 8]]
        blockDoorBottomZ = [pocketMaterials[195, 1]]
        blockDoorTopZ = [pocketMaterials[195, 9]]
    elif tree_type == TreeType.Acacia:
        blockDoorBottomX = [pocketMaterials[196, 0]]
        blockDoorTopX = [pocketMaterials[196, 8]]
        blockDoorBottomZ = [pocketMaterials[196, 1]]
        blockDoorTopZ = [pocketMaterials[196, 9]]
    elif tree_type == TreeType.DarkOak:
        blockDoorBottomX = [pocketMaterials[197, 0]]
        blockDoorTopX = [pocketMaterials[197, 8]]
        blockDoorBottomZ = [pocketMaterials[197, 1]]
        blockDoorTopZ = [pocketMaterials[197, 9]]
    else:
        blockDoorBottomX = [pocketMaterials[64, 0]]
        blockDoorTopX = [pocketMaterials[64, 8]]
        blockDoorBottomZ = [pocketMaterials[64, 1]]
        blockDoorTopZ = [pocketMaterials[64, 9]]

    return BlockPalette(blockRoof, blockBesideCorner, blockAtFloorLevel, blockAboveFloorLevel, blockUnderCeilingLevel,
                        blockBesideDoor, blockAboveDoor, blockUnderWindow, blockAboveWindow, blockForWindow,
                        blockDefault, blockDoorBottomX, blockDoorTopX, blockDoorBottomZ, blockDoorTopZ, flooring,
                        foundationBlock, blockCorner)


def gatherData(level, district_data, world_map):
    lengthAddition = 40
    treeSearchBox = SearchBox(district_data.xOrigin - lengthAddition, district_data.xEnd - district_data.xOrigin +
                              lengthAddition, district_data.zOrigin - lengthAddition, district_data.zEnd -
                              district_data.zOrigin + lengthAddition)
    groundSearchBox = SearchBox(district_data.xOrigin, district_data.xEnd - district_data.xOrigin,
                                district_data.zOrigin, district_data.zEnd - district_data.zOrigin)
    treeDistrictBox = BoundingBox((treeSearchBox.startX, 0, treeSearchBox.startZ),
                                  (treeSearchBox.lengthX, 255, treeSearchBox.lengthZ))
    groundDistrictBox = BoundingBox((groundSearchBox.startX, 0, groundSearchBox.startZ),
                                    (groundSearchBox.lengthX, 255, groundSearchBox.lengthZ))
    treeType = determineWoodType(level, treeDistrictBox, world_map.heightmap, district_data.xShift,
                                 district_data.zShift)
    groundType = determineMainGround(level, groundDistrictBox, world_map.heightmap, district_data.xShift,
                                     district_data.zShift)

    return createPalette(district_data, treeType, groundType)

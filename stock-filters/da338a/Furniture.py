from __future__ import division
from enum import Enum
import random
import pymchelper

from pymclevel.materials import pocketMaterials
from pymclevel.materials import alphaMaterials
from pymclevel.materials import indevMaterials

kitchenFurniture = []
livingRoomFurniture = []
bedRoomFurniture = []
storageFurniture = []
studyRoomFurniture = []


# initialize lists containing furniture for the different types of rooms
def initializeLists():
    # kitchen furniture
    kitchenFurniture.append(Cauldron)
    kitchenFurniture.append(KitchenCounter)
    kitchenFurniture.append(KitchenCounter)
    kitchenFurniture.append(KitchenCounter)
    kitchenFurniture.append(Furnace)

    # living room furniture
    livingRoomFurniture.append(Table)
    livingRoomFurniture.append(Table)
    livingRoomFurniture.append(Chair)
    livingRoomFurniture.append(BookShelf)
    livingRoomFurniture.append(Lamp)

    # bedroom furniture
    bedRoomFurniture.append(Bed)
    bedRoomFurniture.append(Nothing)
    bedRoomFurniture.append(Nothing)
    bedRoomFurniture.append(BookShelf)

    # storage furniture
    storageFurniture.append(Chest)
    storageFurniture.append(Chest)
    storageFurniture.append(Chest)
    storageFurniture.append(Chest)
    storageFurniture.append(Nothing)
    storageFurniture.append(CraftingTable)

    # study room furniture
    studyRoomFurniture.append(EnchantmentTable)
    studyRoomFurniture.append(BookShelf)
    studyRoomFurniture.append(Chair)


class RoomType(Enum):
    kitchen = 0
    livingRoom = 1
    bedRoom = 2
    storage = 3
    studyRoom = 4


class Nothing:
    def __init__(self, level, x, y, z, direction_int):
        if pymchelper.has_material(level, pocketMaterials.Air, x, y, z):
            pymchelper.set_block(level, pocketMaterials.Air, x, y, z)


class Chair:
    def __init__(self, level, x, y, z, direction_int):
        if pymchelper.has_material(level, pocketMaterials.Air, x, y, z):
            pymchelper.set_block(level, pocketMaterials[96, 0], x, y + 1, z)
            if direction_int == 2 or direction_int == 3:
                if direction_int == 2:  # north
                    level.setBlockDataAt(x, y + 1, z, 0 | 4)
                elif direction_int == 3:  # south
                    level.setBlockDataAt(x, y + 1, z, 1 | 4)
                if pymchelper.has_material(level, pocketMaterials.Air, x - 1, y, z):
                    pymchelper.set_block(level, pocketMaterials[96, 0], x - 1, y, z)
                    level.setBlockDataAt(x - 1, y, z, 2 | 4)
                if pymchelper.has_material(level, pocketMaterials.Air, x + 1, y, z):
                    pymchelper.set_block(level, pocketMaterials[96, 0], x + 1, y, z)
                    level.setBlockDataAt(x + 1, y, z, 3 | 4)
            elif direction_int == 4 or direction_int == 5:
                if direction_int == 4:  # west
                    direction_int = 0
                    level.setBlockDataAt(x, y + 1, z, 2 | 4)
                elif direction_int == 5:  # east
                    direction_int = 1
                    level.setBlockDataAt(x, y + 1, z, 3 | 4)
                if pymchelper.has_material(level, pocketMaterials.Air, x, y, z - 1):
                    pymchelper.set_block(level, pocketMaterials[96, 0], x, y, z - 1)
                    level.setBlockDataAt(x, y, z - 1, 0 | 4)
                if pymchelper.has_material(level, pocketMaterials.Air, x, y, z + 1):
                    pymchelper.set_block(level, pocketMaterials[96, 0], x, y, z + 1)
                    level.setBlockDataAt(x, y, z + 1, 1 | 4)
            pymchelper.set_block(level, pocketMaterials.WoodenStairs, x, y, z)
            level.setBlockDataAt(x, y, z, direction_int)


class EnchantmentTable:
    def __init__(self, level, x, y, z, direction_int):
        if pymchelper.has_material(level, pocketMaterials.Air, x, y, z):
            pymchelper.set_block(level, alphaMaterials[116, 0], x, y, z)


class KitchenCounter:
    def __init__(self, level, x, y, z, direction_int):
        if pymchelper.has_material(level, pocketMaterials.Air, x, y, z):
            pymchelper.set_block(level, alphaMaterials.BlockofIron, x, y, z)
            if random.randint(0, 5) == 4:
                pymchelper.set_block(level, pocketMaterials.BrewingStand, x, y + 1, z)
            else:
                pymchelper.set_block(level, pocketMaterials[70, 0], x, y + 1, z)


class Furnace:
    def __init__(self, level, x, y, z, direction_int):
        if pymchelper.has_material(level, pocketMaterials.Air, x, y, z):
            pymchelper.set_block(level, indevMaterials.Furnace, x, y, z)
            level.setBlockDataAt(x, y, z, direction_int)


class Chest:
    def __init__(self, level, x, y, z, direction_int):
        if pymchelper.has_material(level, pocketMaterials.Air, x, y, z):
            pymchelper.set_block(level, indevMaterials[54, 2], x, y, z)
            level.setBlockDataAt(x, y, z, direction_int)


class CraftingTable:
    def __init__(self, level, x, y, z, direction_int):
        if pymchelper.has_material(level, pocketMaterials.Air, x, y, z):
            pymchelper.set_block(level, alphaMaterials.CraftingTable, x, y, z)


class BookShelf:
    def __init__(self, level, x, y, z, direction_int):
        if pymchelper.has_material(level, pocketMaterials.Air, x, y, z):
            pymchelper.set_block(level, alphaMaterials.Bookshelf, x, y, z)
            pymchelper.set_block(level, alphaMaterials.Bookshelf, x, y + 1, z)
            if random.getrandbits(1):
                pymchelper.set_block(level, alphaMaterials.Bookshelf, x, y + 2, z)


class Lamp:
    def __init__(self, level, x, y, z, direction_int):
        if pymchelper.has_material(level, pocketMaterials.Air, x, y, z):
            pymchelper.set_block(level, pocketMaterials.Fence, x, y, z)
            if random.getrandbits(1):
                pymchelper.set_block(level, pocketMaterials.Fence, x, y + 1, z)
                pymchelper.set_block(level, alphaMaterials.Glowstone, x, y + 2, z)
            else:
                pymchelper.set_block(level, alphaMaterials.Glowstone, x, y + 1, z)


class Table:
    def __init__(self, level, x, y, z, direction_int):
        if pymchelper.has_material(level, pocketMaterials.Air, x, y, z):
            pymchelper.set_block(level, alphaMaterials[126, 8], x, y, z)
            if random.randint(0, 5) == 0:
                pymchelper.set_block(level, pocketMaterials.Cake, x, y + 1, z)


class Bed:
    def __init__(self, level, x, y, z, direction_int):
        if pymchelper.has_material(level, pocketMaterials.Air, x, y, z):
            if direction_int == 2:  # north
                if pymchelper.has_material(level, pocketMaterials.Air, x, y, z - 1):
                    pymchelper.set_block(level, alphaMaterials[26, 8], x, y, z)
                    pymchelper.set_block(level, alphaMaterials[26, 0], x, y, z - 1)
            if direction_int == 3:  # south
                if pymchelper.has_material(level, pocketMaterials.Air, x, y, z + 1):
                    pymchelper.set_block(level, alphaMaterials[26, 10], x, y, z)
                    pymchelper.set_block(level, alphaMaterials[26, 2], x, y, z + 1)
            if direction_int == 4:  # west
                if pymchelper.has_material(level, pocketMaterials.Air, x - 1, y, z):
                    pymchelper.set_block(level, alphaMaterials[26, 11], x, y, z)
                    pymchelper.set_block(level, alphaMaterials[26, 3], x - 1, y, z)
            if direction_int == 5:  # east
                if pymchelper.has_material(level, pocketMaterials.Air, x + 1, y, z):
                    pymchelper.set_block(level, alphaMaterials[26, 9], x, y, z)
                    pymchelper.set_block(level, alphaMaterials[26, 1], x + 1, y, z)


class Cauldron:
    def __init__(self, level, x, y, z, direction_int):
        if pymchelper.has_material(level, pocketMaterials.Air, x, y, z):
            pymchelper.set_block(level, alphaMaterials.Cauldron, x, y, z)

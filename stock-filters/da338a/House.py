from __future__ import division

import sys

from enum import Enum
import random
import numpy
import pymchelper
import Furniture
from Coordinate import Coord
from HouseCoordData import HouseCoordData
from pymclevel.materials import pocketMaterials


# checks if rectangles intersect
def checkRectIntersect(rect_a, rect_b):
    xEndA = rect_a.xorigin + rect_a.xlength
    zEndA = rect_a.zorigin + rect_a.zlength
    xEndB = rect_b.xorigin + rect_b.xlength
    zEndB = rect_b.zorigin + rect_b.zlength
    if rect_a.xorigin >= xEndB or rect_b.xorigin >= xEndA or rect_a.zorigin >= zEndB or rect_b.zorigin >= zEndA:
        return False
    else:
        return True


class House(object):
    def __init__(self, level, box, front_direction, two_squares, two_floors, build_layer, block_palette):
        if box.maxx - box.minx >= 7 and box.maxz - box.minz >= 7:
            self.mainSquare = None
            self.secondarySquare = None
            self.foundationHeight = 0
            self.firstFloorHeight = 0
            self.floorPlanArray = None
            self.wallListMainFoundation = []
            self.wallListSecondFoundation = []
            self.wallListOuterCombined = []
            self.wallListInner = []
            self.roomListMainSquare = []
            self.roomListSecondSquare = []
            self.roomListCombined = []
            self.blockPattern = block_palette
            self.roadCoordinate = None

            # house creation methods
            self.buildLayer = build_layer
            self.createHouseFoundations(box, two_squares, two_floors)
            self.createFloorPlanArray(box)
            self.buildFloors(level, box)
            self.buildRoofs(level)
            self.createFirstFloorRoomDivisions(level, box)
            self.prepareFindWalls(level, box, front_direction)
            self.createStairCaseToSecondFloor(level, box)
            self.buildUpperFloors(level, box)
            self.buildSecondStoryWalls(level, box, front_direction)  # buildRoofs need to be called before this
            self.buildRoofLevelWalls(level)
            self.placeFurnitureInHouse(level, box)

    # returns object containing coordinates where house is located, and coordinates were road is connected
    def getHouseCoordData(self):
        return HouseCoordData(self.createHouseCoordList(), self.roadCoordinate)

    # create list of coordinates belonging to house structure
    def createHouseCoordList(self):
        coordList = []
        for x in range(self.mainSquare.xorigin, self.mainSquare.xorigin + self.mainSquare.xlength):
            for z in range(self.mainSquare.zorigin, self.mainSquare.zorigin + self.mainSquare.zlength):
                coordList.append(Coord(x, z))

        if self.secondarySquare is not None:
            for x in range(self.secondarySquare.xorigin, self.secondarySquare.xorigin + self.secondarySquare.xlength):
                for z in range(self.secondarySquare.zorigin, self.secondarySquare.zorigin + self.secondarySquare.zlength):
                    coordList.append(Coord(x, z))

        return coordList

    # assign room types to each room
    def assignRoomTypes(self, is_first_floor, is_main_square):
        if is_first_floor:
            if self.roomListCombined[0].roomType is None:
                self.roomListCombined[0].roomType = Furniture.RoomType.bedRoom
                self.roomListCombined[0].furnitureList = Furniture.bedRoomFurniture
            if len(self.roomListCombined) > 1:
                if self.roomListCombined[1].roomType is None:
                    self.roomListCombined[1].roomType = Furniture.RoomType.kitchen
                    self.roomListCombined[1].furnitureList = Furniture.kitchenFurniture
            if len(self.roomListCombined) > 2:
                if self.roomListCombined[len(self.roomListCombined) - 1].roomType is None:
                    self.roomListCombined[len(self.roomListCombined) - 1].roomType = Furniture.RoomType.livingRoom
                    self.roomListCombined[len(self.roomListCombined) - 1].furnitureList = Furniture.livingRoomFurniture
                for i in range(2, len(self.roomListCombined) - 1):
                    if self.roomListCombined[i].roomType is None:
                        if random.getrandbits(1):
                            self.roomListCombined[i].roomType = Furniture.RoomType.studyRoom
                            self.roomListCombined[i].furnitureList = Furniture.studyRoomFurniture
                        else:
                            self.roomListCombined[i].roomType = Furniture.RoomType.storage
                            self.roomListCombined[i].furnitureList = Furniture.storageFurniture
        else:
            roomList = self.roomListCombined

            for room in roomList:
                if random.getrandbits(1):
                    room.roomType = Furniture.RoomType.studyRoom
                    room.furnitureList = Furniture.studyRoomFurniture
                else:
                    room.roomType = Furniture.RoomType.storage
                    room.furnitureList = Furniture.storageFurniture

    # handles overall furnishing routine
    def placeFurnitureInHouse(self, level, box):
        # sorts list based on room size
        self.roomListMainSquare.sort(key=lambda x: x.roomArea, reverse=False)
        self.roomListSecondSquare.sort(key=lambda x: x.roomArea, reverse=False)
        self.roomListCombined.sort(key=lambda x: x.roomArea, reverse=False)

        self.placeFurnitureInSquare(level, box, True, True)
        self.placeFurnitureInSquare(level, box, True, False)
        if self.mainSquare.floors == 2:
            self.placeFurnitureInSquare(level, box, False, True)
        if self.secondarySquare is not None and self.secondarySquare.floors == 2:
            self.placeFurnitureInSquare(level, box, False, False)

    # handles placement of furniture in square
    def placeFurnitureInSquare(self, level, box, is_first_floor, is_main_square):
        # sort rooms and assign room types
        self.assignRoomTypes(is_first_floor, is_main_square)

        if is_main_square:
            roomList = self.roomListMainSquare
        else:
            roomList = self.roomListSecondSquare
        self.placeDoors(level, box, is_first_floor)

        for room in roomList:
            self.placeFurnitureInRoom(level, box, room, is_first_floor)

    # place doors
    def placeDoors(self, level, box, is_first_floor):
        if is_first_floor:
            y = self.buildLayer + self.foundationHeight + 1
        else:
            y = self.buildLayer + self.foundationHeight + self.firstFloorHeight + 2

        for x in range(abs(box.minx - self.mainSquare.xorigin),
                       abs(box.minx - self.mainSquare.xorigin) + self.mainSquare.xlength):
            for z in range(abs(box.minz - self.mainSquare.zorigin),
                           abs(box.minz - self.mainSquare.zorigin) + self.mainSquare.zlength):
                if self.floorPlanArray[x, z] == 5:
                    buildDoor = True
                    if not is_first_floor:  # don't build door on second floor on top of entrance door.
                        if self.floorPlanArray[x - 1, z] == 0 or self.floorPlanArray[x + 1, z] == 0 or \
                                self.floorPlanArray[x, z - 1] == 0 or self.floorPlanArray[x, z + 1] == 0:
                            buildDoor = False
                        if self.secondarySquare is not None and self.secondarySquare.floors != 2:
                            if x == abs(box.minx - self.mainSquare.xorigin) or \
                                    x == abs(box.minx - self.mainSquare.xorigin) + self.mainSquare.xlength - 1 or \
                                    z == abs(box.minz - self.mainSquare.zorigin) or \
                                    z == abs(box.minz - self.mainSquare.zorigin) + self.mainSquare.zlength - 1:
                                buildDoor = False
                    if self.floorPlanArray[x - 1, z] == 2 or self.floorPlanArray[x + 1, z] == 2:
                        blockBottom = self.blockPattern.blockDoorBottomX
                        blockTop = self.blockPattern.blockDoorTopX
                    else:
                        blockBottom = self.blockPattern.blockDoorBottomZ
                        blockTop = self.blockPattern.blockDoorTopZ
                    if buildDoor:
                        pymchelper.set_block(level, blockBottom, x + box.minx, y, z + box.minz)
                        pymchelper.set_block(level, blockTop, x + box.minx, y + 1, z + box.minz)
                    else:
                        pymchelper.set_block(level, self.blockPattern.blockDefault, x + box.minx, y, z + box.minz)
                        pymchelper.set_block(level, self.blockPattern.blockDefault, x + box.minx, y + 1, z + box.minz)

    # place furniture in room
    def placeFurnitureInRoom(self, level, box, room, is_first_floor):
        if room.roomType is not None:
            if is_first_floor:
                y = self.buildLayer + self.foundationHeight + 1
            else:
                y = self.buildLayer + self.foundationHeight + self.firstFloorHeight + 2
            furnitureLocationList = []
            for x in range(abs(box.minx - room.xOrigin), abs(box.minx - room.xOrigin) + room.xLength):
                for z in range(abs(box.minz - room.zOrigin), abs(box.minz - room.zOrigin) + room.zLength):
                    if self.floorPlanArray[x, z] == 6:
                        directionInt = 0
                        if self.floorPlanArray[x - 1, z] == 2:
                            directionInt = 4
                        elif self.floorPlanArray[x + 1, z] == 2:
                            directionInt = 5
                        elif self.floorPlanArray[x, z - 1] == 2 or (self.floorPlanArray[x - 1, z] == 6 and
                                                                    self.floorPlanArray[x, z - 1] == 6) or \
                                (self.floorPlanArray[x + 1, z] == 6 and self.floorPlanArray[x, z - 1] == 6):
                            directionInt = 2
                        elif self.floorPlanArray[x, z + 1] == 2 or (self.floorPlanArray[x - 1, z] == 6 and
                                                                    self.floorPlanArray[x, z + 1] == 6) or \
                                (self.floorPlanArray[x + 1, z] == 6 and self.floorPlanArray[x, z + 1] == 6):
                            directionInt = 3

                        furnitureLocationList.append((x, z, directionInt))

            # randomize list to make furniture appear in random locations in room
            random.shuffle(furnitureLocationList)

            for location in furnitureLocationList:
                pymchelper.set_block(level, pocketMaterials.Air, location[0] + box.minx, y, location[1] + box.minz)
            for location in furnitureLocationList:
                if len(room.furnitureList) > 0:
                    if location != furnitureLocationList[len(furnitureLocationList) - 1]:
                        index = random.randint(1, len(room.furnitureList) - 1)
                        # place first furniture
                        room.furnitureList[index](level, location[0] + box.minx, y, location[1] + box.minz, location[2])
                    else:
                        room.furnitureList[0](level, location[0] + box.minx, y, location[1] + box.minz, location[2])

            # place mat in living room
            if room.roomType == Furniture.RoomType.livingRoom:
                for x in range(1, room.xLength - 1):
                    for z in range(1, room.zLength - 1):
                        pymchelper.set_block(level, pocketMaterials[171, 14], x + room.xOrigin, y, z + room.zOrigin)

            # place stone tiles in kitchen
            if room.roomType == Furniture.RoomType.kitchen:
                for x in range(0, room.xLength):
                    for z in range(0, room.zLength):
                        pymchelper.set_block(level, pocketMaterials[43, 0], x + room.xOrigin, y - 1, z + room.zOrigin)

    # build staircase to second floor
    def createStairCaseToSecondFloor(self, level, box):
        if self.mainSquare.floors == 2:
            # finds walls in mainSquare (which contains the biggest room always)
            walls = []
            foundWalls = numpy.zeros((box.maxx - box.minx, box.maxz - box.minz), numpy.uint8)
            for x in range(0 + abs(box.minx - self.mainSquare.xorigin), 0 + abs(box.minx - self.mainSquare.xorigin) +
                                                                        self.mainSquare.xlength):
                for z in range(0 + abs(box.minz - self.mainSquare.zorigin), 0 + abs(box.minz - self.mainSquare.zorigin)
                                                                            + self.mainSquare.zlength):
                    if self.floorPlanArray[x, z] == 6 and foundWalls[x, z] == 0:
                        if self.floorPlanArray[x + 1, z] == 6:
                            offset = x
                            while self.floorPlanArray[offset, z] == 6:
                                foundWalls[offset, z] = 1
                                offset = offset + 1
                            wall = self.Wall()
                            wall.isXDirection = True
                            wall.xOrigin = box.minx + x
                            wall.zOrigin = box.minz + z
                            wall.length = offset - x
                            walls.append(wall)
                        elif self.floorPlanArray[x, z + 1] == 6:
                            offset = z
                            while self.floorPlanArray[x, offset] == 6:
                                foundWalls[x, offset] = 1
                                offset = offset + 1
                            wall = self.Wall()
                            wall.isXDirection = False
                            wall.xOrigin = box.minx + x
                            wall.zOrigin = box.minz + z
                            wall.length = offset - z
                            walls.append(wall)

            # gets longest wall
            longestWall = None
            maxLength = 0
            for currentWall in walls:
                if currentWall.length > maxLength:
                    maxLength = currentWall.length
                    longestWall = currentWall

            if longestWall.isXDirection:
                stairDirection = self.Direction.xpositive
                coordX = abs(box.minx - longestWall.xOrigin) - 1
                coordZ = abs(box.minz - longestWall.zOrigin)
            else:
                stairDirection = self.Direction.zpositive
                coordX = abs(box.minx - longestWall.xOrigin)
                coordZ = abs(box.minz - longestWall.zOrigin) - 1

            coordY = self.buildLayer + self.foundationHeight + 1
            while coordY < self.buildLayer + self.foundationHeight + 2 + self.firstFloorHeight:
                if stairDirection == self.Direction.xnegative:
                    if self.floorPlanArray[coordX - 1, coordZ] == 6 or self.floorPlanArray[coordX - 1, coordZ] == 2:
                        coordX = coordX - 1
                    else:
                        if self.floorPlanArray[coordX, coordZ - 1] == 6 or self.floorPlanArray[coordX, coordZ - 1] == 2:
                            stairDirection = self.Direction.znegative
                            coordZ = coordZ - 1
                        else:
                            stairDirection = self.Direction.zpositive
                            coordZ = coordZ + 1
                elif stairDirection == self.Direction.xpositive:
                    if self.floorPlanArray[coordX + 1, coordZ] == 6 or self.floorPlanArray[coordX + 1, coordZ] == 2:
                        coordX = coordX + 1
                    else:
                        if self.floorPlanArray[coordX, coordZ - 1] == 6 or self.floorPlanArray[coordX, coordZ - 1] == 2:
                            stairDirection = self.Direction.znegative
                            coordZ = coordZ - 1
                        else:
                            stairDirection = self.Direction.zpositive
                            coordZ = coordZ + 1
                elif stairDirection == self.Direction.znegative:
                    if self.floorPlanArray[coordX, coordZ - 1] == 6 or self.floorPlanArray[coordX, coordZ - 1] == 2:
                        coordZ = coordZ - 1
                    else:
                        if self.floorPlanArray[coordX - 1, coordZ] == 6 or self.floorPlanArray[coordX - 1, coordZ] == 2:
                            stairDirection = self.Direction.xnegative
                            coordX = coordX - 1
                        else:
                            stairDirection = self.Direction.xpositive
                            coordX = coordX + 1
                elif stairDirection == self.Direction.zpositive:
                    if self.floorPlanArray[coordX, coordZ + 1] == 6 or self.floorPlanArray[coordX, coordZ + 1] == 2:
                        coordZ = coordZ + 1
                    else:
                        if self.floorPlanArray[coordX - 1, coordZ] == 6 or self.floorPlanArray[coordX - 1, coordZ] == 2:
                            stairDirection = self.Direction.xnegative
                            coordX = coordX - 1
                        else:
                            stairDirection = self.Direction.xpositive
                            coordX = coordX + 1

                stairBlock = None
                self.floorPlanArray[coordX, coordZ] = 7
                if stairDirection == self.Direction.xnegative:
                    stairBlock = pocketMaterials[53, 1]
                elif stairDirection == self.Direction.xpositive:
                    stairBlock = pocketMaterials[53, 0]
                elif stairDirection == self.Direction.znegative:
                    stairBlock = pocketMaterials[53, 3]
                elif stairDirection == self.Direction.zpositive:
                    stairBlock = pocketMaterials[53, 2]
                pymchelper.set_block(level, stairBlock, coordX + box.minx, coordY,
                                     coordZ + box.minz)
                coordY = coordY + 1

            # sets all 6-marked positions beside stairs to 2 (so no furniture will be placed beside stairs)
            for x in range(0, self.mainSquare.xlength):
                for z in range(0, self.mainSquare.zlength):
                    if self.floorPlanArray[x + abs(box.minx - self.mainSquare.xorigin),
                                           z + abs(box.minz - self.mainSquare.zorigin)] == 6:
                        if self.floorPlanArray[x + abs(box.minx - self.mainSquare.xorigin) - 1,
                                               z + abs(box.minz - self.mainSquare.zorigin)] == 7 or \
                                self.floorPlanArray[x + abs(box.minx - self.mainSquare.xorigin) + 1,
                                                    z + abs(box.minz - self.mainSquare.zorigin)] == 7 or \
                                self.floorPlanArray[x + abs(box.minx - self.mainSquare.xorigin),
                                                    z + abs(box.minz - self.mainSquare.zorigin) - 1] == 7 or \
                                self.floorPlanArray[x + abs(box.minx - self.mainSquare.xorigin),
                                                    z + abs(box.minz - self.mainSquare.zorigin) + 1] == 7:
                            self.floorPlanArray[x + abs(box.minx - self.mainSquare.xorigin),
                                                z + abs(box.minz - self.mainSquare.zorigin)] = 2

    # create second floor outline around square
    def createSecondFloorOutline(self, level, box, square):
        placeBlock = False
        for x in range(0, square.xlength):
            for z in range(0, square.zlength):
                if self.floorPlanArray[x + abs(box.minx - square.xorigin),
                                       z + abs(box.minz - square.zorigin)] == 1 or \
                        self.floorPlanArray[x + abs(box.minx - square.xorigin),
                                            z + abs(box.minz - square.zorigin)] == 3:
                    placeBlock = True
                # if x == 0 or x == square.xlength or z == 0 or z == square.zlength:
                if self.floorPlanArray[x + abs(box.minx - square.xorigin), z + abs(box.minz - square.zorigin)] == 5:
                    placeBlock = True
                if placeBlock:
                    pymchelper.set_block(level, self.blockPattern.blockAtFloorLevel, x + square.xorigin,
                                         self.buildLayer + self.foundationHeight + 1 + self.firstFloorHeight,
                                         z + square.zorigin)
                    placeBlock = False

    # build second story walls
    def buildSecondStoryWalls(self, level, box, front_direction):
        # main foundation
        if self.mainSquare.floors == 2:
            self.createSecondFloorOutline(level, box, self.mainSquare)
            self.buildInsideWalls(level, box, self.buildLayer + self.foundationHeight + 2 + self.firstFloorHeight,
                                  self.buildLayer + self.foundationHeight + 5 + self.firstFloorHeight,
                                  self.mainSquare)
            self.buildWalls(level, box, False, False, self.wallListMainFoundation, self.mainSquare, front_direction)

        # secondary square
        if self.secondarySquare is not None and self.secondarySquare.floors == 2:
            self.createSecondFloorOutline(level, box, self.secondarySquare)
            self.buildInsideWalls(level, box, self.buildLayer + self.foundationHeight + 2 + self.firstFloorHeight,
                                  self.buildLayer + self.foundationHeight + 5 + self.firstFloorHeight,
                                  self.secondarySquare)
            self.buildWalls(level, box, False, False, self.wallListSecondFoundation, self.secondarySquare,
                            front_direction)

        # covers doorway on 2nd floor leading out to secondary square in case secondary square only has one floor
        elif self.secondarySquare is not None and self.secondarySquare.floors == 2:
            for x in range(-1, self.secondarySquare.xlength + 1):
                for z in range(-1, self.secondarySquare.zlength + 1):
                    if x == -1 or x == self.secondarySquare.xlength + 1 or z == -1 or \
                            z == self.secondarySquare.zlength + 1:
                        if self.floorPlanArray[x + abs(box.minx - self.secondarySquare.xorigin),
                                               z + abs(box.minz - self.secondarySquare.zorigin)] == 5:
                            for y in range(self.buildLayer + self.foundationHeight + 2 + self.firstFloorHeight,
                                           self.buildLayer + self.foundationHeight + self.firstFloorHeight + 5):
                                pymchelper.set_block(level, self.blockPattern.blockDefault,
                                                     x + self.secondarySquare.xorigin, y,
                                                     z + self.secondarySquare.zorigin)

        # builds up walls reaching to the ceiling
        if self.mainSquare.floors == 2:
            for x in range(0, self.mainSquare.xlength):
                for z in range(0, self.mainSquare.zlength):
                    if self.floorPlanArray[x + abs(box.minx - self.mainSquare.xorigin),
                                           z + abs(box.minz - self.mainSquare.zorigin)] == 3 or \
                            self.floorPlanArray[x + abs(box.minx - self.mainSquare.xorigin),
                                                z + abs(box.minz - self.mainSquare.zorigin)] == 4 or \
                            self.floorPlanArray[x + abs(box.minx - self.mainSquare.xorigin),
                                                z + abs(box.minz - self.mainSquare.zorigin)] == 5:
                        y = 0
                        while not pymchelper.has_material(level, self.blockPattern.blockRoof, self.mainSquare.xorigin +
                                                          x, self.buildLayer + self.foundationHeight +
                                                          self.firstFloorHeight + 5 + y, self.mainSquare.zorigin + z):
                            pymchelper.set_block(level, self.blockPattern.blockDefault, self.mainSquare.xorigin + x,
                                                 self.buildLayer + self.foundationHeight + self.firstFloorHeight + 5
                                                 + y, self.mainSquare.zorigin + z)
                            y = y + 1
                            if y > self.buildLayer + self.foundationHeight + self.firstFloorHeight + 20:
                                return
                        if not pymchelper.has_material(level, self.blockPattern.blockRoof, x + self.mainSquare.xorigin,
                                                       self.buildLayer + self.foundationHeight + self.firstFloorHeight +
                                                       5, z + self.mainSquare.zorigin):
                            pymchelper.set_block(level, self.blockPattern.blockAtFloorLevel, x +
                                                 self.mainSquare.xorigin, self.buildLayer + self.foundationHeight +
                                                 self.firstFloorHeight + 5, z + self.mainSquare.zorigin)

    # build inside walls
    def buildInsideWalls(self, level, box, y_start_height, y_end_height, square):
        if square is not None:
            for y in range(y_start_height, y_end_height):
                for x in range(0, square.xlength):
                    for z in range(0, square.zlength):
                        if self.floorPlanArray[x + abs(box.minx - square.xorigin),
                                               z + abs(box.minz - square.zorigin)] == 4:
                            pymchelper.set_block(level, self.blockPattern.blockDefault, x + square.xorigin, y,
                                                 z + square.zorigin)
                        elif self.floorPlanArray[x + abs(box.minx - square.xorigin),
                                                 z + abs(box.minz - square.zorigin)] == 5:
                            if y == y_start_height + 2:
                                pymchelper.set_block(level, self.blockPattern.blockAboveDoor, x + square.xorigin, y,
                                                     z + square.zorigin)
                            if y > y_start_height + 2:
                                pymchelper.set_block(level, self.blockPattern.blockDefault, x + square.xorigin, y,
                                                     z + square.zorigin)

    # create wall split
    def createInsideWall(self, level, box, room):
        if room.xlength > room.zlength:
            xDirectionSplit = False
        else:
            xDirectionSplit = True
        size = 9  # number to be experimented with
        xoriginTranslated = abs(box.minx - room.xorigin)
        zoriginTranslated = abs(box.minz - room.zorigin)
        self.floorPlanArray[xoriginTranslated, zoriginTranslated] = 3
        self.floorPlanArray[xoriginTranslated + room.xlength - 1, zoriginTranslated] = 3
        self.floorPlanArray[xoriginTranslated, zoriginTranslated + room.zlength - 1] = 3
        self.floorPlanArray[xoriginTranslated + room.xlength - 1, zoriginTranslated + room.zlength - 1] = 3
        if xDirectionSplit:
            if room.zlength > size:
                index = random.randint(zoriginTranslated + 4, zoriginTranslated + room.zlength - 5)
            else:
                return None
        else:
            if room.xlength > size:
                index = random.randint(xoriginTranslated + 4, xoriginTranslated + room.xlength - 5)
            else:
                return None

        for x in range(0, room.xlength):
            for z in range(0, room.zlength):
                if self.floorPlanArray[x + xoriginTranslated, z + zoriginTranslated] == 2:
                    if xDirectionSplit and z + zoriginTranslated == index:
                        self.floorPlanArray[x + xoriginTranslated, z + zoriginTranslated] = 4
                    elif not xDirectionSplit and x + xoriginTranslated == index:
                        self.floorPlanArray[x + xoriginTranslated, z + zoriginTranslated] = 4

        newRoom = self.FoundationSquare()
        newRoom.xorigin = room.xorigin
        newRoom.zorigin = room.zorigin
        if xDirectionSplit:
            newRoom.xlength = room.xlength
            newRoom.zlength = index - zoriginTranslated + 1
        else:
            newRoom.xlength = index - xoriginTranslated + 1
            newRoom.zlength = room.zlength

        self.createInsideWall(level, box, newRoom)  # recursive call

    # find rooms
    def findRooms(self, box, is_main_square):
        startX = 0
        startZ = 0
        endX = 0
        endZ = 0
        if is_main_square:
            startX = abs(box.minx - self.mainSquare.xorigin)
            startZ = abs(box.minz - self.mainSquare.zorigin)
            endX = startX + self.mainSquare.xlength
            endZ = startZ + self.mainSquare.zlength
        elif self.secondarySquare is not None:
            startX = abs(box.minx - self.secondarySquare.xorigin) + 1
            startZ = abs(box.minz - self.secondarySquare.zorigin) + 1
            endX = startX + 1  # ugly hack because second square only contains one room
            endZ = startZ + 1  # ugly hack because second square only contains one room

        tempArray = numpy.zeros((box.maxx - box.minx, box.maxz - box.minz), numpy.uint8)
        # creates room objects out of the divided segments of the house
        for x in range(startX, endX):
            for z in range(startZ, endZ):
                if self.floorPlanArray[x, z] == 2 or self.floorPlanArray[x, z] == 6:
                    if tempArray[x, z] == 0:
                        offsetX = x
                        offsetZ = z
                        originalXCoord = x
                        originalZCoord = z
                        longestX = 0
                        longestZ = 0
                        while self.floorPlanArray[offsetX, offsetZ] == 2 or \
                                self.floorPlanArray[offsetX, offsetZ] == 6:
                            while self.floorPlanArray[offsetX, offsetZ] == 2 or \
                                    self.floorPlanArray[offsetX, offsetZ] == 6:
                                tempArray[offsetX, offsetZ] = 1
                                offsetX = offsetX + 1
                                if offsetX - originalXCoord > longestX:
                                    longestX = offsetX - originalXCoord
                                if offsetZ - originalZCoord > longestZ:
                                    longestZ = offsetZ - originalZCoord
                            offsetX = x
                            offsetZ = offsetZ + 1
                        # creating room
                        room = self.Room()
                        room.xOrigin = box.minx + originalXCoord
                        room.zOrigin = box.minz + originalZCoord
                        room.xLength = longestX
                        room.zLength = longestZ + 1
                        room.roomArea = room.xLength * room.zLength
                        if is_main_square:
                            self.roomListMainSquare.append(room)
                        else:
                            self.roomListSecondSquare.append(room)

    # create first floor room divisions
    def createFirstFloorRoomDivisions(self, level, box):
        # clearing space in house
        self.clearIndoorSpace(level, box, self.buildLayer + self.foundationHeight + 1, True)

        # for creating wall through intersection of main and second foundation
        for y in range(self.buildLayer + self.foundationHeight + 1,
                       self.buildLayer + self.foundationHeight + 2 + self.firstFloorHeight - 1):
            for x in range(0, self.mainSquare.xlength):
                for z in range(0, self.mainSquare.zlength):
                    if self.floorPlanArray[x + abs(box.minx - self.mainSquare.xorigin),
                                           z + abs(box.minz - self.mainSquare.zorigin)] == 2:
                        self.floorPlanArray[x + abs(box.minx - self.mainSquare.xorigin),
                                            z + abs(box.minz - self.mainSquare.zorigin)] = 4
            for x in range(1, self.mainSquare.xlength - 1):
                for z in range(1, self.mainSquare.zlength - 1):
                    self.floorPlanArray[x + abs(box.minx - self.mainSquare.xorigin),
                                        z + abs(box.minz - self.mainSquare.zorigin)] = 2

        self.createInsideWall(level, box, self.mainSquare)
        self.findInnerWalls(box)
        for wall in self.wallListInner:
            self.createDoorwayForInnerWall(box, wall)

        self.findRooms(box, True)
        self.findRooms(box, False)
        self.roomListCombined = self.roomListMainSquare + self.roomListSecondSquare

        self.buildInsideWalls(level, box, self.buildLayer + self.foundationHeight + 1,
                              self.buildLayer + self.foundationHeight + 2 + self.firstFloorHeight - 1,
                              self.mainSquare)
        self.buildInsideWalls(level, box, self.buildLayer + self.foundationHeight + 1,
                              self.buildLayer + self.foundationHeight + 2 + self.firstFloorHeight - 1,
                              self.secondarySquare)

        # set cells in array used to determine which blocks can have furniture on
        self.setCellsInArrayForFurnishing()

    # set cells to certain values used for furnishing
    def setCellsInArrayForFurnishing(self):
        # set blocks neighbouring outer or inner walls to 6
        for x in range(0, self.floorPlanArray.shape[0]):
            for z in range(0, self.floorPlanArray.shape[1]):
                if self.floorPlanArray[x, z] == 2:
                    if self.floorPlanArray[x - 1, z] != 5 and self.floorPlanArray[x + 1, z] != 5 and \
                            self.floorPlanArray[x, z - 1] != 5 and self.floorPlanArray[x, z + 1] != 5:
                        if self.floorPlanArray[x - 1, z] == 1 or self.floorPlanArray[x - 1, z] == 4 or \
                                self.floorPlanArray[x - 1, z] == 3 or self.floorPlanArray[x + 1, z] == 1 or \
                                self.floorPlanArray[x + 1, z] == 4 or self.floorPlanArray[x + 1, z] == 3 or \
                                self.floorPlanArray[x, z - 1] == 1 or self.floorPlanArray[x, z - 1] == 4 or \
                                self.floorPlanArray[x, z - 1] == 3 or self.floorPlanArray[x, z + 1] == 1 or \
                                self.floorPlanArray[x, z + 1] == 4 or self.floorPlanArray[x, z + 1] == 3:
                            self.floorPlanArray[x, z] = 6
                    if self.floorPlanArray[x - 1, z - 1] == 5 or self.floorPlanArray[x - 1, z + 1] == 5 or \
                            self.floorPlanArray[x + 1, z - 1] == 5 or self.floorPlanArray[x + 1, z + 1] == 5:
                        self.floorPlanArray[x, z] = 2

    # create door for inner wall
    def createDoorwayForInnerWall(self, box, wall):
        if wall.length >= 3:
            if wall.isXDirection:
                coordX = random.randint(1, wall.length - 2)
                self.floorPlanArray[abs(box.minx - wall.xOrigin) + coordX, abs(box.minz - wall.zOrigin)] = 5
            else:
                coordZ = random.randint(1, wall.length - 2)
                self.floorPlanArray[abs(box.minx - wall.xOrigin), abs(box.minz - wall.zOrigin) + coordZ] = 5
        else:
            self.floorPlanArray[abs(box.minx - wall.xOrigin), abs(box.minz - wall.zOrigin)] = 5

    # find inner walls
    def findInnerWalls(self, box):
        # find walls
        foundWalls = numpy.zeros((box.maxx - box.minx, box.maxz - box.minz), numpy.uint8)
        for x in range(0, self.floorPlanArray.shape[0]):
            for z in range(0, self.floorPlanArray.shape[1]):
                if self.floorPlanArray[x, z] == 4 and foundWalls[x, z] == 0:
                    #  determine if horizontal or vertical wall
                    if (self.floorPlanArray[x - 1, z] == 2 and self.floorPlanArray[x + 1, z] == 2) or \
                            (self.floorPlanArray[x - 1, z] == 2 and self.floorPlanArray[x + 1, z] == 2):
                        offset = z
                        while self.floorPlanArray[x, offset] == 4:
                            foundWalls[x, offset] = 1
                            offset = offset + 1
                        # Creating wall
                        wall = self.Wall()
                        wall.isXDirection = False
                        wall.xOrigin = box.minx + x
                        wall.zOrigin = box.minz + z
                        wall.length = offset - z
                        wall.height = self.firstFloorHeight
                        self.wallListInner.append(wall)
                    elif (self.floorPlanArray[x, z - 1] == 2 and self.floorPlanArray[x, z + 1] == 2) or \
                            (self.floorPlanArray[x, z - 1] == 2 and self.floorPlanArray[x, z + 1] == 2):
                        offset = x
                        while self.floorPlanArray[offset, z] == 4:
                            foundWalls[offset, z] = 1
                            offset = offset + 1
                        # Creating a wall
                        wall = self.Wall()
                        wall.isXDirection = True
                        wall.xOrigin = box.minx + x
                        wall.zOrigin = box.minz + z
                        wall.length = offset - x
                        wall.height = self.firstFloorHeight
                        self.wallListInner.append(wall)

    # create floor plan 2d array
    def createFloorPlanArray(self, box):
        self.floorPlanArray = numpy.zeros((box.maxx - box.minx, box.maxz - box.minz), numpy.uint8)

        # main foundation
        for x in range(0, self.mainSquare.xlength):
            for z in range(0, self.mainSquare.zlength):
                self.floorPlanArray[x + abs(box.minx - self.mainSquare.xorigin),
                                    z + abs(box.minz - self.mainSquare.zorigin)] = 1

        # second foundation
        if self.secondarySquare is not None:
            for x in range(0, self.secondarySquare.xlength):
                for z in range(0, self.secondarySquare.zlength):
                    self.floorPlanArray[x + abs(box.minx - self.secondarySquare.xorigin),
                                        z + abs(box.minz - self.secondarySquare.zorigin)] = 1

        # set inside area to 2
        for x in range(1, self.floorPlanArray.shape[0] - 1):
            for z in range(1, self.floorPlanArray.shape[1] - 1):
                if self.floorPlanArray[x - 1, z] == 1 or self.floorPlanArray[x - 1, z] == 2:
                    if self.floorPlanArray[x + 1, z] == 1 or self.floorPlanArray[x + 1, z] == 2:
                        if self.floorPlanArray[x, z - 1] == 1 or self.floorPlanArray[x, z - 1] == 2:
                            if self.floorPlanArray[x, z + 1] == 1 or self.floorPlanArray[x, z + 1] == 2:
                                if self.floorPlanArray[x - 1, z - 1] != 0 and \
                                        self.floorPlanArray[x + 1, z - 1] != 0 and \
                                        self.floorPlanArray[x - 1, z + 1] != 0 and \
                                        self.floorPlanArray[x + 1, z + 1] != 0:
                                    self.floorPlanArray[x, z] = 2

        # set corner walls
        for x in range(1, self.floorPlanArray.shape[0] - 1):
            for z in range(1, self.floorPlanArray.shape[1] - 1):
                if self.floorPlanArray[x, z] == 1:
                    onesAround = 0
                    twosAround = 0
                    if self.floorPlanArray[x - 1, z] == 0:
                        onesAround = onesAround + 1
                    elif self.floorPlanArray[x - 1, z] == 2:
                        twosAround = twosAround + 1
                    if self.floorPlanArray[x + 1, z] == 0:
                        onesAround = onesAround + 1
                    elif self.floorPlanArray[x + 1, z] == 2:
                        twosAround = twosAround + 1
                    if self.floorPlanArray[x, z - 1] == 0:
                        onesAround = onesAround + 1
                    elif self.floorPlanArray[x, z - 1] == 2:
                        twosAround = twosAround + 1
                    if self.floorPlanArray[x, z + 1] == 0:
                        onesAround = onesAround + 1
                    elif self.floorPlanArray[x, z + 1] == 2:
                        twosAround = twosAround + 1
                    if onesAround >= 2 or twosAround >= 2:
                        self.floorPlanArray[x, z] = 3

    # build second story floors
    def buildUpperFloors(self, level, box):
        iteration = 1
        yHeightIncrease = 0
        for floors in range(0, iteration):
            for x in range(0, self.floorPlanArray.shape[0]):
                for z in range(0, self.floorPlanArray.shape[1]):
                    buildIt = False
                    if self.floorPlanArray[x, z] == 2 or self.floorPlanArray[x, z] == 4 or \
                            self.floorPlanArray[x, z] == 5 or self.floorPlanArray[x, z] == 6:
                        buildIt = True
                    if self.mainSquare.floors == 1 and floors == 0 and self.floorPlanArray[x, z] == 5:
                        buildIt = False
                    if buildIt:
                        pymchelper.set_block(level, self.blockPattern.flooring, box.minx + x, self.buildLayer +
                                             self.foundationHeight + self.firstFloorHeight + 1 + yHeightIncrease,
                                             box.minz + z)
            yHeightIncrease = 4

    # build roof
    def buildRoof(self, level, square, start_height, roof_direction_x, is_main_square):
        if roof_direction_x:
            lengthStart = square.xorigin - 1
            lengthEnd = square.xorigin + square.xlength + 1
            roofHeight = int(round(square.zlength / 2) + 1)
        else:
            lengthStart = square.zorigin - 1
            lengthEnd = square.zorigin + square.zlength + 1
            roofHeight = int(round(square.xlength / 2) + 1)
        for y in range(0, roofHeight):
            for length in range(lengthStart, lengthEnd):
                if roof_direction_x:
                    xOne = length
                    xTwo = length
                    zOne = square.zorigin + y - 1
                    zTwo = square.zorigin + square.zlength - y
                else:
                    xOne = square.xorigin + y - 1
                    xTwo = square.xorigin + square.xlength - y
                    zOne = length
                    zTwo = length

                pymchelper.set_block(level, self.blockPattern.blockRoof, xOne, start_height + y - 2, zOne)
                pymchelper.set_block(level, self.blockPattern.blockRoof, xTwo, start_height + y - 2, zTwo)

        # putting out extra block on border of roof-top to make it seamless
        if not is_main_square:
            if self.mainSquare.floors == self.secondarySquare.floors:
                direction = self.getBasedSecondDirection(self.secondarySquare)
                if roofHeight > 3:
                    for y in range(3, roofHeight):
                        if roof_direction_x:
                            zOne = square.zorigin + y - 1
                            zTwo = square.zorigin + square.zlength - y
                            if direction == self.Direction.xnegative:
                                xOne = square.xorigin - 2
                                xTwo = square.xorigin - 2
                            else:
                                xOne = square.xorigin + square.xlength + 1
                                xTwo = square.xorigin + square.xlength + 1
                        else:
                            xOne = square.xorigin + y - 1
                            xTwo = square.xorigin + square.xlength - y
                            if direction == self.Direction.znegative:
                                zOne = square.zorigin - 2
                                zTwo = square.zorigin - 2
                            else:
                                zOne = square.zorigin + square.zlength + 1
                                zTwo = square.zorigin + square.zlength + 1

                        self.buildLine(level, self.blockPattern.blockRoof, direction, xOne, start_height + y - 2, zOne)
                        self.buildLine(level, self.blockPattern.blockRoof, direction, xTwo, start_height + y - 2, zTwo)

    # builds straight line with block until hits certain block
    def buildLine(self, level, build_material, direction, x, y, z):
        while pymchelper.has_material(level, pocketMaterials.Air, x, y, z):
            pymchelper.set_block(level, build_material, x, y, z)
            if direction == self.Direction.xnegative:
                x = x - 1
            elif direction == self.Direction.xpositive:
                x = x + 1
            elif direction == self.Direction.znegative:
                z = z - 1
            elif direction == self.Direction.zpositive:
                z = z + 1

    # build roofs
    def buildRoofs(self, level):
        # main foundation roof
        if self.mainSquare.floors == 2:
            yAdditionMain = 6
        else:
            yAdditionMain = 2
        if self.secondarySquare is not None and self.secondarySquare.floors == 2:
            yAdditionSecondary = 6
        else:
            yAdditionSecondary = 2
        self.buildRoof(level, self.mainSquare, self.buildLayer + self.foundationHeight + self.firstFloorHeight +
                       yAdditionMain, self.mainSquare.xDirection, True)

        # second foundation roof
        if self.secondarySquare is not None:
            self.buildRoof(level, self.secondarySquare, self.buildLayer + self.foundationHeight + 1 +
                           self.firstFloorHeight + yAdditionSecondary - 1, self.secondarySquare.xDirection, False)

        # second roof
        if self.secondarySquare is not None:
            self.cleanRoofInside(level, self.secondarySquare, self.buildLayer + self.foundationHeight +
                                 1 + self.firstFloorHeight + yAdditionSecondary - 1, self.secondarySquare.xDirection)

    # clean roof insides
    def cleanRoofInside(self, level, square, start_height, roof_direction_x):
        direction = self.getBasedSecondDirection(square)
        if roof_direction_x:
            if direction == self.Direction.xnegative:
                lengthStart = square.xorigin - int(round(self.mainSquare.xlength / 2))
                lengthEnd = square.xorigin + square.xlength + 1
            else:
                lengthStart = square.xorigin - 1
                lengthEnd = square.xorigin + square.xlength + int(round(self.mainSquare.xlength / 2))
            widthStart = square.zorigin
            widthEnd = square.zorigin + square.zlength
            roofHeight = int(round(square.zlength / 2) + 1)
        else:
            if direction == self.Direction.znegative:
                lengthStart = square.zorigin - int(round(self.mainSquare.zlength / 2))
                lengthEnd = square.zorigin + square.zlength + 1
            else:
                lengthStart = square.zorigin - 1
                lengthEnd = square.zorigin + square.zlength + int(round(self.mainSquare.zlength / 2))
            widthStart = square.xorigin
            widthEnd = square.xorigin + square.xlength
            roofHeight = int(round(square.xlength / 2) + 1)
        for y in range(0, roofHeight):
            for length in range(lengthStart, lengthEnd):
                for width in range(widthStart + y, widthEnd - y):
                    if roof_direction_x:
                        x = length
                        z = width
                    else:
                        x = width
                        z = length
                    pymchelper.set_block(level, pocketMaterials.Air, x, start_height + y - 2, z)

    # build second floor walls
    def buildRoofLevelWalls(self, level):
        chanceToBuildWindow = 0  # variable to be experimented with
        if self.mainSquare.floors == 2:
            yHeightIncreaseMain = 4
        else:
            yHeightIncreaseMain = 0

        # main square
        if self.mainSquare.xDirection:
            for edgeWallBlock in range(self.mainSquare.zorigin + 1,
                                       self.mainSquare.zorigin + self.mainSquare.zlength - 1):
                # builds floor-level wall-blocks
                pymchelper.set_block(level, self.blockPattern.blockAtFloorLevel, self.mainSquare.xorigin,
                                     self.buildLayer + self.foundationHeight + 1 + yHeightIncreaseMain +
                                     self.firstFloorHeight, edgeWallBlock)
                pymchelper.set_block(level, self.blockPattern.blockAtFloorLevel,
                                     self.mainSquare.xorigin + self.mainSquare.xlength - 1,
                                     self.buildLayer + self.foundationHeight + 1 + yHeightIncreaseMain +
                                     self.firstFloorHeight, edgeWallBlock)
            # builds default blocks
            for height in range(0, int(round(self.mainSquare.zlength / 2)) - 2):
                for edgeWallBlock in range(self.mainSquare.zorigin + height + 2,
                                           self.mainSquare.zorigin + self.mainSquare.zlength - height - 2):
                    pymchelper.set_block(level, self.blockPattern.blockDefault, self.mainSquare.xorigin,
                                         height + self.buildLayer + self.foundationHeight + 1 + yHeightIncreaseMain +
                                         self.firstFloorHeight + 1, edgeWallBlock)
                    pymchelper.set_block(level, self.blockPattern.blockDefault,
                                         self.mainSquare.xorigin + self.mainSquare.xlength - 1,
                                         height + self.buildLayer + self.foundationHeight + 1 + + yHeightIncreaseMain +
                                         self.firstFloorHeight + 1, edgeWallBlock)
            # builds windows
            if random.randint(0, chanceToBuildWindow) == 0:
                for height in range(self.buildLayer + self.foundationHeight + 1 + self.firstFloorHeight + 2 +
                                    yHeightIncreaseMain,
                                    self.buildLayer + self.foundationHeight + 1 + self.firstFloorHeight + 2 +
                                    yHeightIncreaseMain + int(round(self.mainSquare.zlength / 2)) - 4):
                    pymchelper.set_block(level, self.blockPattern.blockForWindow,
                                         self.mainSquare.xorigin, height,
                                         self.mainSquare.zorigin + int(round(self.mainSquare.zlength / 2)) - 1)
                    if self.mainSquare.zlength % 2 == 0:  # if number is even
                        pymchelper.set_block(level, self.blockPattern.blockForWindow,
                                             self.mainSquare.xorigin, height,
                                             self.mainSquare.zorigin +
                                             int(round(self.mainSquare.zlength / 2)))

            if random.randint(0, chanceToBuildWindow) == 0:
                for height in range(self.buildLayer + self.foundationHeight + 1 + self.firstFloorHeight + 2 +
                                    yHeightIncreaseMain,
                                    self.buildLayer + self.foundationHeight + 1 + self.firstFloorHeight + 2 +
                                    yHeightIncreaseMain + int(round(self.mainSquare.zlength / 2)) - 4):
                    pymchelper.set_block(level, self.blockPattern.blockForWindow,
                                         self.mainSquare.xorigin + self.mainSquare.xlength - 1, height,
                                         self.mainSquare.zorigin + int(round(self.mainSquare.zlength / 2)) - 1)
                    if self.mainSquare.zlength % 2 == 0:  # if number is even
                        pymchelper.set_block(level, self.blockPattern.blockForWindow,
                                             self.mainSquare.xorigin + self.mainSquare.xlength - 1, height,
                                             self.mainSquare.zorigin + int(round(self.mainSquare.zlength / 2)))

        else:
            for edgeWallBlock in range(self.mainSquare.xorigin + 1,
                                       self.mainSquare.xorigin + self.mainSquare.xlength - 1):
                # builds floor-level wall-blocks
                pymchelper.set_block(level, self.blockPattern.blockAtFloorLevel, edgeWallBlock,
                                     self.buildLayer + self.foundationHeight + 1 + yHeightIncreaseMain +
                                     self.firstFloorHeight, self.mainSquare.zorigin)
                pymchelper.set_block(level, self.blockPattern.blockAtFloorLevel, edgeWallBlock,
                                     self.buildLayer + self.foundationHeight + 1 + yHeightIncreaseMain +
                                     self.firstFloorHeight,
                                     self.mainSquare.zorigin + self.mainSquare.zlength - 1)
            # builds default blocks
            for height in range(0, int(round(self.mainSquare.xlength / 2)) - 2):
                for edgeWallBlock in range(self.mainSquare.xorigin + height + 2,
                                           self.mainSquare.xorigin + self.mainSquare.xlength - height - 2):
                    pymchelper.set_block(level, self.blockPattern.blockDefault, edgeWallBlock,
                                         height + self.buildLayer + self.foundationHeight + 1 + yHeightIncreaseMain +
                                         self.firstFloorHeight + 1, self.mainSquare.zorigin)
                    pymchelper.set_block(level, self.blockPattern.blockDefault, edgeWallBlock,
                                         height + self.buildLayer + self.foundationHeight + 1 + yHeightIncreaseMain +
                                         self.firstFloorHeight + 1,
                                         self.mainSquare.zorigin + self.mainSquare.zlength - 1)
            # builds windows
            if random.randint(0, chanceToBuildWindow) == 0:
                for height in range(self.buildLayer + self.foundationHeight + 1 + self.firstFloorHeight + 2 +
                                    yHeightIncreaseMain, self.buildLayer + self.foundationHeight + 1 +
                                    self.firstFloorHeight + 2 + yHeightIncreaseMain +
                                    int(round(self.mainSquare.xlength / 2)) - 4):
                    pymchelper.set_block(level, self.blockPattern.blockForWindow,
                                         self.mainSquare.xorigin + int(round(self.mainSquare.xlength / 2) - 1),
                                         height, self.mainSquare.zorigin)
                    if self.mainSquare.xlength % 2 == 0:  # if number is even
                        pymchelper.set_block(level, self.blockPattern.blockForWindow,
                                             self.mainSquare.xorigin + int(round(self.mainSquare.xlength / 2)),
                                             height, self.mainSquare.zorigin)

            if random.randint(0, chanceToBuildWindow) == 0:
                for height in range(self.buildLayer + self.foundationHeight + 1 + self.firstFloorHeight + 2 +
                                    yHeightIncreaseMain, self.buildLayer + self.foundationHeight + 1 +
                                    self.firstFloorHeight + 2 + yHeightIncreaseMain +
                                    int(round(self.mainSquare.xlength / 2)) - 4):
                    pymchelper.set_block(level, self.blockPattern.blockForWindow,
                                         self.mainSquare.xorigin + int(round(self.mainSquare.xlength / 2) - 1),
                                         height, self.mainSquare.zorigin + self.mainSquare.zlength - 1)
                    if self.mainSquare.xlength % 2 == 0:  # if number is even
                        pymchelper.set_block(level, self.blockPattern.blockForWindow,
                                             self.mainSquare.xorigin + int(round(self.mainSquare.xlength / 2)),
                                             height, self.mainSquare.zorigin + self.mainSquare.zlength - 1)
        # second foundation
        if self.secondarySquare is not None:
            if self.secondarySquare.floors == 2:
                yHeightIncreaseSecondary = 4
            else:
                yHeightIncreaseSecondary = 0

            direction = self.getBasedSecondDirection(self.secondarySquare)
            if self.secondarySquare.xDirection:
                if direction == self.Direction.xpositive:
                    xCoord = self.secondarySquare.xorigin
                else:
                    xCoord = self.secondarySquare.xorigin + self.secondarySquare.xlength - 1
                for iteration in range(self.secondarySquare.zorigin + 1,
                                       self.secondarySquare.zorigin + self.secondarySquare.zlength - 1):
                    # builds floor-level wall-blocks
                    pymchelper.set_block(level, self.blockPattern.blockAtFloorLevel, xCoord, yHeightIncreaseSecondary +
                                         self.buildLayer + self.foundationHeight + 1 + self.firstFloorHeight,
                                         iteration)
                # builds default blocks
                for height in range(0, int(round(self.secondarySquare.zlength / 2)) - 2):
                    for edgeWallBlock in range(self.secondarySquare.zorigin + height + 2,
                                               self.secondarySquare.zorigin + self.secondarySquare.zlength -
                                               height - 2):
                        pymchelper.set_block(level, self.blockPattern.blockDefault, xCoord, yHeightIncreaseSecondary +
                                             height + self.buildLayer + self.foundationHeight + 1 +
                                             self.firstFloorHeight + 1, edgeWallBlock)
                # builds windows
                if random.randint(0, chanceToBuildWindow) == 0:
                    for height in range(self.buildLayer + self.foundationHeight + 1 + self.firstFloorHeight + 2 +
                                        yHeightIncreaseSecondary, self.buildLayer + self.foundationHeight + 1 +
                                        self.firstFloorHeight + 2 + yHeightIncreaseSecondary +
                                        int(round(self.secondarySquare.zlength / 2)) - 4):
                        pymchelper.set_block(level, self.blockPattern.blockForWindow, xCoord, height,
                                             self.secondarySquare.zorigin +
                                             int(round(self.secondarySquare.zlength / 2)) - 1)
                        if self.secondarySquare.zlength % 2 == 0:  # if number is even
                            pymchelper.set_block(level, self.blockPattern.blockForWindow,
                                                 xCoord, height, self.secondarySquare.zorigin +
                                                 int(round(self.secondarySquare.zlength / 2)))

            else:
                if direction == self.Direction.zpositive:
                    zCoord = self.secondarySquare.zorigin
                else:
                    zCoord = self.secondarySquare.zorigin + self.secondarySquare.zlength - 1
                for iteration in range(self.secondarySquare.xorigin + 1,
                                       self.secondarySquare.xorigin + self.secondarySquare.xlength - 1):
                    # builds floor-level wall-blocks
                    pymchelper.set_block(level, self.blockPattern.blockAtFloorLevel, iteration,
                                         yHeightIncreaseSecondary + self.buildLayer + self.foundationHeight + 1 +
                                         self.firstFloorHeight, zCoord)
                # builds default blocks
                for height in range(0, int(round(self.secondarySquare.xlength / 2)) - 2):
                    for edgeWallBlock in range(self.secondarySquare.xorigin + height + 2,
                                               self.secondarySquare.xorigin + self.secondarySquare.xlength -
                                               height - 2):
                        pymchelper.set_block(level, self.blockPattern.blockDefault, edgeWallBlock,
                                             yHeightIncreaseSecondary + height + self.buildLayer +
                                             self.foundationHeight + 1 + self.firstFloorHeight + 1, zCoord)

                # builds windows
                if random.randint(0, chanceToBuildWindow) == 0:
                    for height in range(self.buildLayer + self.foundationHeight + 1 + self.firstFloorHeight + 2 +
                                        yHeightIncreaseSecondary, self.buildLayer + self.foundationHeight + 1 +
                                        self.firstFloorHeight + 2 + yHeightIncreaseSecondary +
                                        int(round(self.secondarySquare.xlength / 2)) - 4):
                        pymchelper.set_block(level, self.blockPattern.blockForWindow, self.secondarySquare.xorigin +
                                             int(round(self.secondarySquare.xlength / 2)) - 1, height, zCoord)
                        if self.secondarySquare.xlength % 2 == 0:  # if number is even
                            pymchelper.set_block(level, self.blockPattern.blockForWindow, self.secondarySquare.xorigin +
                                                 int(round(self.secondarySquare.xlength / 2)), height, zCoord)

    # builds floors
    def buildFloors(self, level, box):
        for y in range(self.buildLayer, self.buildLayer + self.foundationHeight + 1):
            for x in range(0, self.floorPlanArray.shape[0]):
                for z in range(0, self.floorPlanArray.shape[1]):
                    if self.floorPlanArray[x, z] == 1 or self.floorPlanArray[x, z] == 3:
                        pymchelper.set_block(level, self.blockPattern.foundationBlock, box.minx + x, y, box.minz + z)
                    elif self.floorPlanArray[x, z] == 2 or self.floorPlanArray[x, z] == 6:
                        pymchelper.set_block(level, self.blockPattern.flooring, box.minx + x, y, box.minz + z)

    # find walls
    def findOuterWalls(self, box, square, wall_list, wall_height):
        if square is not None:
            foundWalls = numpy.zeros((box.maxx - box.minx, box.maxz - box.minz), numpy.uint8)
            for x in range(0 + abs(box.minx - square.xorigin), abs(box.minx - square.xorigin) + square.xlength):
                for z in range(0 + abs(box.minz - square.zorigin), abs(box.minz - square.zorigin) + square.zlength):
                    if self.floorPlanArray[x, z] == 1 and foundWalls[x, z] == 0:
                        # determine if horizontal or vertical wall
                        if (self.floorPlanArray[x - 1, z] == 0 and ((self.floorPlanArray[x + 1, z] == 6) or
                                                                    self.floorPlanArray[x + 1, z] == 2)) or \
                                (self.floorPlanArray[x + 1, z] == 0 and ((self.floorPlanArray[x - 1, z] == 6) or
                                                                         self.floorPlanArray[x - 1, z] == 2)):
                            offset = z
                            while self.floorPlanArray[x, offset] == 1:
                                foundWalls[x, offset] = 1
                                offset = offset + 1
                            # Creating wall
                            wall = self.Wall()
                            wall.isXDirection = False
                            wall.xOrigin = box.minx + x
                            wall.zOrigin = box.minz + z
                            wall.length = offset - z
                            wall.height = wall_height
                            if self.floorPlanArray[x - 1, z] == 0:
                                wall.directionOut = self.Direction.xnegative
                            else:
                                wall.directionOut = self.Direction.xpositive
                            wall_list.append(wall)
                        elif (self.floorPlanArray[x, z - 1] == 0 and ((self.floorPlanArray[x, z + 1] == 6) or
                                                                      self.floorPlanArray[x, z + 1] == 2)) or \
                                ((self.floorPlanArray[x, z + 1] == 0) and ((self.floorPlanArray[x, z - 1] == 6) or
                                                                           self.floorPlanArray[x, z - 1] == 2)):
                            offset = x
                            while self.floorPlanArray[offset, z] == 1:
                                foundWalls[offset, z] = 1
                                offset = offset + 1
                            # Creating a wall
                            wall = self.Wall()
                            wall.isXDirection = True
                            wall.xOrigin = box.minx + x
                            wall.zOrigin = box.minz + z
                            wall.length = offset - x
                            wall.height = wall_height
                            if self.floorPlanArray[x, z - 1] == 0:
                                wall.directionOut = self.Direction.znegative
                            else:
                                wall.directionOut = self.Direction.zpositive
                            wall_list.append(wall)

    # builds walls
    def buildWalls(self, level, box, is_first_floor, build_entrance, wall_list, square, front_direction):
        # covers certain wall area with wooden planks to makeup for gap created by door on first floor.
        if not is_first_floor:
            for x in range(0, square.xlength):
                for z in range(0, square.zlength):
                    if x == 0 or x == square.xlength or z == 0 or z == square.zlength:
                        if self.floorPlanArray[x + abs(box.minx - square.xorigin),
                                               z + abs(box.minz - square.zorigin)] == 5:
                            for y in range(self.buildLayer + self.foundationHeight + 2 + self.firstFloorHeight,
                                           self.buildLayer + self.foundationHeight + self.firstFloorHeight + 5):
                                pymchelper.set_block(level, self.blockPattern.blockDefault, x + square.xorigin,
                                                     y, z + square.zorigin)
                            pymchelper.set_block(level, self.blockPattern.blockAtFloorLevel, x + square.xorigin,
                                                 self.buildLayer + self.foundationHeight + 1 + self.firstFloorHeight,
                                                 z + square.zorigin)

        # builds entrance
        chosenWall = None
        if build_entrance:
            # x negative
            if front_direction == 4:
                coord = sys.maxint
                for wall in wall_list:
                    if not wall.isXDirection:
                        if wall.xOrigin < coord:
                            chosenWall = wall
                            coord = wall.xOrigin
            # x positive
            elif front_direction == 5:
                coord = -sys.maxint - 1
                for wall in wall_list:
                    if not wall.isXDirection:
                        if coord < wall.xOrigin:
                            chosenWall = wall
                            coord = wall.xOrigin
            # z negative
            elif front_direction == 2:
                coord = sys.maxint
                for wall in wall_list:
                    if wall.isXDirection:
                        if wall.zOrigin < coord:
                            chosenWall = wall
                            coord = wall.zOrigin
            # z positive
            elif front_direction == 3:
                coord = -sys.maxint - 1
                for wall in wall_list:
                    if wall.isXDirection:
                        if coord < wall.zOrigin:
                            chosenWall = wall
                            coord = wall.zOrigin

        # builds walls
        for wallObject in wall_list:
            if is_first_floor:
                self.buildOuterWalls(level, wallObject, self.buildLayer + self.foundationHeight + 2,
                                     self.buildLayer + self.foundationHeight + wallObject.height, True)
            else:
                self.buildOuterWalls(level, wallObject, self.buildLayer + self.foundationHeight + 2 +
                                     self.firstFloorHeight, self.buildLayer + self.foundationHeight +
                                     self.firstFloorHeight + 5, False)

        # calls method that builds entry way
        if build_entrance:
            self.buildEntrance(level, box, chosenWall)

            # splits door-wall in two, so windows can be created on both sides if room exists
            self.splitWall(chosenWall, wall_list)

        # calls method that builds windows
        for wall in wall_list:
            if is_first_floor:
                self.buildWindows(level, wall, self.buildLayer + self.foundationHeight + 2,
                                  self.buildLayer + self.foundationHeight + 2 + self.firstFloorHeight - 2)
            else:
                self.buildWindows(level, wall, self.buildLayer + self.foundationHeight + 3 + self.firstFloorHeight,
                                  self.buildLayer + self.foundationHeight + 3 + self.firstFloorHeight + 1)

        # builds corners
        if is_first_floor:
            self.buildCornerPillars(level, box, square, 0, self.firstFloorHeight)
        else:
            self.buildCornerPillars(level, box, square, self.firstFloorHeight, self.firstFloorHeight + 4)

    # handles preparation of finding walls
    def prepareFindWalls(self, level, box, front_direction):
        # find walls
        self.findOuterWalls(box, self.mainSquare, self.wallListMainFoundation, self.firstFloorHeight)
        self.findOuterWalls(box, self.secondarySquare, self.wallListSecondFoundation, self.firstFloorHeight)
        self.wallListOuterCombined = self.wallListMainFoundation + self.wallListSecondFoundation

        self.buildWalls(level, box, True, True, self.wallListMainFoundation, self.mainSquare, front_direction)
        self.buildWalls(level, box, True, False, self.wallListSecondFoundation, self.secondarySquare, front_direction)

    # build corner pillars
    def buildCornerPillars(self, level, box, square, start_height, end_height):
        if square is not None:
            for x in range(0, square.xlength):
                for z in range(0, square.zlength):
                    if self.floorPlanArray[x + abs(box.minx - square.xorigin), z + abs(box.minz - square.zorigin)] == 3:
                        for height in range(start_height, end_height):
                            pymchelper.set_block(level, self.blockPattern.blockCorner, x + square.xorigin,
                                                 self.buildLayer + self.foundationHeight + 1 + height,
                                                 z + square.zorigin)

    # splits wall in two and creates new walls out of it. used when wall contains a door.
    def splitWall(self, wall_to_split, wall_list):
        wall = wall_to_split
        leftWall = False
        leftWallXCoords = 0
        leftWallZCoords = 0
        leftWallLength = 0
        rightWall = True
        rightWallXCoords = 0
        rightWallZCoords = 0
        rightWallLength = 0
        wallThreshold = 1
        if wall.isXDirection:
            if wall.doorXOrigin - wall.xOrigin >= wallThreshold:
                leftWall = True
                leftWallXCoords = wall.xOrigin
                leftWallZCoords = wall.zOrigin
                leftWallLength = wall.doorXOrigin - wall.xOrigin
            if wall.xOrigin + wall.length - wall.doorXOrigin - 1 >= wallThreshold:
                rightWall = True
                rightWallXCoords = wall.doorXOrigin + 1
                rightWallZCoords = wall.zOrigin
                rightWallLength = wall.xOrigin + wall.length - wall.doorXOrigin - 1
        else:
            if wall.doorZOrigin - wall.zOrigin >= wallThreshold:
                leftWall = True
                leftWallXCoords = wall.xOrigin
                leftWallZCoords = wall.zOrigin
                leftWallLength = wall.doorZOrigin - wall.zOrigin
            if wall.zOrigin + wall.length - wall.doorZOrigin - 1 >= wallThreshold:
                rightWall = True
                rightWallXCoords = wall.xOrigin
                rightWallZCoords = wall.doorZOrigin + 1
                rightWallLength = wall.zOrigin + wall.length - wall.doorZOrigin - 1
        if leftWall:
            # Creating wall
            newWall = self.Wall()
            newWall.isXDirection = wall.isXDirection
            newWall.xOrigin = leftWallXCoords
            newWall.zOrigin = leftWallZCoords
            newWall.length = leftWallLength
            newWall.height = self.firstFloorHeight
            newWall.directionOut = wall.directionOut
            wall_list.append(newWall)
        if rightWall:
            # Creating wall
            newWall = self.Wall()
            newWall.isXDirection = wall.isXDirection
            newWall.xOrigin = rightWallXCoords
            newWall.zOrigin = rightWallZCoords
            newWall.length = rightWallLength
            newWall.height = self.firstFloorHeight
            newWall.directionOut = wall.directionOut
            wall_list.append(newWall)
        if leftWall or rightWall:
            wall_list.remove(wall)

    # clears indoors space
    def clearIndoorSpace(self, level, box, floor_layer, first_floor):
        for x in range(self.floorPlanArray.shape[0]):
            for z in range(self.floorPlanArray.shape[1]):
                if self.floorPlanArray[x, z] == 2:
                    if first_floor:
                        for y in range(floor_layer, floor_layer + self.firstFloorHeight + 1):
                            pymchelper.set_block(level, pocketMaterials.Air, box.minx + x, y, box.minz + z)
                    else:
                        for y in range(floor_layer, floor_layer + 4):
                            pymchelper.set_block(level, pocketMaterials.Air, box.minx + x, y, box.minz + z)

    # builds the outer walls
    def buildOuterWalls(self, level, wall, start_height, end_height, is_first_floor):
        if wall.isXDirection:
            lengthStart = wall.xOrigin
            widthBlock = wall.zOrigin
        else:
            lengthStart = wall.zOrigin
            widthBlock = wall.xOrigin
        lengthEnd = lengthStart + wall.length
        firstCorner = lengthStart
        secondCorner = lengthStart + wall.length - 1

        for length in range(lengthStart, lengthEnd):
            if wall.isXDirection:
                x = length
                z = widthBlock
            else:
                x = widthBlock
                z = length
            for y in range(start_height, end_height):
                # blocks beside corners
                if length == firstCorner or length == secondCorner:
                    pymchelper.set_block(level, self.blockPattern.blockBesideCorner, x, y, z)
                # the rest of the blocks
                else:
                    pymchelper.set_block(level, self.blockPattern.blockDefault, x, y, z)

            # blocks above floor and under ceiling
            if is_first_floor:
                pymchelper.set_block(level, self.blockPattern.blockAboveFloorLevel, x, start_height - 1, z)
                pymchelper.set_block(level, self.blockPattern.blockUnderCeilingLevel, x, end_height, z)
            else:
                pymchelper.set_block(level, self.blockPattern.blockAboveFloorLevel, x, start_height, z)
                pymchelper.set_block(level, self.blockPattern.blockUnderCeilingLevel, x, end_height - 1, z)

    # builds windows
    def buildWindows(self, level, wall, start_height, end_height):
        if wall.isXDirection:
            firstRangeNumber = wall.xOrigin + 1
            secondRangeNumber = wall.xOrigin + wall.length - 1
        else:
            firstRangeNumber = wall.zOrigin + 1
            secondRangeNumber = wall.zOrigin + wall.length - 1

        placedInRow = 0
        placeWindow = True
        for coord in range(firstRangeNumber, secondRangeNumber):
            number = random.randint(placedInRow, 4)
            if number == 4:
                placeWindow = False
            if placeWindow:
                for y in range(start_height, end_height):
                    if wall.isXDirection:
                        pymchelper.set_block(level, self.blockPattern.blockForWindow, coord, y, wall.zOrigin)
                    else:
                        pymchelper.set_block(level, self.blockPattern.blockForWindow, wall.xOrigin, y, coord)
                if wall.isXDirection:
                    pymchelper.set_block(level, self.blockPattern.blockUnderWindow, coord, start_height - 1,
                                         wall.zOrigin)
                    pymchelper.set_block(level, self.blockPattern.blockAboveWindow, coord, end_height,
                                         wall.zOrigin)
                else:
                    pymchelper.set_block(level, self.blockPattern.blockUnderWindow, wall.xOrigin, start_height - 1,
                                         coord)
                    pymchelper.set_block(level, self.blockPattern.blockAboveWindow, wall.xOrigin, end_height,
                                         coord)
                placedInRow = placedInRow + 1
            if not placeWindow:
                placeWindow = True
                placedInRow = 0

    # builds outer door
    def buildEntrance(self, level, box, wall):
        # if wall is just 2 blocks wide
        if wall.length == 1:
            wall.doorXOrigin = wall.xOrigin
            wall.doorZOrigin = wall.zOrigin

        # if wall is just 3 blocks wide
        elif wall.length == 2:
            if wall.isXDirection:
                wall.doorXOrigin = wall.xOrigin + 1
                wall.doorZOrigin = wall.zOrigin
            else:
                wall.doorXOrigin = wall.xOrigin
                wall.doorZOrigin = wall.zOrigin + 1

        # if wall is longer
        else:
            # door shall be in center
            if random.randint(0, 2) == 0:
                if wall.isXDirection:
                    wall.doorZOrigin = wall.zOrigin
                    wall.doorXOrigin = wall.xOrigin + int(wall.length / 2)
                else:
                    wall.doorXOrigin = wall.xOrigin
                    wall.doorZOrigin = wall.zOrigin + int(wall.length / 2)
            # door position on wall is random
            else:
                if wall.isXDirection:
                    wall.doorZOrigin = wall.zOrigin
                    wall.doorXOrigin = random.randint(wall.xOrigin + 1, wall.xOrigin + wall.length - 1)
                else:
                    wall.doorXOrigin = wall.xOrigin
                    wall.doorZOrigin = random.randint(wall.zOrigin + 1, wall.zOrigin + wall.length - 1)

        # setting door location in array to 5
        self.floorPlanArray[abs(box.minx - wall.doorXOrigin), abs(box.minz - wall.doorZOrigin)] = 5

        # makes sure furniture can't be placed in front of entry door
        if self.floorPlanArray[abs(box.minx - wall.doorXOrigin) - 1, abs(box.minz - wall.doorZOrigin)] == 6:
            self.floorPlanArray[abs(box.minx - wall.doorXOrigin) - 1, abs(box.minz - wall.doorZOrigin)] = 2
        if self.floorPlanArray[abs(box.minx - wall.doorXOrigin) + 1, abs(box.minz - wall.doorZOrigin)] == 6:
            self.floorPlanArray[abs(box.minx - wall.doorXOrigin) + 1, abs(box.minz - wall.doorZOrigin)] = 2
        if self.floorPlanArray[abs(box.minx - wall.doorXOrigin), abs(box.minz - wall.doorZOrigin) - 1] == 6:
            self.floorPlanArray[abs(box.minx - wall.doorXOrigin), abs(box.minz - wall.doorZOrigin) - 1] = 2
        if self.floorPlanArray[abs(box.minx - wall.doorXOrigin), abs(box.minz - wall.doorZOrigin) + 1] == 6:
            self.floorPlanArray[abs(box.minx - wall.doorXOrigin), abs(box.minz - wall.doorZOrigin) + 1] = 2
        if self.floorPlanArray[abs(box.minx - wall.doorXOrigin) - 1, abs(box.minz - wall.doorZOrigin) - 1] == 6:
            self.floorPlanArray[abs(box.minx - wall.doorXOrigin) - 1, abs(box.minz - wall.doorZOrigin) - 1] = 2
        if self.floorPlanArray[abs(box.minx - wall.doorXOrigin) - 1, abs(box.minz - wall.doorZOrigin) + 1] == 6:
            self.floorPlanArray[abs(box.minx - wall.doorXOrigin) - 1, abs(box.minz - wall.doorZOrigin) + 1] = 2
        if self.floorPlanArray[abs(box.minx - wall.doorXOrigin) + 1, abs(box.minz - wall.doorZOrigin) - 1] == 6:
            self.floorPlanArray[abs(box.minx - wall.doorXOrigin) + 1, abs(box.minz - wall.doorZOrigin) - 1] = 2
        if self.floorPlanArray[abs(box.minx - wall.doorXOrigin) + 1, abs(box.minz - wall.doorZOrigin) + 1] == 6:
            self.floorPlanArray[abs(box.minx - wall.doorXOrigin) + 1, abs(box.minz - wall.doorZOrigin) + 1] = 2

        # building entry
        pymchelper.set_block(level, self.blockPattern.blockAboveDoor, wall.doorXOrigin,
                             self.buildLayer + self.foundationHeight + 3, wall.doorZOrigin)
        if wall.isXDirection:
            wallOrigin = wall.doorXOrigin
        else:
            wallOrigin = wall.doorZOrigin

        for length in range(wallOrigin - 1, wallOrigin + 2):
            if wall.isXDirection:
                x = length
                z = wall.doorZOrigin
            else:
                x = wall.doorXOrigin
                z = length
            for y in range(self.buildLayer + self.foundationHeight + 1,
                           self.buildLayer + self.foundationHeight + 3):
                if length == wallOrigin:
                    pymchelper.set_block(level, pocketMaterials.Air, x, y, z)
                elif length == wallOrigin - 1 or length == wallOrigin + 1:
                    pymchelper.set_block(level, self.blockPattern.blockBesideDoor, x, y, z)

        # calling method to build entrance stairs
        self.buildEntranceStairs(level, box, wall.doorXOrigin, wall.doorZOrigin)

    # builds entrance stairs
    def buildEntranceStairs(self, level, box, coord_x, coord_z):
        coord_y = self.buildLayer + self.foundationHeight
        if self.floorPlanArray[abs(box.minx - coord_x) - 1, abs(box.minz - coord_z)] == 0:
            pymchelper.set_block(level, pocketMaterials[67, 0], coord_x - 1, coord_y, coord_z)
            self.roadCoordinate = Coord(coord_x - 1, coord_z)
        elif self.floorPlanArray[abs(box.minx - coord_x) + 1, abs(box.minz - coord_z)] == 0:
            pymchelper.set_block(level, pocketMaterials[67, 1], coord_x + 1, coord_y, coord_z)
            self.roadCoordinate = Coord(coord_x + 1, coord_z)
        elif self.floorPlanArray[abs(box.minx - coord_x), abs(box.minz - coord_z) - 1] == 0:
            pymchelper.set_block(level, pocketMaterials[67, 2], coord_x, coord_y, coord_z - 1)
            self.roadCoordinate = Coord(coord_x, coord_z - 1)
        elif self.floorPlanArray[abs(box.minx - coord_x), abs(box.minz - coord_z) + 1] == 0:
            pymchelper.set_block(level, pocketMaterials[67, 3], coord_x, coord_y, coord_z + 1)
            self.roadCoordinate = Coord(coord_x, coord_z + 1)
        if self.foundationHeight == 1:
            if self.floorPlanArray[abs(box.minx - coord_x) - 1, abs(box.minz - coord_z)] == 0:
                pymchelper.set_block(level, pocketMaterials[67, 0], coord_x - 2, coord_y - 1, coord_z)
                pymchelper.set_block(level, pocketMaterials.Cobblestone, coord_x - 1, coord_y - 1, coord_z)
                self.roadCoordinate = Coord(coord_x - 2, coord_z)
            elif self.floorPlanArray[abs(box.minx - coord_x) + 1, abs(box.minz - coord_z)] == 0:
                pymchelper.set_block(level, pocketMaterials[67, 1], coord_x + 2, coord_y - 1, coord_z)
                pymchelper.set_block(level, pocketMaterials.Cobblestone, coord_x + 1, coord_y - 1, coord_z)
                self.roadCoordinate = Coord(coord_x + 2, coord_z)
            elif self.floorPlanArray[abs(box.minx - coord_x), abs(box.minz - coord_z) - 1] == 0:
                pymchelper.set_block(level, pocketMaterials[67, 2], coord_x, coord_y - 1, coord_z - 2)
                pymchelper.set_block(level, pocketMaterials.Cobblestone, coord_x, coord_y - 1, coord_z - 1)
                self.roadCoordinate = Coord(coord_x, coord_z - 2)
            elif self.floorPlanArray[abs(box.minx - coord_x), abs(box.minz - coord_z) + 1] == 0:
                pymchelper.set_block(level, pocketMaterials[67, 3], coord_x, coord_y - 1, coord_z + 2)
                pymchelper.set_block(level, pocketMaterials.Cobblestone, coord_x, coord_y - 1, coord_z + 1)
                self.roadCoordinate = Coord(coord_x, coord_z + 2)

    # Creates foundations for the house
    def createHouseFoundations(self, box, two_squares, two_floors):
        self.createMainSquare(box, two_squares, two_floors)
        if two_squares:
            self.secondarySquare = self.createSecondSquare(box)

        # decide how tall the floor is going to be
        if self.mainSquare.xlength < self.mainSquare.zlength:
            shortestLength = self.mainSquare.xlength
        else:
            shortestLength = self.mainSquare.zlength

        # ceiling shouldn't be taller than the width of the shortest side of mainFoundation. would look strange
        if shortestLength <= 5:
            self.firstFloorHeight = random.randint(3, shortestLength)
        else:
            self.firstFloorHeight = random.randint(4, 5)

        # RNG to create heightened foundations or not
        self.foundationHeight = random.randint(0, 1)

    # Creates main foundation for the house
    def createMainSquare(self, box, two_squares, two_floors):
        xMaxLengthMultiplier = 0
        zMaxLengthMultiplier = 0
        xMinLengthMultiplier = 0
        zMinLengthMultiplier = 0
        if two_squares:
            if box.maxx - box.minx > box.maxz - box.minz:
                if two_squares:
                    xMaxLengthMultiplier = 0.9
                    zMaxLengthMultiplier = 0.6
                    xMinLengthMultiplier = 0.65
                    zMinLengthMultiplier = 0.5
            else:
                if two_squares:
                    xMaxLengthMultiplier = 0.6
                    zMaxLengthMultiplier = 0.9
                    xMinLengthMultiplier = 0.5
                    zMinLengthMultiplier = 0.65
        else:
            xMaxLengthMultiplier = 0.9
            zMaxLengthMultiplier = 0.9
            xMinLengthMultiplier = 0.65
            zMinLengthMultiplier = 0.65

        self.mainSquare = self.FoundationSquare()
        if (box.maxx - box.minx - 2) * 0.5 >= 5:
            self.mainSquare.xlength = random.randint(int(round((box.maxx - box.minx - 2) * xMinLengthMultiplier)),
                                                     int(round((box.maxx - box.minx - 2) * xMaxLengthMultiplier)))
        else:
            self.mainSquare.xlength = random.randint(5, box.maxx - box.minx - 2)
        if (box.maxz - box.minz - 2) * 0.5 >= 5:
            self.mainSquare.zlength = random.randint(int(round((box.maxz - box.minz - 2) * zMinLengthMultiplier)),
                                                     int(round((box.maxz - box.minz - 2) * zMaxLengthMultiplier)))
        else:
            self.mainSquare.zlength = random.randint(5, box.maxz - box.minz - 2)

        # determines how many floors it shall have.
        if two_floors:
            self.mainSquare.floors = 2
        else:
            self.mainSquare.floors = 1

        # place square by selecting random origin point. (min x and z values of one of the corners become origin)
        self.mainSquare.xorigin = random.randint(box.minx + 1, box.maxx - 1 - self.mainSquare.xlength)
        self.mainSquare.zorigin = random.randint(box.minz + 1, box.maxz - 1 - self.mainSquare.zlength)

        # determine what axis the length of the foundation goes through
        if self.mainSquare.xlength > self.mainSquare.zlength:
            self.mainSquare.xDirection = True
        else:
            self.mainSquare.xDirection = False

    # Creates second foundation for house
    def createSecondSquare(self, box):
        secondSquare = self.FoundationSquare()

        # determines how many floors it shall have.
        if self.mainSquare.floors == 2 and random.getrandbits(1):
            secondSquare.floors = 2
        else:
            secondSquare.floors = 1

        if self.mainSquare.xlength < self.mainSquare.zlength:
            shortestLength = self.mainSquare.xlength
        else:
            shortestLength = self.mainSquare.zlength

        secondSquare.xlength = random.randint(5, shortestLength)
        secondSquare.zlength = random.randint(5, shortestLength)
        secondSquare.xorigin = random.randint(box.minx + 1, box.maxx - 1 - secondSquare.xlength)
        secondSquare.zorigin = random.randint(box.minz + 1, box.maxz - 1 - secondSquare.zlength)
        secondSquare.xDirection = not self.mainSquare.xDirection

        # calls method that expands main square to fit in a way that makes sense
        self.expandMainSquare(secondSquare)
        secondSquare = self.makeSureSquareStickOut(box, secondSquare)
        if secondSquare is None:
            return None

        # calls method that trims secondary square to fit in a way that makes sense
        secondSquare = self.trimSecondarySquare(secondSquare)

        return secondSquare

    # makes sure enough of the secondary square sticks ou
    def makeSureSquareStickOut(self, box, square):
        stickOutBlocks = 4  # determines how many blocks square must stick out
        directionTowardsMainSquare = self.getBasedSecondDirection(square)
        if directionTowardsMainSquare == self.Direction.xpositive:
            while square.xorigin > self.mainSquare.xorigin - stickOutBlocks and \
                    checkRectIntersect(square, self.mainSquare):
                if square.xorigin - 1 >= box.minx + 1:
                    square.xorigin = square.xorigin - 1
                elif self.mainSquare.xorigin + self.mainSquare.xlength + 1 <= box.maxx - 1:
                    self.mainSquare.xorigin = self.mainSquare.xorigin + 1
                else:
                    return None
        elif directionTowardsMainSquare == self.Direction.xnegative:
            while square.xorigin + square.xlength < self.mainSquare.xorigin + self.mainSquare.xlength + \
                    stickOutBlocks and checkRectIntersect(square, self.mainSquare):
                if square.xorigin + square.xlength + 1 <= box.maxx - 1:
                    square.xorigin = square.xorigin + 1
                elif self.mainSquare.xorigin - 1 >= box.minx + 1:
                    self.mainSquare.xorigin = self.mainSquare.xorigin - 1
                else:
                    return None
        elif directionTowardsMainSquare == self.Direction.zpositive:
            while square.zorigin > self.mainSquare.zorigin - stickOutBlocks and \
                    checkRectIntersect(square, self.mainSquare):
                if square.zorigin + square.zlength + 1 <= box.maxz - 1:
                    square.zorigin = square.zorigin + 1
                elif self.mainSquare.zorigin - 1 >= box.minz + 1:
                    self.mainSquare.zorigin = self.mainSquare.zorigin - 1
                else:
                    return None
        elif directionTowardsMainSquare == self.Direction.znegative:
            while square.zorigin + square.zlength < self.mainSquare.zorigin + self.mainSquare.zlength + \
                    stickOutBlocks and checkRectIntersect(square, self.mainSquare):
                if square.zorigin + square.zlength + 1 <= box.maxz - 1:
                    square.zorigin = square.zorigin + 1
                elif self.mainSquare.zorigin - 1 >= box.minz + 1:
                    self.mainSquare.zorigin = self.mainSquare.zorigin - 1
                else:
                    return None
        return square

    # gets direction used to move foundation-square based on shape of main foundation
    def getBasedMainDirection(self, other_square):
        if self.mainSquare.xDirection:
            if (other_square.xorigin + other_square.xlength) / 2 >= \
                    (self.mainSquare.xorigin + self.mainSquare.xlength) / 2:
                return self.Direction.xnegative
            return self.Direction.xpositive
        else:
            if (other_square.zorigin + other_square.zlength) / 2 >= \
                    (self.mainSquare.zorigin + self.mainSquare.zlength) / 2:
                return self.Direction.znegative
            return self.Direction.zpositive

    # gets direction used to move foundation-square based on shape of main foundation
    def getBasedSecondDirection(self, other_square):
        if self.mainSquare.xDirection:
            if (other_square.zorigin + other_square.zlength) / 2 >= \
                    (self.mainSquare.zorigin + self.mainSquare.zlength) / 2:
                return self.Direction.znegative
            return self.Direction.zpositive
        else:
            if (other_square.xorigin + other_square.xlength) / 2 >= \
                    (self.mainSquare.xorigin + self.mainSquare.xlength) / 2:
                return self.Direction.xnegative
            return self.Direction.xpositive

    # expands main square if necessary to make a more good-looking shape
    def expandMainSquare(self, second_square):
        # direction along length of main square
        mainDirection = self.getBasedMainDirection(second_square)

        # if main foundation length-axis thing is in X-axis
        if self.mainSquare.xDirection:
            if mainDirection == self.Direction.xpositive:
                if second_square.xorigin < self.mainSquare.xorigin:
                    distanceToMoveMainInX = self.mainSquare.xorigin - second_square.xorigin
                    self.mainSquare.xorigin = second_square.xorigin
                    self.mainSquare.xlength = self.mainSquare.xlength + distanceToMoveMainInX
            elif mainDirection == self.Direction.xnegative:
                if second_square.xorigin + second_square.xlength > \
                        self.mainSquare.xorigin + self.mainSquare.xlength:
                    distanceToMoveMainInX = second_square.xorigin + second_square.xlength - \
                                            self.mainSquare.xorigin - self.mainSquare.xlength
                    self.mainSquare.xlength = self.mainSquare.xlength + distanceToMoveMainInX
        elif not self.mainSquare.xDirection:
            if mainDirection == self.Direction.zpositive:
                if second_square.zorigin < self.mainSquare.zorigin:
                    distanceToMoveMainInZ = self.mainSquare.zorigin - second_square.zorigin
                    self.mainSquare.zorigin = second_square.zorigin
                    self.mainSquare.zlength = self.mainSquare.zlength + distanceToMoveMainInZ
            elif mainDirection == self.Direction.znegative:
                if second_square.zorigin + second_square.zlength > \
                        self.mainSquare.zorigin + self.mainSquare.zlength:
                    distanceToMoveMainInZ = second_square.zorigin + second_square.zlength - \
                                            self.mainSquare.zorigin - self.mainSquare.zlength
                    self.mainSquare.zlength = self.mainSquare.zlength + distanceToMoveMainInZ

    # trims off secondary square if intersecting with main square
    def trimSecondarySquare(self, second_square):
        # direction inwards along the short side of main square
        secondDirection = self.getBasedSecondDirection(second_square)
        # if main foundation length-axis thing is in X-axis
        if self.mainSquare.xDirection:
            if secondDirection == self.Direction.znegative:
                distanceToMoveSecondSquare = second_square.zorigin - self.mainSquare.zorigin - self.mainSquare.zlength
                second_square.zorigin = self.mainSquare.zorigin + self.mainSquare.zlength
                second_square.zlength = second_square.zlength + distanceToMoveSecondSquare
            elif secondDirection == self.Direction.zpositive:
                distanceToMoveSecondSquare = second_square.zorigin + second_square.zlength - self.mainSquare.zorigin
                second_square.zlength = second_square.zlength - distanceToMoveSecondSquare
        # if main foundation length-axis thing is in Z-axis
        else:
            if secondDirection == self.Direction.xnegative:
                distanceToMoveSecondSquare = second_square.xorigin - self.mainSquare.xorigin - self.mainSquare.xlength
                second_square.xorigin = self.mainSquare.xorigin + self.mainSquare.xlength
                second_square.xlength = second_square.xlength + distanceToMoveSecondSquare
            elif secondDirection == self.Direction.xpositive:
                distanceToMoveSecondSquare = second_square.xorigin + second_square.xlength - self.mainSquare.xorigin
                second_square.xlength = second_square.xlength - distanceToMoveSecondSquare

        return second_square

    # Rectangle representing the foundation-parts of the house
    class FoundationSquare(object):
        def __init__(self):
            self.xorigin = 0
            self.zorigin = 0
            self.xlength = 0
            self.zlength = 0
            self.floors = 1
            self.xDirection = None

    # Enum representing direction
    class Direction(Enum):
        xpositive = 0
        xnegative = 1
        zpositive = 2
        znegative = 3

    # room class becoming a room inside building
    class Room(object):
        def __init__(self):
            self.xOrigin = 0
            self.zOrigin = 0
            self.xLength = 0
            self.zLength = 0
            self.roomType = None
            self.roomArea = 0
            self.furnitureList = []

    # wall class becoming a wall
    class Wall(object):
        def __init__(self):
            self.isXDirection = False
            self.length = 0
            self.height = 0
            self.xOrigin = 0
            self.zOrigin = 0
            self.doorXOrigin = 0
            self.doorZorigin = 0
            self.directionOut = None

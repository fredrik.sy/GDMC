import numpy

from worldmap import Material


class Rule(object):
    def __init__(self, conditions, duplicate=False):
        """
        :type conditions: list
        :type duplicate: bool
        """
        self._duplicate = duplicate
        self._key = numpy.zeros(len(conditions), dtype=object)
        self._value = numpy.zeros(len(conditions), dtype=object)

        for i in range(len(conditions)):
            if isinstance(conditions[i], basestring):
                self._key[i] = conditions[i]
                self._value[i] = None
            elif isinstance(conditions[i], tuple):
                self._key[i] = conditions[i][0]
                self._value[i] = conditions[i][1]
            else:
                raise ValueError

    def interpret(self, graph, height, child_id, parent_id=None):
        """
        :type graph: Graph
        :type height: int
        :type child_id: int
        :type parent_id: int
        """
        choice_key = set()
        choice_id = set()

        if parent_id is None:
            if not self._duplicate and len(graph.nodes[child_id].adjacent) != len(self._key):
                return False

            for adjacent_id in graph.nodes[child_id].adjacent:
                adjacent_symbol = graph.get_symbol(adjacent_id, height)

                if adjacent_symbol not in self._key:
                    return False

                for i in numpy.argwhere(self._key == adjacent_symbol):
                    i = int(i)

                    if self._value[i] is None or self._value[i].interpret(graph, height, adjacent_id, child_id):
                        choice_key.add(i)
                        choice_id.add(adjacent_id)

            return len(choice_key) == len(self._key) and len(choice_id) >= len(self._key)
        else:
            if not self._duplicate and len(graph.nodes[child_id].adjacent) != len(self._key):
                return False

            for adjacent_id in graph.nodes[child_id].adjacent:
                if adjacent_id != parent_id:
                    adjacent_symbol = graph.get_symbol(adjacent_id, height)

                    if adjacent_symbol not in self._key:
                        return False

                    for i in numpy.argwhere(self._key == adjacent_symbol):
                        i = int(i)

                        if self._value[i] is None:
                            return True

                        if self._value[i].interpret(graph, height, adjacent_id, child_id):
                            choice_key.add(i)
                            choice_id.add(adjacent_id)

            return len(choice_key) == len(self._key) and len(choice_id) >= len(self._key)


def run(graph):
    """
    :type graph: Graph
    """
    changed = True

    while changed:
        stack = set(graph.nodes.iterkeys())
        changed = False

        while stack:
            node_id = stack.pop()

            if node_id in graph.nodes:
                height = graph.nodes[node_id].height
                symbol = graph.get_symbol(node_id, height)

                if symbol in rules:
                    for (rule, offset) in rules[symbol].iteritems():
                        if rule.interpret(graph, height, node_id):
                            graph.set_height(node_id, height + offset)
                            changed = True
                            break


symbols = {
    Material.Empty: 'N',
    Material.Ground: 'G',
    Material.Lava: 'L',
    Material.Tree: 'T',
    Material.Water: 'W'
}

rules = {
    'GE0': {
        Rule(['G1']): 1,
        Rule(['GE1']): 1,
        Rule(['G-1', 'W-1']): -1,
        Rule(['G-1' 'GE-2']): -1,
        Rule(['G-1']): -1,
        Rule(['G-1', 'W-2', 'GE-2'], duplicate=True): -1,
        Rule(['G1', 'W0', 'G2']): 1,
        Rule(['G1', 'W0']): 1,
        Rule(['G-1', 'W-1', 'GE-1']): -1,
        Rule(['GE-1', 'GE-1', 'GE-2', 'W-2']): -1,
        Rule(['G5', 'G1']): 1,
        Rule(['G5', 'GE1', 'GE1']): 1,
        Rule(['G4', 'GE1', 'GE1']): 1,
        Rule(['G4', 'G3', 'GE1']): 1,
        Rule(['G1', 'G4', 'G5']): 1,
        Rule(['G5', 'GE1', 'GE3', 'G6', 'GE4'], duplicate=True): 1,
        Rule(['G4', 'GE3', 'GE2', 'G5']): 2,
        Rule(['G6', 'GE2']): 2,
        Rule(['GE1', 'G4'], duplicate=True): 1,
        Rule(['G1', 'G2'], duplicate=True): 1,
        Rule(['G4', 'GE5', 'G1']): 1,
        Rule(['G3']): 3,
        Rule(['G-4', 'GE-1', 'G-2', 'G-1', 'GE-2']): -1,
    },
    'G0': {
        Rule(['GE1', 'GE2', 'G2'], duplicate=True): 1,
        Rule(['GE1', 'GE2', 'G1', 'G2'], duplicate=True): 1,
        Rule(['GE1', 'G1', 'G2'], duplicate=True): 1,
        Rule(['G1']): 1,
        Rule(['G-1']): -1,
        Rule(['G-1', 'GE-1', 'G-2'], duplicate=True): -1,
        Rule(['W-2', 'G-1']): -1,
        Rule(['G-2', 'G-1', 'WE-4'], duplicate=True): -1,
        Rule(['G4', 'GE1', 'GE4']): 1,
        Rule(['G2', 'G3', 'GE3']): 2,
        Rule(['G4', 'GE1', 'G3']): 1,
        Rule(['G3', 'G2']): 2,
        Rule(['G2', 'G3', 'GE1', 'GE1']): 1,
        Rule(['G1', 'G2']): 1,
        Rule(['GE1', 'G4']): 1,
        Rule(['GE1', 'G2', 'G3']): 1,
        Rule(['GE1', 'G4', 'G5']): 1,
        Rule(['G3', 'G4']): 3,
        Rule(['GE1', 'G1', 'GE2', 'GE3', 'G3', 'G4'], duplicate=True): 1,
        Rule(['G4', 'G1', 'GE2', 'G5']): 1,
        Rule(['G3', 'G2', 'GE1', 'GE2', 'G4'], duplicate=True): 1,
        Rule(['G1', 'G2', 'GE1', 'G3'], duplicate=True): 1,
        Rule(['GE1', 'G2', 'GE2', 'G3'], duplicate=True): 1,
        Rule(['GE1', 'G1', 'G2', 'G3', 'GE3', 'G4'], duplicate=True): 1,
        Rule(['G1', 'G2'], duplicate=True): 1,
        Rule(['G3', 'GE2', 'GE1'], duplicate=True): 1,
        Rule(['G2', 'GE1'], duplicate=True): 1,
        Rule(['WE-1', 'G1']): 1,
        Rule(['G1', 'G2', 'WE-2']): 1,
        Rule(['G1', 'GE1', 'G2', 'GE2', 'GE3', 'GE4', 'GE5', 'G6'], duplicate=True): 1,
        Rule(['G-1', 'GE-2', 'GE-1', ], duplicate=True): -1,
        Rule(['G-1', 'GE-1', 'G-4', 'GE-3', 'G-3', 'GE-2'], duplicate=True): -1,
    }
}

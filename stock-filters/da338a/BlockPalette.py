import random


# class describing block "palette"
class BlockPalette(object):
    def __init__(self, block_roof, block_beside_corner, block_at_floor_level, block_above_floor_level,
                 block_under_ceiling_level, block_beside_door, block_above_door, block_under_window, block_above_window,
                 block_for_window, block_default, block_door_bottom_x, block_door_top_x, block_door_bottom_z,
                 block_door_top_z, block_flooring, block_foundation, block_corner):
        self._flooring = block_flooring
        self._foundationBlock = block_foundation
        self._blockCorner = block_corner
        self._blockRoof = block_roof
        self._blockBesideCorner = block_beside_corner
        self._blockAtFloorLevel = block_at_floor_level
        self._blockAboveFloorLevel = block_above_floor_level
        self._blockUnderCeilingLevel = block_under_ceiling_level
        self._blockBesideDoor = block_beside_door
        self._blockAboveDoor = block_above_door
        self._blockUnderWindow = block_under_window
        self._blockAboveWindow = block_above_window
        self._blockForWindow = block_for_window
        self._blockDefault = block_default
        self._blockDoorBottomX = block_door_bottom_x
        self._blockDoorTopX = block_door_top_x
        self._blockDoorBottomZ = block_door_bottom_z
        self._blockDoorTopZ = block_door_top_z

    @property
    def flooring(self):
        if len(self._flooring) - 1 > 0:
            index = random.randint(0, len(self._flooring) - 1)
            return self._flooring[index]
        return self._flooring[0]

    @property
    def foundationBlock(self):
        if len(self._foundationBlock) - 1 > 0:
            index = random.randint(0, len(self._foundationBlock) - 1)
            return self._foundationBlock[index]
        return self._foundationBlock[0]

    @property
    def blockCorner(self):
        if len(self._blockCorner) - 1 > 0:
            index = random.randint(0, len(self._blockCorner) - 1)
            return self._blockCorner[index]
        return self._blockCorner[0]

    @property
    def blockRoof(self):
        if len(self._blockRoof) - 1 > 0:
            index = random.randint(0, len(self._blockRoof) - 1)
            return self._blockRoof[index]
        return self._blockRoof[0]

    @property
    def blockBesideCorner(self):
        if len(self._blockBesideCorner) - 1 > 0:
            index = random.randint(0, len(self._blockBesideCorner) - 1)
            return self._blockBesideCorner[index]
        return self._blockBesideCorner[0]

    @property
    def blockAtFloorLevel(self):
        if len(self._blockAtFloorLevel) - 1 > 0:
            index = random.randint(0, len(self._blockAtFloorLevel) - 1)
            return self._blockAtFloorLevel[index]
        return self._blockAtFloorLevel[0]

    @property
    def blockAboveFloorLevel(self):
        if len(self._blockAboveFloorLevel) - 1 > 0:
            index = random.randint(0, len(self._blockAboveFloorLevel) - 1)
            return self._blockAboveFloorLevel[index]
        return self._blockAboveFloorLevel[0]

    @property
    def blockUnderCeilingLevel(self):
        if len(self._blockUnderCeilingLevel) - 1 > 0:
            index = random.randint(0, len(self._blockUnderCeilingLevel) - 1)
            return self._blockUnderCeilingLevel[index]
        return self._blockUnderCeilingLevel[0]

    @property
    def blockBesideDoor(self):
        if len(self._blockBesideDoor) - 1 > 0:
            index = random.randint(0, len(self._blockBesideDoor) - 1)
            return self._blockBesideDoor[index]
        return self._blockBesideDoor[0]

    @property
    def blockAboveDoor(self):
        if len(self._blockAboveDoor) - 1 > 0:
            index = random.randint(0, len(self._blockAboveDoor) - 1)
            return self._blockAboveDoor[index]
        return self._blockAboveDoor[0]

    @property
    def blockUnderWindow(self):
        if len(self._blockUnderWindow) - 1 > 0:
            index = random.randint(0, len(self._blockUnderWindow) - 1)
            return self._blockUnderWindow[index]
        return self._blockUnderWindow[0]

    @property
    def blockAboveWindow(self):
        if len(self._blockAboveWindow) - 1 > 0:
            index = random.randint(0, len(self._blockAboveWindow) - 1)
            return self._blockAboveWindow[index]
        return self._blockAboveWindow[0]

    @property
    def blockForWindow(self):
        if len(self._blockForWindow) - 1 > 0:
            index = random.randint(0, len(self._blockForWindow) - 1)
            return self._blockForWindow[index]
        return self._blockForWindow[0]

    @property
    def blockDefault(self):
        if len(self._blockDefault) - 1 > 0:
            index = random.randint(0, len(self._blockDefault) - 1)
            return self._blockDefault[index]
        return self._blockDefault[0]

    @property
    def blockDoorBottomX(self):
        return self._blockDoorBottomX[0]

    @property
    def blockDoorTopX(self):
        return self._blockDoorTopX[0]

    @property
    def blockDoorBottomZ(self):
        return self._blockDoorBottomZ[0]

    @property
    def blockDoorTopZ(self):
        return self._blockDoorTopZ[0]

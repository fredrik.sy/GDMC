import heapq
import sys

from worldmap import WorldMap


class AStar(object):
    def __init__(self, worldmap):
        """
        :type worldmap: WorldMap
        """
        self._worldmap = worldmap

    def run(self, start, goals):
        openset = []
        camefrom = {}
        gscore = {start: 0}
        fscore = {start: self.manhattan_distance(start, goals)}
        heapq.heappush(openset, (fscore[start], start))

        while openset:
            (x, z) = heapq.heappop(openset)[1]

            if (x, z) in goals:
                return self._reconstruct_path(camefrom, (x, z))

            y = self._worldmap.heightmap[z, x]

            for (xc, zc) in ((x - 1, z), (x + 1, z), (x, z - 1), (x, z + 1)):
                if 0 <= xc < self._worldmap.width and 0 <= zc < self._worldmap.length:
                    yc = self._worldmap.heightmap[zc, xc]

                    if self._worldmap.materials[z, x] == self._worldmap.materials[zc, xc] and abs(y - yc) < 2:
                        tentative_gscore = gscore[(x, z)] + self._weight((x, y, z), (xc, yc, zc))

                        if (xc, zc) not in gscore or tentative_gscore < gscore[(xc, zc)]:
                            camefrom[(xc, zc)] = (x, z)
                            gscore[(xc, zc)] = tentative_gscore
                            fscore[(xc, zc)] = gscore[(xc, zc)] + self.manhattan_distance((xc, zc), goals)

                            if not any((xc, zc) == (xv, zv) for (v_fscore, (xv, zv)) in openset):
                                heapq.heappush(openset, (fscore[(xc, zc)], (xc, zc)))

        return None

    def manhattan_distance(self, (x, z), goals):
        if isinstance(goals, list):
            min_distance = sys.maxint

            for (xc, zc) in goals:
                distance = abs(x - xc) + abs(self._worldmap.heightmap[z, x] - self._worldmap.heightmap[zc, xc]) + \
                           abs(z - zc)

                if distance < min_distance:
                    min_distance = distance

            return min_distance
        else:
            (xc, zc) = goals
            return abs(x - xc) + abs(self._worldmap.heightmap[z, x] - self._worldmap.heightmap[zc, xc]) + abs(z - zc)

    def _weight(self, (x, y, z), (xc, yc, zc)):
        if x == xc:
            if z - 1 >= 0 and z + 1 < self._worldmap.length and zc - 1 >= 0 and zc + 1 < self._worldmap.length:
                if self._worldmap.heightmap[z - 1, x] == y == self._worldmap.heightmap[z + 1, x] and \
                        self._worldmap.heightmap[zc - 1, xc] == yc == self._worldmap.heightmap[zc + 1, xc]:
                    return 1
        else:
            if x - 1 >= 0 and x + 1 < self._worldmap.width and xc - 1 >= 0 and xc + 1 < self._worldmap.width:
                if self._worldmap.heightmap[z, x - 1] == y == self._worldmap.heightmap[z, x + 1] and \
                        self._worldmap.heightmap[z, xc - 1] == yc == self._worldmap.heightmap[z, xc + 1]:
                    return 1

        return 2

    # noinspection PyMethodMayBeStatic
    def _reconstruct_path(self, came_from, current):
        total_path = [current]

        while current in came_from:
            current = came_from[current]
            total_path.append(current)

        return total_path

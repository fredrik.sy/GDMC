import numpy

from enum import IntEnum
from numpy import ndarray
from PIL import Image
from pymclevel.materials import alphaMaterials


class Material(IntEnum):
    Empty = 0
    Ground = 1
    Lava = 2
    Tree = 3
    Water = 4


class WorldMap(object):
    def __init__(self, level, box):
        """
        :type level: MCInfdevOldLevel
        :type box: BoundingBox
        """
        self._level = level
        self._box = box
        self._chunk_materials = numpy.zeros((box.length, box.width, len(Material)), dtype=int)
        self._edges = numpy.zeros((box.length, box.width), dtype=int)
        self._heightmap = numpy.zeros((box.length, box.width), dtype=int)
        self._materials = numpy.zeros((box.length, box.width), dtype=int)

        for (cx, cz) in level.allChunks:
            bounds = level.getChunk(cx, cz).bounds.intersect(box)
            chunk_material = [0] * len(Material)

            for z in range(bounds.minz, bounds.maxz):
                zc = z - box.minz

                for x in range(bounds.minx, bounds.maxx):
                    xc = x - box.minx
                    once = True

                    for y in reversed(range(bounds.miny, level.heightMapAt(x, z))):
                        blockid = level.blockAt(x, y, z)

                        if blockid in alpha_materials:
                            material = alpha_materials[blockid]
                        else:
                            material = Material.Ground

                        if material != Material.Empty:
                            if once:
                                chunk_material[material] += 1
                                once = False

                            if material == Material.Ground or material == Material.Lava or material == Material.Water:
                                self._heightmap[zc, xc] = y
                                self._materials[zc, xc] = material
                                break

            for z in range(bounds.minz, bounds.maxz):
                zc = z - box.minz

                for x in range(bounds.minx, bounds.maxx):
                    xc = x - box.minx
                    self._chunk_materials[zc, xc] = chunk_material

        for z in range(box.length - 1):
            for x in range(box.width - 1):
                y = self._heightmap[z, x]

                for (xc, zc) in ((x + 1, z), (x, z + 1)):
                    yc = self._heightmap[zc, xc]

                    if y - yc != 0:
                        self._edges[z, x] = y
                        self._edges[zc, xc] = yc

    @property
    def box(self):
        return self._box

    @property
    def chunk_materials(self):
        return self._chunk_materials

    @property
    def edges(self):
        return self._edges

    @property
    def level(self):
        return self._level

    @property
    def materials(self):
        return self._materials

    @property
    def heightmap(self):
        return self._heightmap

    @property
    def height(self):
        return self._box.height

    @property
    def length(self):
        return self._box.length

    @property
    def width(self):
        return self._box.width

    def biome(self, x, z):
        """
        :type x: int
        :type z: int
        """
        return self._level.biomeAt(x + self._box.minx, z + self._box.minz)

    def block_eq(self, (x, z), (xc, zc)):
        return self._heightmap[z, x] == self._heightmap[zc, xc] and self._materials[z, x] == self._materials[zc, xc]

    def push_block(self, x, y, z, value):
        """
        :type x: int
        :type y: int
        :type z: int
        :type value: int
        """
        assert value != 0
        xc = x + self._box.minx
        yc = y + value
        zc = z + self._box.minz
        # TODO: Remove self._edges[z, x] = 0
        self._heightmap[z, x] = yc

        if value < 0:
            self._level.setBlockAt(xc, yc, zc, self._level.blockAt(xc, y, zc))
            self._level.setBlockDataAt(xc, yc, zc, self._level.blockDataAt(xc, y, zc))

            for i in range(1, abs(value) + 2):
                self._level.setBlockAt(xc, yc + i, zc, 0)
                self._level.setBlockDataAt(xc, yc + i, zc, 0)
        else:
            for i in range(value + 1):
                self._level.setBlockAt(xc, yc - i, zc, self._level.blockAt(xc, y - i, zc))
                self._level.setBlockDataAt(xc, yc - i, zc, self._level.blockDataAt(xc, y - i, zc))

    def erase_block(self, x, y, z):
        """
        :type x: int
        :type y: int
        :type z: int
        """
        xc = x + self._box.minx
        yc = self._heightmap[z, x]
        zc = z + self._box.minz
        self._heightmap[z, x] = y - 1

        for i in range(abs(y - yc)):
            self._level.setBlockAt(xc, y + i, zc, 0)
            self._level.setBlockDataAt(xc, y + i, zc, 0)

    def revalidate(self, points):
        """
        :type points: set
        """
        for (x, z) in points:
            y = self._heightmap[z, x]
            self._edges[z, x] = 0

            for (xo, zo) in ((x - 1, z), (x + 1, z), (x, z - 1), (x, z + 1)):
                if 0 <= xo < self._box.width and 0 <= zo < self._box.length:
                    yo = self._heightmap[zo, xo]

                    if y - yo != 0:
                        self._edges[z, x] = y
                        self._edges[zo, xo] = yo

    def save(self, filename, ma):
        """
        :type filename: str
        :type ma: ndarray
        """
        bounds = self._level.getWorldBounds()
        array = numpy.zeros((bounds.length, bounds.width, 3), numpy.uint8)

        for z in range(self.length):
            zc = z + self._box.minz - bounds.minz

            for x in range(self.width):
                xc = x + self._box.minx - bounds.minx

                if ma[z, x]:
                    array[zc, xc] = color[self._materials[z, x]]

        Image.fromarray(array, "RGB").save(filename)

    def set_block(self, x, y, z, (block_id, blockdata_id)):
        """
        :type x: int
        :type y: int
        :type z: int
        """
        self._level.setBlockAt(x + self._box.minx, y, z + self._box.minz, block_id)
        self._level.setBlockDataAt(x + self._box.minx, y, z + self._box.minz, blockdata_id)

        for i in range(1, 3):
            # Don't remove stairs
            if self._level.blockAt(x + self._box.minx, y + i, z + self._box.minz) == 67 or \
                    self._level.blockAt(x + self._box.minx, y + i, z + self._box.minz) == 4:
                break

            self._level.setBlockAt(x + self._box.minx, y + i, z + self._box.minz, 0)
            self._level.setBlockDataAt(x + self._box.minx, y + i, z + self._box.minz, 0)


alpha_materials = {
    # Empty
    alphaMaterials.Air.ID: Material.Empty,
    alphaMaterials.BrownMushroom.ID: Material.Empty,
    alphaMaterials.Flower.ID: Material.Empty,
    alphaMaterials.Pumpkin.ID: Material.Ground,
    alphaMaterials.RedMushroom.ID: Material.Empty,
    alphaMaterials.Rose.ID: Material.Empty,
    alphaMaterials.SugarCane.ID: Material.Empty,
    alphaMaterials.TallFlowers.ID: Material.Empty,
    # Ground
    alphaMaterials.Clay.ID: Material.Ground,
    alphaMaterials.CoalOre.ID: Material.Ground,
    alphaMaterials.Dirt.ID: Material.Ground,
    alphaMaterials.Grass.ID: Material.Ground,
    alphaMaterials.Gravel.ID: Material.Ground,
    alphaMaterials.Ice.ID: Material.Ground,
    alphaMaterials.IronOre.ID: Material.Ground,
    alphaMaterials.Sand.ID: Material.Ground,
    alphaMaterials.Sandstone.ID: Material.Ground,
    alphaMaterials.SnowLayer.ID: Material.Ground,
    alphaMaterials.Stone.ID: Material.Ground,
    # Lava
    alphaMaterials.Lava.ID: Material.Lava,
    # Tree
    alphaMaterials.AcaciaLeaves.ID: Material.Tree,
    alphaMaterials.Leaves.ID: Material.Tree,
    alphaMaterials.UnusedShrub.ID: Material.Tree,
    alphaMaterials.Wood.ID: Material.Tree,
    alphaMaterials.Wood2.ID: Material.Tree,
    # Water
    alphaMaterials.Water.ID: Material.Water,
    alphaMaterials.WaterActive.ID: Material.Water
}

color = {
    Material.Empty: (0, 0, 0),
    Material.Ground: (100, 80, 0),
    Material.Lava: (255, 0, 0),
    Material.Tree: (0, 255, 0),
    Material.Water: (0, 0, 255)
}

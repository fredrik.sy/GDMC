from pymclevel.infiniteworld import MCInfdevOldLevel
from pymclevel.materials import Block
from numpy import *
from pymclevel.materials import pocketMaterials


def set_block(level, material, x, y, z):
    """Sets the block at (x, y, z) position.

    :param MCInfdevOldLevel level: The level.
    :param Block material: The material to set.
    :param int x: The x position.
    :param int y: The y position.
    :param int z: The z position.
    """
    level.setBlockAt(x, y, z, material.ID)
    level.setBlockDataAt(x, y, z, material.blockData)


def has_material(level, material, x, y, z):
    """Checks if the block is of material type.

    :param MCInfdevOldLevel level: The level.
    :param Block material: The material type.
    :param int x: The x position.
    :param int y: The y position.
    :param int z: The z position.
    """
    return level.blockAt(x, y, z) == material.ID and level.blockDataAt(x, y, z) == material.blockData


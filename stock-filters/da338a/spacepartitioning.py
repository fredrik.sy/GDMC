import heapq
import numpy
import sys

from graph import Graph
from numpy import ndarray
from pymclevel.box import BoundingBox
from random import randint
from worldmap import WorldMap


class Space(object):
    def __init__(self, x, z, width, length):
        """
        :type x: int
        :type z: int
        :type width: int
        :type length: int
        """
        self.x = x
        self.z = z
        self.width = width
        self.length = length

    def __contains__(self, item):
        (x, z) = item

        if self.x <= x < self.x + self.width and self.z <= z < self.z + self.length:
            return True
        else:
            return False

    def __le__(self, other):
        """
        :type other: Space
        """
        # Reverse For Min-PriorityQueue
        return self.width * self.length > other.width * other.length

    def center(self):
        return self.x + self.width / 2, self.z + self.length / 2


class SpacePartitioning(object):
    def __init__(self, worldmap, options):
        """
        :type worldmap: WorldMap
        :type options: dict
        """
        self._worldmap = worldmap
        self._options = options

    def run(self, graph):
        """
        :type graph: Graph
        """
        stack = set(graph.nodes.iterkeys())
        areas = []
        zones = []
        districts = []

        while stack:
            node_id = stack.pop()
            (ma, minx, minz) = graph.nodes[node_id].mask()

            for space in self._find_spaces(ma, minx, minz):
                self._partitioning(areas, space.x, space.z, space.width, space.length,
                                   self._options['Min Building Area Width'],
                                   self._options['Min Building Area Length'])

        if areas:
            minx = sys.maxint
            minz = sys.maxint
            maxx = -sys.maxint - 1
            maxz = -sys.maxint - 1

            for area in areas:
                if area.x < minx:
                    minx = area.x
                if area.z < minz:
                    minz = area.z
                if maxx < area.x + area.width:
                    maxx = area.x + area.width
                if maxz < area.z + area.length:
                    maxz = area.z + area.length

            self._partitioning(zones, minx, minz, maxx - minx, maxz - minz, self._options['Min District Width'],
                               self._options['Min District Length'])

            for zone in zones:
                a = []

                for area in areas:
                    if area.center() in zone:
                        a.append(BoundingBox((area.x + 1 + self._worldmap.box.minx,
                                              self._worldmap.heightmap[area.z, area.x],
                                              area.z + 1 + self._worldmap.box.minz),
                                             (area.width - 2, 0, area.length - 2)))

                if a:
                    districts.append(a)

        return districts

    # noinspection PyMethodMayBeStatic
    def _find_spaces(self, ma, minx, minz):
        """
        :type ma: ndarray
        :type minx: int
        :type minz: int
        """
        (ma_length, ma_width) = ma.shape

        while True:
            ma_copy = numpy.copy(ma)
            heap = []

            for z in range(ma_length):
                histogram = numpy.zeros(ma_width, dtype=int)
                ma_changed = False

                for x in range(ma_width):
                    for zc in range(z, ma_length):
                        if ma_copy[zc, x]:
                            ma_changed = True
                            ma_copy[zc, x] = False

                        if not ma[zc, x]:
                            histogram[x] = zc - z
                            break
                    else:
                        histogram[x] = zc - z + 1

                if ma_changed:
                    x = 0
                    stack = []

                    while x < len(histogram):
                        if not stack or histogram[stack[-1]] <= histogram[x]:
                            stack.append(x)
                            x = x + 1
                        else:
                            h = histogram[stack.pop()]

                            if stack:
                                w = x - 1 - stack[-1]

                                if w > 10 and h > 10:
                                    space = Space(stack[-1] + 1, z, w, h)
                                    heapq.heappush(heap, space)
                            else:
                                if x > 10 and h > 10:
                                    space = Space(0, z, x, h)
                                    heapq.heappush(heap, space)

                    while stack:
                        h = histogram[stack.pop()]

                        if stack:
                            w = x - 1 - stack[-1]

                            if w > 10 and h > 10:
                                space = Space(stack[-1] + 1, z, w, h)
                                heapq.heappush(heap, space)
                        else:
                            if x > 10 and h > 10:
                                space = Space(0, z, x, h)
                                heapq.heappush(heap, space)

            if heap:
                space = heap[0]

                for z in range(space.z, space.z + space.length):
                    for x in range(space.x, space.x + space.width):
                        ma[z, x] = False

                space.x += minx
                space.z += minz
                yield space
            else:
                break

    # noinspection PyMethodMayBeStatic
    def _partitioning(self, a, x, z, width, length, min_width, min_length):
        """
        :type a: list
        :type x: int
        :type z: int
        :type width: int
        :type length: int
        """
        stack = [(x, z, width, length)]

        while stack:
            (x, z, width, length) = stack.pop()

            if width < length:
                if length < 2 * min_length:
                    a.append(Space(x, z, width, length))
                else:
                    offset = randint(min_length, length - min_length)
                    stack.append((x, z, width, length - offset))
                    stack.append((x, z + length - offset, width, offset))
            else:
                if width < 2 * min_width:
                    a.append(Space(x, z, width, length))
                else:
                    offset = randint(min_width, width - min_width)
                    stack.append((x, z, width - offset, length))
                    stack.append((x + width - offset, z, offset, length))

from Coordinate import Coord


class HouseCoordData(object):
    def __init__(self, house_block_coords, door_coord):
        self.houseBlockCoords = house_block_coords
        self.doorCoord = door_coord

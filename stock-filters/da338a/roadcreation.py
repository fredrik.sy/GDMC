import heapq
import numpy
import random

from astar import AStar
from enum import IntEnum
from pymclevel.materials import alphaMaterials
from worldmap import Material
from worldmap import WorldMap


class BiomeType(IntEnum):
    Uncalculated = -1
    Ocean = 0
    Plains = 1
    Desert = 2
    ExtremeHills = 3
    Forest = 4
    Taiga = 5
    Swampland = 6
    River = 7
    Hell = 8
    Sky = 9
    FrozenOcean = 10
    FrozenRiver = 11
    IcePlains = 12
    IceMountains = 13
    MushroomIsland = 14
    MushroomIslandShore = 15
    Beach = 16
    DesertHills = 17
    ForestHills = 18
    TaigaHills = 19
    ExtremeHillsEdge = 20
    Jungle = 21
    JungleHills = 22
    JungleEdge = 23
    DeepOcean = 24
    StoneBeach = 25
    ColdBeach = 26
    BirchForest = 27
    BirchForestHills = 28
    RoofedForest = 29
    ColdTaiga = 30
    ColdTaigaHills = 31
    MegaTaiga = 32
    MegaTaigaHills = 33
    ExtremeHillsPlus = 34
    Savanna = 35
    SavannaPlateau = 36
    Messa = 37
    MessaPlateauF = 38
    MessaPlateau = 39
    SunflowerPlains = 129
    DesertM = 130
    ExtremeHillsM = 131
    FlowerForest = 132
    TaigaM = 133
    SwamplandM = 134
    IcePlainsSpikes = 140
    IceMountainsSpikes = 141
    JungleM = 149
    JungleEdgeM = 151
    BirchForestM = 155
    BirchForestHillsM = 156
    RoofedForestM = 157
    ColdTaigaM = 158
    MegaSpruceTaiga = 160
    MegaSpruceTaiga2 = 161
    ExtremeHillsPlusM = 162
    SavannaM = 163
    SavannaPlateauM = 164
    Mesa = 165
    MesaPlateauFM = 166
    MesaPlateauM = 167


class Stair(IntEnum):
    BottomEast = 0
    BottomWest = 1
    BottomSouth = 2
    BottomNorth = 3
    TopEast = 4
    TopWest = 5
    TopSouth = 6
    TopNorth = 7


class Road(object):
    def __init__(self, points, worldmap):
        """
        :type points: list
        :type worldmap: WorldMap
        """
        self._biome = numpy.bincount(numpy.array([worldmap.biome(x, z) for (x, z) in points], dtype=int)).argmax()
        self._points = points
        self._worldmap = worldmap

    @property
    def biome(self):
        return self._biome

    @property
    def points(self):
        return self._points


class RoadCreation(object):
    def __init__(self, worldmap):
        """
        :type worldmap: WorldMap
        """
        self._astar = AStar(worldmap)
        self._roads = []
        self._worldmap = worldmap

    def run(self, districts):
        """
        :type districts: list
        """
        self._roads = []
        global_points = []
        local_points = []
        goals = []

        for district in districts:
            heap = []
            points = numpy.array([(x - self._worldmap.box.minx, z - self._worldmap.box.minz) for (x, z) in district],
                                 dtype=int)
            (meanx, meanz) = points.mean(0, dtype=int)

            for point in points:
                point = tuple(point)
                heapq.heappush(heap, (self._astar.manhattan_distance((meanx, meanz), [point]), point))

            if heap:
                heapiter = iter(heap)
                global_points.append(next(heapiter)[1])

                for (distance, point) in heapiter:
                    local_points.append(point)

        random.shuffle(global_points)
        random.shuffle(local_points)

        if global_points:
            goals.append(global_points.pop())

            while global_points:
                points = self._astar.run(global_points.pop(), goals)

                if points is not None:
                    self._roads.append(Road(points, self._worldmap))
                    goals.extend(points)

        while local_points:
            points = self._astar.run(local_points.pop(), goals)

            if points is not None:
                self._roads.append(Road(points, self._worldmap))
                goals.extend(points)

        if self._roads:
            biome = numpy.bincount(numpy.array([road.biome for road in self._roads], dtype=int)).argmax()

            if biome in biome_materials:
                self._build_roads(biome_materials[biome])
            else:
                self._build_roads(biome_materials[BiomeType.Plains])

    def _build_roads(self, (material_id, stair_id)):
        for road in self._roads:
            marked = numpy.zeros_like(self._worldmap.heightmap, dtype=bool)
            (xp, zp) = road.points[-1]

            while road.points:
                (xc, zc) = road.points.pop()
                (xu, zu) = road.points[-1] if road.points else (xc, zc)
                yp = self._worldmap.heightmap[zp, xp]
                yc = self._worldmap.heightmap[zc, xc]
                yu = self._worldmap.heightmap[zu, xu]
                self._expand((xp, yp, zp), (xc, yc, zc), (xu, yu, zu), marked, material_id, stair_id)
                xp = xc
                zp = zc

    def _expand(self, previous, current, upcoming, marked, material_id, stair_id):
        (xp, yp, zp) = previous
        (xc, yc, zc) = current
        (xu, yu, zu) = upcoming

        minx = max(xc - 1, 0)
        minz = max(zc - 1, 0)
        maxx = min(xc + 2, self._worldmap.width)
        maxz = min(zc + 2, self._worldmap.length)

        heightmap = self._worldmap.heightmap[minz:maxz, minx:maxx]
        materials = self._worldmap.materials[minz:maxz, minx:maxx] == Material.Ground

        yp_heightmap = numpy.logical_and.reduce((heightmap == yp, marked[minz:maxz, minx:maxx], materials))
        yc_heightmap = numpy.logical_and.reduce((heightmap == yc, materials))
        yu_heightmap = numpy.logical_and.reduce((heightmap == yu, materials))

        indices = set(numpy.ndindex(yc_heightmap.shape))
        points = {(xc - minx, zc - minz)}

        while points:
            (x, z) = points.pop()
            heap = []
            indices.remove((z, x))

            for (xo, zo) in ((x - 1, z), (x + 1, z), (x, z - 1), (x, z + 1)):
                if (zo, xo) in indices:
                    if yc > yu and yu_heightmap[zo, xo]:
                        heapq.heappush(heap, (abs(x - xo) + abs(z - zo), (xo, yc, zo)))
                    elif yc > yp and yp_heightmap[zo, xo]:
                        heapq.heappush(heap, (abs(x - xo) + abs(z - zo), (xo, yc, zo)))
                    elif yc_heightmap[zo, xo]:
                        points.add((xo, zo))

            xe = xc + (x - 1)
            ze = zc + (z - 1)

            if not marked[ze, xe]:
                marked[ze, xe] = True

                if heap:
                    (xo, yo, zo) = heapq.heappop(heap)[1]
                    self._worldmap.set_block(xe, yo, ze, (stair_id, self._stair_direction((xo, zo), (x, z))))
                else:
                    self._worldmap.set_block(xe, yc, ze, (material_id, 0))

    # noinspection PyMethodMayBeStatic
    def _stair_direction(self, (x, z), (xc, zc)):
        if x < xc:
            return Stair.BottomEast
        elif x > xc:
            return Stair.BottomWest
        elif z < zc:
            return Stair.BottomSouth
        else:
            return Stair.BottomNorth


biome_materials = {
    BiomeType.Plains: (alphaMaterials.StoneBricks.ID, alphaMaterials.StoneBrickStairs.ID),
    BiomeType.Savanna: (alphaMaterials.StoneBricks.ID, alphaMaterials.StoneBrickStairs.ID),
    -2: (alphaMaterials.Brick.ID, alphaMaterials.BrickStairs.ID),
    BiomeType.Desert: (alphaMaterials.Sandstone.ID, alphaMaterials.SandstoneStairs.ID),
    -3: (alphaMaterials.Cobblestone.ID, alphaMaterials.StoneStairs.ID),
    -4: (alphaMaterials.RedSandstone.ID, alphaMaterials.RedSandstoneSairs.ID)
}

# coding=utf-8
from __future__ import division
import random
import time
import GatherWorldData
import pymclevel
import grammar
import sys
import numpy
from districtData import DistrictData
import Furniture

from House import House
from Coordinate import Coord

from graph import Graph
from roadcreation import RoadCreation
from selection import Selection
from spacepartitioning import SpacePartitioning
from worldmap import WorldMap, Material

displayName = 'Settlement Generator'
inputs = [
    (("Selection", "title"),
     ('Min Chunks', 10)),
    (("Space Partitioning", "title"),
     ('Min District Length', 96),
     ('Min District Width', 96),
     ('Min Building Area Length', 15),
     ('Min Building Area Width', 15))
]


class Timer(object):
    def __init__(self):
        self._time = time.time()

    def timeit(self):
        print time.time() - self._time
        self._time = time.time()


def getDistrictData(box, center_coords):
    lowestX = sys.maxint
    highestX = -sys.maxint - 1
    lowestZ = sys.maxint
    highestZ = -sys.maxint - 1

    for coords in center_coords:
        if coords.coordX < lowestX:
            lowestX = coords.coordX
        if coords.coordX > highestX:
            highestX = coords.coordX
        if coords.coordZ < lowestZ:
            lowestZ = coords.coordZ
        if coords.coordZ > highestZ:
            highestZ = coords.coordZ

    blocksInDistrict = (highestX - lowestX) * (highestZ - lowestZ) + 1
    houseToSpaceRatio = (len(center_coords) * 100) / blocksInDistrict
    wealthScore = 0

    # wealth score
    houseAmount = len(center_coords)  # used in a hacky way, I know, I know...
    if houseAmount >= 7:
        if random.randint(0, 10) >= 8:
            wealthScore = 0
        else:
            houseAmount = 7
    if 9 > houseAmount >= 5:
        if random.randint(0, 10) >= 8:
            wealthScore = 1
        else:
            houseAmount = 0
    if houseAmount < 5:
        wealthScore = 2

    # two squares
    twoSquares = False
    if wealthScore == 0 or wealthScore == 1:
        twoSquares = True
    elif wealthScore == 2:
        if random.randint(0, 10) >= 5:
            twoSquares = True
        else:
            twoSquares = False

    # two floors
    twoFloors = False
    if wealthScore == 0:
        twoFloors = True
    elif wealthScore == 1:
        if random.randint(0, 10) >= 5:
            twoFloors = True
    elif wealthScore == 2:
        twoFloors = False

    if lowestX == highestX:
        lowestX = lowestX - 5
        highestX = highestX + 5
    if lowestZ == highestZ:
        lowestZ = lowestZ - 5
        highestZ = highestZ + 5

    xShift = int(lowestX - box.minx)
    zShift = int(lowestZ - box.minz)

    return DistrictData(lowestX, lowestZ, highestX, highestZ, len(center_coords), wealthScore, twoSquares, twoFloors,
                        xShift, zShift)


def perform(level, box, options):
    """Executes the settlement generator.

    :param pymclevel.infiniteworld.MCInfdevOldLevel level: The entirety of the world.
    :param pymclevel.box.BoundingBox box: User-selected box in the world.
    :param dict options: Option parameters of user input variables.
    """
    timer = Timer()

    print 'Create WorldMap\t\t\t\t',
    worldmap = WorldMap(level, box)
    timer.timeit()
    print 'Create Selection\t\t\t',
    selection = Selection(worldmap, options)
    timer.timeit()
    print 'Create SpacePartitioning\t',
    spacepartitioning = SpacePartitioning(worldmap, options)
    timer.timeit()
    print 'Create RoadCreation\t\t\t',
    roadcreation = RoadCreation(worldmap)
    timer.timeit()
    print '| Selection.find()\t\t\t',
    masks = selection.find()
    timer.timeit()

    Furniture.initializeLists()  # initializing furniture lists

    for ma in masks:
        print '\n- Create Graph\t\t\t\t',
        graph = Graph(worldmap, ma)
        timer.timeit()
        print '- Run Grammar\t\t\t\t',
        grammar.run(graph)
        timer.timeit()
        print '- Run SpacePartitioning\t\t',
        districts = spacepartitioning.run(graph)
        timer.timeit()
        district_doors = []
        print '- Run HouseGeneration\t\t',
        for district in districts:
            centerCoords = []
            roadConnectionCoords = []  # list of coordinates where road is supposed to connect into house-structure

            for area in district:
                centerCoords.append(Coord(area.minx + ((area.maxx - area.minx) / 2),
                                          (area.minz + (area.maxz - area.minz) / 2)))

            districtData = getDistrictData(box, centerCoords)
            # creates a "block palette" describing the different blocks that will be used and where,
            # based on surroundings. re-used for the entire district
            districtPalette = GatherWorldData.gatherData(level, districtData, worldmap)

            for area in district:
                #  3rd parameter determines front-side. 4 = -x, 5 = +x, 2 = -z, 3 = +z
                house = House(level, area, random.randint(2, 5), districtData.twoSquares, districtData.twoFloors,
                              area.miny + 1, districtPalette)

                coordinateObject = house.getHouseCoordData()

                for coord in coordinateObject.houseBlockCoords:
                    worldmap.materials[coord.coordZ - worldmap.box.minz, coord.coordX - worldmap.box.minx] = Material.Empty

                roadConnectionCoords.append((coordinateObject.doorCoord.coordX, coordinateObject.doorCoord.coordZ))

            district_doors.append(roadConnectionCoords)

        timer.timeit()
        print '- Run RoadCreation\t\t\t',
        roadcreation.run(district_doors)
        timer.timeit()
